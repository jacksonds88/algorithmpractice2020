import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;

public class TopologicalSort {
    public static void main(String[] args) throws Exception {
        int[][] G = new int[][] {
                {0,0,1,0,0},
                {0,0,1,0,0},
                {0,0,0,1,1},
                {0,0,0,0,1},
                {0,0,0,0,0},
        };

        LinkedList<Vertex> G2 = new LinkedList<>();
        var n0 = new LinkedList<Integer>();
        n0.add(2);
        var n1 = new LinkedList<Integer>();
        n1.add(2);
        var n2 = new LinkedList<Integer>();
        n2.add(3);
        n2.add(4);
        var n3 = new LinkedList<Integer>();
        n3.add(4);
        G2.add(new Vertex(0, n0));
        G2.add(new Vertex(1, n1));
        G2.add(new Vertex(2, n2));
        G2.add(new Vertex(3, n3));
        G2.add(new Vertex(4, new LinkedList<>()));

        //for(int x : sortDecreaseAndCounter(G))
            //System.out.println(x);

        for(int x : sortDecreaseAndCounter(G2))
            System.out.println(x);

        System.out.println();
        for(int x : sortDFS(G))
            System.out.println(x);

        System.out.println();
        for(int x : kahn(G))
            System.out.println(x);
    }

    public static class Vertex
    {
        int id;
        LinkedList<Integer> neighbors;

        public Vertex(int id, LinkedList<Integer> neighbors)
        {
            this.id = id;
            this.neighbors = neighbors;
        }
    }

    // decrease and counter algorithm
    static LinkedList<Integer> kahn(int[][] G) throws Exception {
        var L = new LinkedList<Integer>();
        var S = getNodesWithNoIncomingEdges(G);

        while(!S.isEmpty())
        {
            var n = S.iterator().next();
            S.remove(n);
            L.add(n);
            for(int i = 0; i < G.length; i++)
            {
                if(G[n][i] > 0)
                {
                    G[n][i] = 0; // remove edge n -> i from the graph

                    // if i has no other incoming edges
                    boolean noIncomingEdges = true;
                    for(int j = 0; j < G[i].length; j++)
                        if(G[j][i] > 0)
                        {
                            noIncomingEdges = false;
                            break;
                        }

                    if(noIncomingEdges)
                        S.add(i);
                }
            }
        }

        // if graph has edges then it's a failure
        for(int i = 0; i < G.length; i++)
        {
            for(int j = 0; j < G[i].length; j++)
            {
                System.out.print(G[i][j]);
                //throw new Exception("Graph has at least one cycle");
            }

            System.out.println();
        }

        return L;
    }

    static LinkedList<Integer> sortDecreaseAndCounter(int[][] G)
    {
        var deleted = new LinkedList<Integer>();
        var D = new HashSet<Integer>();
        while (deleted.size() < G.length)
        {
            var toDelete = new LinkedList<Integer>();
            boolean noDependencies = true;
            for(int i = 0; i < G.length; i++)
            {
                if (!D.contains(i))
                {
                    for(int j = 0; j < G[i].length; j++)
                        if (G[j][i] != 0)
                        {
                            noDependencies = false;
                            break;
                        }

                    if(noDependencies)
                        toDelete.add(i);
                }
            }

            for(int i : toDelete)
            {
                for(int j = 0; j < G[i].length; j++)
                    G[i][j] = 0;

                deleted.add(i);
                D.add(i);
            }
        }

        return deleted;
    }

    static LinkedList<Integer> sortDecreaseAndCounter(LinkedList<Vertex> G)
    {
        var deleted = new LinkedList<Integer>();
        var D = new HashSet<Integer>();
        while (deleted.size() < G.size())
        {
            var toDelete = new LinkedList<Integer>();
            boolean noDependencies = true;
            for(int i = 0; i < G.size(); i++)
            {
                if (!D.contains(i))
                {
                    for(var v : G)
                        if (v.id != i)
                            for(int j : v.neighbors)
                                if (j == i)
                                {
                                    noDependencies = false;
                                    break;
                                }

                    if(noDependencies)
                        toDelete.add(i);
                }
            }

            for(int i : toDelete)
            {
                for(var v : G)
                    if(v.id == i)
                        v.neighbors = new LinkedList<>();

                deleted.add(i);
                D.add(i);
            }
        }

        return deleted;
    }

    static LinkedList<Integer> sortDFS(int[][] G)
    {
        var orderedRoute = new LinkedList<Integer>();
        var V = new HashSet<Integer>();
        var startingIndexes = getNodesWithNoIncomingEdges(G);

        for(int i : startingIndexes)
            orderedRoute = sortDFS(G, i, orderedRoute, V);

        var reverseOrderedRoute = new LinkedList<Integer>();
        for(int x : orderedRoute)
            reverseOrderedRoute.addFirst(x);
        return reverseOrderedRoute;
    }

    static Set<Integer> getNodesWithNoIncomingEdges(int[][] G)
    {
        var startingIndexes = new HashSet<Integer>();
        for(int i = 0; i < G.length; i++)
        {
            boolean foundStartingIndex = true;
            for(int j = 0; j < G[i].length; j++)
                if(G[j][i] != 0)
                {
                    foundStartingIndex = false;
                    break;
                }

            if (foundStartingIndex)
                startingIndexes.add(i);
        }

        return startingIndexes;
    }

    static LinkedList<Integer> sortDFS(int[][] G, int i, LinkedList<Integer> orderedRoute, HashSet<Integer> V)
    {
        for(int j = 0; j < G[i].length; j++)
        {
            if(G[i][j] == 1 && !V.contains(j))
                sortDFS(G, j, orderedRoute, V);
        }

        orderedRoute.add(i);
        V.add(i);
        return orderedRoute;
    }
}
