package Hoare;

import java.util.Arrays;

public class Quicksort {
    public static void main(String[] args)
    {
        int[] A = {6, 1, 3, 8, 9, 4, 7};
        quickSort(A, 0, A.length - 1);
        System.out.println(Arrays.toString(A));
    }

    // Hoare.Quicksort routine
    public static void quickSort(int[] a, int low, int high)
    {
        // base condition
        if (low >= high) {
            return;
        }

        // rearrange the elements across pivot
        int pivot = Quickselect.partition(a, low, high);

        quickSort(a, low, pivot - 1);

        // recur on sub-array containing elements more than the pivot
        quickSort(a, pivot + 1, high);
    }
}
