package Hoare;

import java.util.Arrays;

// https://rcoh.me/posts/linear-time-median-finding/
public class Quickselect {

    public static void main(String[] args)
    {
        int[] nums = {3,2,1,5,6,4};
        int[] nums2 = {3,2,3,1,2,4,5,5,6};
        int[] nums3 = {3,3,3,3,4,3,3,3,3};
        int[] nums4 = {7,6,5,4,3,2,1};
        int[] nums5 = {-1, -1};
        int[] nums6 = {3,2,3,1,2,4,5,5,6,7,7,8,2,3,1,1,1,10,11,5,6,2,4,7,8,5,6};

        printTest(nums, 2);
        printTest(nums2, 4);
        printTest(nums3, 1);
        printTest(nums4, 2);
        printTest(nums5, 2);
        printTest(nums6, 20);
    }

    public static void printTest(int[] a, int k)
    {
        System.out.println();
        System.out.println(Arrays.toString(a));
        int valueOfK = findKthLargest(a, k);
        System.out.println("length: " + a.length);
        System.out.println("valueOfK: " + valueOfK);
        System.out.println(Arrays.toString(a));
    }

    public static int findKthLargest(int[] nums, int k)
    {
        int left = 0;
        int right = nums.length - 1;
        int target = nums.length - k;

        return quickSelect(nums, left, right, target);
    }

    private static int quickSelect(int[] nums, int left, int right, int k)
    {
        if (left >= right)
            return nums[left];

        int pivot = partition(nums, left, right);

        if (pivot == k)
            return nums[pivot];
        else if (pivot < k)
            return quickSelect(nums, pivot + 1, right, k);
        else
            return quickSelect(nums, left, pivot - 1, k);
    }

    public static int partition(int[] nums, int left, int right) {
        return partition(nums, left, right, true);
    }

    public static int partition(int[] nums, int left, int right, boolean pickPivot) {
        int p = left, i = left, j = right;
        if (pickPivot)
        {
            int pivot = pickPivot(nums, left, right);
            for (int k = left; k < right; k++)
                if (nums[k] == pivot)
                    p = k;
        }

        while (i <= j) {
            while (i <= j && nums[i] <= nums[p]) i++;
            while (i <= j && nums[j] > nums[p]) j--;

            if (i > j)
                break;

            swap(nums, i, j);
        }
        swap(nums, p, j);
        return j;
    }

    private static int pickPivot(int[] nums, int left, int right)
    {
        if (right - left < 4)
            return left;

        final int chuckSize = 5;
        int chunkCount = (right - left + 1) / chuckSize;
        IntegerTuple[][] chunks = new IntegerTuple[chunkCount][chuckSize];
        for(int i = 0; i < chunkCount; i++)
            for(int j = 0; j < 5; j++)
            {
                int index = left + (chuckSize*i) + j;
                int value = nums[index];
                chunks[i][j] = new IntegerTuple(value, index);
            }

        int[] medians = new int[chunkCount];
        for(int i = 0; i < chunkCount; i++) {
            Arrays.sort(chunks[i]);
            medians[i] = chunks[i][2].value;
        }

        return partition(medians, 0, medians.length - 1, false);
    }

    private static void swap(int[] nums, int i, int j)
    {
        int temp = nums[i];
        nums[i] = nums[j];
        nums[j] = temp;
    }

    private static class IntegerTuple implements Comparable<IntegerTuple> {
        public int value;
        public int index;

        public IntegerTuple(int value, int index) {
            this.value = value;
            this.index = index;
        }

        @Override
        public int compareTo(IntegerTuple o) {
            return Integer.compare(value, o.value);
        }
    }
}
