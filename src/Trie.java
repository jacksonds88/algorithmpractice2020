import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

public class Trie {
    public static void main(String[] args)
    {
        Trie trie = new Trie();
        trie.add("car");
        trie.add("card");
        trie.add("cards");
        trie.add("trie");
        trie.add("tried");

        System.out.println(trie);

        System.out.println(trie.search("tried"));
        System.out.println(trie.search("cat"));

        LinkedList<String> suggestions = new LinkedList<>();
        System.out.println(trie.searchWithSuggestions("cat", 2, suggestions));
        System.out.println(suggestions.toString());
    }

    Node root = new Node('*');
    HashMap<String, Node> map = new HashMap<>();

    public Trie() {}

    public void add(String word)
    {
        addHelpder(root, word.toLowerCase(), 0);
    }

    public void addHelpder(Node current, String word, int index)
    {
        if (index >= word.length())
        {
            current.isCompleteWord = true;
            return;
        }

        Character next = word.charAt(index);
        if(!current.children.containsKey(next))
        {
            Node newChild = new Node(next);
            newChild.path = current.path + next;
            current.children.put(next, newChild);
        }

        addHelpder(current.children.get(next), word, index + 1);
    }

    public boolean search(String word)
    {
        return searchHelper(root, word, 0);
    }

    public boolean searchWithSuggestions(String word, int suggestionCount, LinkedList<String> suggestions)
    {
        Node current = searchOrFindLastNode(root, word, 0);
        if (current == null)
            return true;

        toStringHelper(current, new StringBuilder(current.path.substring(0, current.path.length() - 1)), suggestions, 2);
        return false;
    }

    private boolean searchHelper(Node current, String word, int index)
    {
        if (index == word.length() - 1 && current.isCompleteWord)
        {
            return true;
        }

        if (index >= word.length() - 1)
        {
            return false;
        }

        Character next = word.charAt(index);
        var child = current.children.get(next);
        if (child == null)
            return false;

        return searchHelper(child, word, index + 1);
    }

    private Node searchOrFindLastNode(Node current, String word, int index)
    {
        if (index == word.length() && current.isCompleteWord)
        {
            return null;
        }

        if (index >= word.length())
        {
            return current;
        }

        Character next = word.charAt(index);
        var child = current.children.get(next);
        if (child == null)
            return current;

        return searchOrFindLastNode(child, word, index + 1);
    }

    private void toStringHelper(Node current, StringBuilder sb, List<String> words)
    {
        toStringHelper(current, sb, words, -1);
    }

    private void toStringHelper(Node current, StringBuilder sb, List<String> words, int capacity)
    {
        sb.append(current.character);

        if((capacity < 0 || words.size() < capacity) && current.isCompleteWord)
        {
            words.add(sb.toString());
        }

        for(var child : current.children.entrySet())
        {
            StringBuilder newSb = new StringBuilder(sb);
            if (capacity >= 0)
                toStringHelper(child.getValue(), newSb, words, capacity);
            else
                toStringHelper(child.getValue(), newSb, words);
        }
    }

    public String toString()
    {
        LinkedList<String> words = new LinkedList<>();
        toStringHelper(root, new StringBuilder(), words);

        StringBuilder toReturn = new StringBuilder();
        for(var s : words)
            toReturn.append(s.substring(1)).append("\n");

        return toReturn.toString();
    }

    class Node
    {
        Character character;
        boolean isCompleteWord;
        HashMap<Character, Node> children = new HashMap<>(26);
        String path = new String();

        public Node() {}

        public Node(Character character) {
            this.character = character;
        }

        public String toString()
        {
            return "Char: " + character + "; isCompleted: " + isCompleteWord;
        }
    }
}
