// https://www.youtube.com/watch?v=5o-kdjv7FD0

public class Steps {
    public static void main(String[] args)
    {
        System.out.println(countTwoSteps(2, 0)); // recursion
        System.out.println(countTwoSteps(3, 0)); // recursion
        System.out.println(countTwoSteps(4, 0)); // recursion
        System.out.println(countTwoSteps(5, 0)); // recursion

        System.out.println();
        System.out.println(countTwoSteps(2)); // recursion
        System.out.println(countTwoSteps(3)); // recursion
        System.out.println(countTwoSteps(4)); // recursion
        System.out.println(countTwoSteps(5)); // recursion

        System.out.println();
        System.out.println(countTwoStepsDp(2, 0, new int[2])); // recursion
        System.out.println(countTwoStepsDp(3, 0, new int[3])); // recursion
        System.out.println(countTwoStepsDp(4, 0, new int[4])); // recursion
        System.out.println(countTwoStepsDp(5, 0, new int[5])); // recursion

        System.out.println();
        System.out.println(countTwoStepsDp(2, new int[2])); // recursion
        System.out.println(countTwoStepsDp(3, new int[3])); // recursion
        System.out.println(countTwoStepsDp(4, new int[4])); // recursion
        System.out.println(countTwoStepsDp(5, new int[5])); // recursion

        System.out.println();
        System.out.println(countSteps(4, 0, new int[]{1, 2}));
        System.out.println(countSteps(5, 0, new int[]{1, 2}));
        System.out.println(countSteps(5, 0, new int[]{1, 3, 5})); // 5
    }

    // we could also just use 1 int, and count backwards from n to 0
    public static int countTwoSteps(int n, int current)
    {
        if (current == n)
            return 1;

        int sum = 0;
        sum = countTwoSteps(n, current + 1);

        if (current + 2 <= n)
            sum += countTwoSteps(n, current + 2);

        return sum;
    }

    // we could also just use 1 int, and count backwards from n to 0
    public static int countTwoSteps(int n)
    {
        if (n == 0)
            return 1;

        int sum = 0;
        sum = countTwoSteps(n - 1);

        if (n - 2 >= 0)
            sum += countTwoSteps(n - 2);

        return sum;
    }

    public static int countTwoStepsDp(int n, int current, int[] dp)
    {
        if (current == n)
            return 1;

        if (dp[current] > 0)
            return dp[current];

        int sum = 0;
        sum = countTwoStepsDp(n, current + 1, dp);

        if (current + 2 <= n)
            sum += countTwoStepsDp(n, current + 2, dp);

        dp[current] = sum;
        return sum;
    }

    public static int countTwoStepsDp(int n, int[] dp)
    {
        if (n == 0)
            return 1;

        if (dp[n - 1] > 0)
            return dp[n - 1];

        int sum = 0;
        sum = countTwoStepsDp(n - 1, dp);

        if (n - 2 >= 0)
            sum += countTwoStepsDp(n - 2, dp);

        dp[n - 1] = sum;
        return sum;
    }

    public static int countSteps(int n, int current,  int[] steps)
    {
        if (current == n)
            return 1;

        int sum = 0;
        for (int step : steps) {
            if (current + step <= n)
                sum += countSteps(n, current + step, steps);
        }

        return sum;
    }

    // bottom-up as opposed to top-down
    // iteration
    static int countStepsBottomUp(int n, int[] intervals)
    {
        if (n == 0) return 1;

        int[] S = new int[n + 1];
        S[0] = 1;
        for(int i = 1; i <= n; i++)
        {
            int sum = 0;
            for (int j : intervals)
            {
                if (i - j >= 0)
                    sum += S[i - j];
            }
            S[i] = sum;
        }

        return S[n];
    }
}
