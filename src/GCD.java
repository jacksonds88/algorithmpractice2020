public class GCD {
    public static void main(String[] args)
    {
        System.out.println(gcd(6, 22));
        System.out.println(gcd(22, 6));
        System.out.println(gcd(5, 11));
        System.out.println(gcd(9, 6));

        System.out.println(gcdR(6, 22));
        System.out.println(gcdR(22, 6));
        System.out.println(gcdR(5, 11));
        System.out.println(gcdR(9, 6));
    }

    static int gcd(int a, int b)
    {
        while(b > 0)
        {
            int t = b;
            b = a % b;
            a = t;
        }

        return a;
    }

    static int gcdR(int a, int b)
    {
        if(b == 0)
            return a;
        return gcdR(b, a % b);
    }
}
