import DataStructure.SimpleLinkedList;

// Find intersection of two linked lists
// https://www.youtube.com/watch?v=_7byKXAhxyM

public class IntersectionOfTwoLinkedLists {
    public static void main(String[] args)
    {
        SimpleLinkedList<String> myList = new SimpleLinkedList<>();
        SimpleLinkedList<String> myList2 = new SimpleLinkedList<>();
        setup(myList, myList2);

        System.out.println(merges(myList, myList));
        System.out.println(merges(myList, myList2));

        System.out.println(mergesAt(myList, myList2));
    }

    static void setup(SimpleLinkedList<String> myList, SimpleLinkedList<String> myList2)
    {
        myList.addAll("a", "b", "c", "d", "e", "f", "g");
        myList2.addAll("m", "n");
        System.out.println(myList.toString());
        System.out.println(myList.getSize());

        System.out.println(myList2.toString());
        System.out.println(myList2.getSize());
        var current = myList.getFirstNodeWithElement("e");
        var current2 = myList2.getFirstNodeWithElement("n");
        System.out.println("current: " + current.toString());
        System.out.println("current2: " + current.toString());
        current2.next = current;
        System.out.println("current2: " + current.toString());

        System.out.println(myList2.toString());
    }

    static boolean merges(SimpleLinkedList<String> myList, SimpleLinkedList<String> myList2)
    {
        if (myList == myList2)
            return false; // lists are the same, so they don't merge

        var tailElement = myList.getTailByIteration().e;
        var tailElement2 = myList2.getTailByIteration().e;
        return (tailElement != null && tailElement.equals(tailElement2));
    }

    static SimpleLinkedList.Node mergesAt(SimpleLinkedList<String> myList, SimpleLinkedList<String> myList2)
    {
        var listSize1 = myList.getSizeByIteration();
        var listSize2 = myList2.getSizeByIteration();

        var biggerList = listSize1 >= listSize2 ? myList : myList2;
        var smallerList = listSize1 >= listSize2 ? myList2 : myList;

        int diff = Math.abs(listSize1 - listSize2);
        int count = 0;

        while (count < diff)
        {
            biggerList.next();
            count++;
        }

        while (biggerList.hasNext() && smallerList.hasNext())
        {
            var current = biggerList.current();
            var current2 = smallerList.current();

            if(current.e.equals(current2.e))
                return current;

            biggerList.next();
            smallerList.next();
        }

        return null;
    }
}
