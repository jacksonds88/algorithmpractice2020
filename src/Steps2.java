public class Steps2 {
    public static void main(String[] args) {
        System.out.println(countTwoSteps(2)); // recursion
        System.out.println(countTwoSteps(3)); // recursion
        System.out.println(countTwoSteps(4)); // recursion
        System.out.println(countTwoSteps(5)); // recursion

        System.out.println(countTwoSteps(2, new int[2])); // recursion
        System.out.println(countTwoSteps(3, new int[3])); // recursion
        System.out.println(countTwoSteps(4, new int[4])); // recursion
        System.out.println(countTwoSteps(5, new int[5])); // recursion
    }

    public static int countTwoSteps(int n)
    {
        if (n == 0)
            return 1;

        int sum = countTwoSteps(n - 1);
        if (n > 1)
            sum += countTwoSteps(n - 2);

        return sum;
    }

    public static int countTwoSteps(int n, int[] dp)
    {
        if (n == 0)
            return 1;

        if (dp[n - 1] > 0)
            return dp[n - 1];

        int sum = countTwoSteps(n - 1);
        if (n > 1)
            sum += countTwoSteps(n - 2);

        dp[n - 1] = sum;
        return sum;
    }
}
