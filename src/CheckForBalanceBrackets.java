import java.util.Stack;

// https://www.geeksforgeeks.org/check-for-balanced-parentheses-in-an-expression/

public class CheckForBalanceBrackets {
    // can handle different types of brackets, braces, and parantheses
    public static boolean isBalanced(String s)
    {
        Stack<Character> S = new Stack<>();

        for(char c : s.toCharArray())
        {
            if (c == '<'){
                S.add(c);
            }
            else if (c == '>') {
                if (S.isEmpty())
                    return false;
                S.pop();
            }
        }

        return S.isEmpty();
    }

    public static boolean isBalancedByCounter(String s)
    {
        int openedBrackets = 0;
        for(char c : s.toCharArray())
        {
            if (c == '<'){
                openedBrackets++;
            }
            else if (c == '>') {
                if (openedBrackets == 0)
                    return false;
                openedBrackets--;
            }
        }

        return openedBrackets == 0;
    }

    public static void main(String[] args)
    {
        String s = "<<abc<<123>z>>>";
        String s2 = "<<><>";
        String s3 = ">>><<<";

        System.out.println(isBalanced(s));
        System.out.println(isBalanced(s2));
        System.out.println(isBalanced(s3));

        System.out.println(isBalancedByCounter(s));
        System.out.println(isBalancedByCounter(s2));
        System.out.println(isBalancedByCounter(s3));
    }
}
