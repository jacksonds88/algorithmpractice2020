package LeetCode;

// https://leetcode.com/problems/merge-k-sorted-lists/submissions/

/*
    Try solving with a priority queue
    Try solving with divide-and-conquer much like how MergeSort works
 */

public class MergeKSortedLists {
    public static void main(String[] args)
    {
        var l1a = new ListNode(1);
        var l1b = new ListNode(4);
        var l1c = new ListNode(5);
        l1a.next = l1b;
        l1b.next = l1c;

        var l2a = new ListNode(1);
        var l2b = new ListNode(3);
        var l2c = new ListNode(4);
        l2a.next = l2b;
        l2b.next = l2c;

        var l3a = new ListNode(2);
        l3a.next = new ListNode(6);

        var lists = new ListNode[] {l1a, l2a, l3a};
        var m = new MergeKSortedLists();
        ListNode ret = m.mergeKLists(lists);

        while (ret != null)
        {
            System.out.println(ret.val);
            ret = ret.next;
        }
    }

    public ListNode mergeKLists(ListNode[] lists) {
        if (lists == null || lists.length == 0) {
            return null;
        }

        ListNode mergedList = new ListNode();
        ListNode head = mergedList;

        while(!noneHaveNext(lists))
        {
            int smallestCurrentValue = Integer.MAX_VALUE;
            int smallestNodeIndex = 0;
            for(int i = 0; i < lists.length; i++)
            {
                if (lists[i] == null)
                    continue;

                if (lists[i].val < smallestCurrentValue)
                {
                    smallestCurrentValue = lists[i].val;
                    smallestNodeIndex = i;
                }
            }

            if (lists[smallestNodeIndex] == null)
                break;

            mergedList.next = new ListNode(lists[smallestNodeIndex].val);
            mergedList = mergedList.next;
            lists[smallestNodeIndex] = lists[smallestNodeIndex].next;
        }

        return head.next;
    }

    public boolean noneHaveNext(ListNode[] lists)
    {
        for(var node : lists)
            if(node != null)
                return false;

        return true;
    }

    public static class ListNode
    {
        int val;
        ListNode next;

        public ListNode() {}

        public ListNode(int val) { this.val = val; }

        public String toString()
        {
            return "val=" + val + "; next=" + (next != null ? next.val : null);
        }
    }
}
