package LeetCode;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;

// https://leetcode.com/problems/word-search/

public class WordSearch {

    int[][] DIRECTION = {
            {1, 0}, // down
            {0, 1}, // right
            {-1, 0}, // up
            {0, -1}, // left
    };

    public boolean exist(char[][] board, String word) {
        for (int i = 0; i < board.length; i++)
        {
            for (int j = 0; j < board[i].length; j++)
            {
                if (board[i][j] == word.charAt(0)) {
                    boolean done = exist(board, word, i, j, 0, new HashSet<>());
                    if (done)
                        return true;
                }
            }
        }

        return false;
    }

    public boolean existIteration(char[][] board, String word) {
        for (int i = 0; i < board.length; i++)
        {
            for (int j = 0; j < board[i].length; j++)
            {
                if (board[i][j] == word.charAt(0)) {
                    boolean done = existIteration(board, word, i, j);
                    if (done)
                        return true;
                }
            }
        }

        return false;
    }

    public boolean exist(char[][] board, String word, int row, int col, int index, HashSet<String> V) {
        if (index == word.length() - 1)
            return true;

        String visited = row + "," + col;
        V.add(visited);

        for (int[] dir : DIRECTION)
        {
            int nextRow = row + dir[0];
            int nextCol = col + dir[1];
            String nextPosition = nextRow + "," + nextCol;
            if (isValid(board, nextRow, nextCol)
                    && !V.contains(nextPosition)
                    && board[nextRow][nextCol] == word.charAt(index + 1))
            {
                boolean done = exist(board, word, nextRow, nextCol, index + 1, V);
                if (done)
                    return true;
            }
        }

        return false;
    }

    public boolean existIteration(char[][] board, String word, int row, int col) {

        int index = 0;
        Set<String> V = new HashSet<>();
        String position = row + "," + col;
        String state = position + "," + index;
        Queue<String> Q = new LinkedList<>();
        Q.add(state);
        V.add(position);

        while(!Q.isEmpty())
        {
            String[] s = Q.poll().split(",");
            row = Integer.parseInt(s[0]);
            col = Integer.parseInt(s[1]);
            index = Integer.parseInt(s[2]);

            if (index == word.length() - 1)
                return true;

            position = row + "," + col;
            V.add(position);

            for (int[] dir : DIRECTION)
            {
                int nextRow = row + dir[0];
                int nextCol = col + dir[1];
                String nextPosition = nextRow + "," + nextCol;
                if (isValid(board, nextRow, nextCol)
                        && !V.contains(nextPosition)
                        && board[nextRow][nextCol] == word.charAt(index + 1))
                {
                    state = nextPosition + "," + (index+1);
                    Q.add(state);
                }
            }
        }

        return false;
    }

    private boolean isValid(char[][] board, int row, int col)
    {
        return row >= 0 && row < board.length && col >= 0 && col < board[row].length;
    }

    public void runTests()
    {
        char[][] board = {
                {'A', 'B', 'C', 'E'},
                {'S', 'F', 'C', 'S'},
                {'A', 'D', 'E', 'E'}
        };

        System.out.println(exist(board, "ABCCED")); // true
        System.out.println(exist(board, "SEE")); // true
        System.out.println(exist(board, "ABCB")); // false

        System.out.println(existIteration(board, "ABCCED")); // true
        System.out.println(existIteration(board, "SEE")); // true
        System.out.println(existIteration(board, "ABCB")); // false
    }

    public static void main(String[] args)
    {
        WordSearch WS = new WordSearch();
        WS.runTests();
    }
}
