package LeetCode;

public class ZigZagConversion {
    public static void main(String[] args)
    {
        ZigZagConversion zzc = new ZigZagConversion();
        System.out.println(zzc.convert("PAYPALISHIRING", 4));
    }

    public String convert(String s, int numRows) {
        StringBuilder[] Z = new StringBuilder[numRows];
        for(int i = 0; i < Z.length; i++)
            Z[i] = new StringBuilder();

        if(Z.length == 1)
            return s;

        return helper(s, 0, Z,  0, -1);
    }

    public String helper(String s, int sIndex, StringBuilder[] Z, int p, int lastP) {
        if(sIndex == s.length())
            return buildSolution(Z);

        Z[p].append(s.charAt(sIndex));
        System.out.println(sIndex + ", " + p + ", " + lastP);

        int direction = p - lastP;
        lastP = p;
        if ((direction > 0 && isValid(Z,p + 1)) || !isValid(Z, p - 1))
            p++;
        else if ((direction < 0 && isValid(Z,p - 1)) || !isValid(Z, p + 1))
            p--;

        return helper(s, sIndex + 1, Z, p, lastP);
    }

    boolean isValid(StringBuilder[] Z, int p)
    {
        return p >= 0 && p < Z.length;
    }

    String buildSolution(StringBuilder[] Z)
    {
        int length = 0;
        for(var z : Z)
            length += z.length();
        StringBuilder solution = new StringBuilder(length);

        for(var z : Z)
            solution.append(z);

        return solution.toString();
    }
}
