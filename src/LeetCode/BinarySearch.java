package LeetCode;

public class BinarySearch {

    public int search(int[] nums, int target) {
        return search(nums, target, 0, nums.length);
    }

    public int search(int[] nums, int target, int left, int right) {
        if (right < left)
            return -1;

        int mid = left + ((right - left) / 2);

        if (nums[mid] == target)
            return target;
        else if (nums[mid] > target)
            return search(nums, target, left, mid - 1);
        else
            return search(nums, target, mid + 1, right);
    }

    public int searchIteration(int[] nums, int target) {

        int left = 0;
        int right = nums.length - 1;
        while(left <= right)
        {
            int mid = left + (right - left) / 2;

            if (nums[mid] == target)
                return mid;
            else if (nums[mid] > target)
                right = mid - 1;
            else
                left = mid + 1;
        }

        return -1;
    }

    public static void main(String[] args)
    {
        BinarySearch bs = new BinarySearch();

        int[] nums = {-1,0,3,5,9,12};
        int target = 9;
        System.out.println(bs.searchIteration(nums, target));

        nums = new int[]{-1,0,3,5,9,12};
        target = 2;
        System.out.println(bs.searchIteration(nums, target));

        nums = new int[]{-1,0,3,5,9,12};
        target = 13;
        System.out.println(bs.searchIteration(nums, target));
    }
}
