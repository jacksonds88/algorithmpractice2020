package LeetCode.Trees;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Queue;

public class MergeTwoBinaryTrees {
    public static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode() {
        }

        TreeNode(int val) {
            this.val = val;
        }

        TreeNode(int val, TreeNode left, TreeNode right) {
            this.val = val;
            this.left = left;
            this.right = right;
        }

        public String toString()
        {
            return "[ " + val + " ]";
        }
    }

    public TreeNode mergeTrees(TreeNode t1, TreeNode t2) {
        if (t1 == null && t2 == null)
            return null;

        TreeNode mergedTree = new TreeNode();
        mergeTrees(t1, t2, mergedTree);
        return mergedTree;
    }

    public void mergeTrees(TreeNode t1, TreeNode t2, TreeNode t3) {
        if(t1 == null && t2 == null)
            return;

        t3.val = (t1 != null ? t1.val : 0) + (t2 != null ? t2.val : 0);

        TreeNode t1Left = t1 != null ? t1.left : null;
        TreeNode t2Left = t2 != null ? t2.left : null;
        if (t1Left != null || t2Left != null)
        {
            mergeTrees(t1Left, t2Left, t3.left = new TreeNode());
        }

        TreeNode t1Right = t1 != null ? t1.right : null;
        TreeNode t2Right = t2 != null ? t2.right : null;
        if (t1Right != null || t2Right != null)
        {
            mergeTrees(t1Right, t2Right, t3.right = new TreeNode());
        }
    }

    static String toStringBFS(TreeNode root)
    {
        ArrayList<LinkedList<String>> layers = new ArrayList<>();
        Queue<TreeNode> Q = new LinkedList<>();
        Q.add(root);
        HashMap<TreeNode, Integer> depthMap = new HashMap<>();
        depthMap.put(root, 0);

        while(!Q.isEmpty())
        {
            TreeNode current = Q.poll();
            if (depthMap.get(current) >= layers.size())
            {
                layers.add(depthMap.get(current), new LinkedList<>());
            }
            LinkedList<String> layer = layers.get(depthMap.get(current));
            layer.add(current.toString());

            System.out.println(depthMap.get(current) + ": " + current.toString());

            if (current.left != null)
            {
                Q.add(current.left);
                depthMap.putIfAbsent(current.left, depthMap.get(current) + 1);
            }

            if (current.right != null)
            {
                Q.add(current.right);
                depthMap.putIfAbsent(current.right, depthMap.get(current) + 1);
            }
        }

        StringBuilder sb = new StringBuilder();
        for(int depth = 0; depth < layers.size(); depth++)
        {
            sb.append("depth=").append(depth).append(": ").append(String.join(",", layers.get(depth))).append("\n");
        }

        return sb.toString();
    }

    static String toString(TreeNode root)
    {
        StringBuilder sb = new StringBuilder();
        toString(root, sb, 0);
        return sb.toString();
    }

    static void toString(TreeNode current, StringBuilder sb, int depth)
    {
        if (current.left != null)
            toString(current.left, sb, depth + 1);

        sb.append(current).append(", depth=").append(depth).append("\n");

        if (current.right != null)
            toString(current.right, sb, depth + 1);
    }

    public static void main(String[] args)
    {
        MergeTwoBinaryTrees mtbt = new MergeTwoBinaryTrees();

        TreeNode d = new TreeNode(5);
        TreeNode c = new TreeNode(2);
        TreeNode b = new TreeNode(3, d, null);
        TreeNode a = new TreeNode(1, b, c);

        TreeNode e2 = new TreeNode(7);
        TreeNode d2 = new TreeNode(4);
        TreeNode c2 = new TreeNode(3, null, e2);
        TreeNode b2 = new TreeNode(1, null, d2);
        TreeNode a2 = new TreeNode(2, b2, c2);

        System.out.println(toString(a));
        System.out.println();
        System.out.println(toString(a2));
        System.out.println();

        TreeNode t3 = mtbt.mergeTrees(a, a2);
        System.out.println(toString(t3));
        System.out.println();
        System.out.println(toStringBFS(t3));
    }
}
