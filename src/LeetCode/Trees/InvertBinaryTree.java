package LeetCode.Trees;

import java.util.LinkedList;
import java.util.Queue;

/*
https://leetcode.com/problems/invert-binary-tree/
 */

public class InvertBinaryTree {
    /**
     * Definition for a binary tree node.
     * public class TreeNode {
     *     int val;
     *     TreeNode left;
     *     TreeNode right;
     *     TreeNode() {}
     *     TreeNode(int val) { this.val = val; }
     *     TreeNode(int val, TreeNode left, TreeNode right) {
     *         this.val = val;
     *         this.left = left;
     *         this.right = right;
     *     }
     * }
     */

    static class TreeNode
    {
        int val;
        TreeNode left;
        TreeNode right;

        public TreeNode(int val)
        {
            this.val = val;
        }

        public TreeNode(int val, TreeNode left, TreeNode right)
        {
            this.val = val;
            this.left = left;
            this.right = right;
        }

        public String toString()
        {
            return val + "";
        }
    }

    public TreeNode invertTreeIteration(TreeNode root) {
        if (root == null)
            return null;

        Queue<TreeNode> Q = new LinkedList<>();
        Q.add(root);

        while(!Q.isEmpty())
        {
            var current = Q.poll();

            var left = current.left;
            var right = current.right;

            current.left = right;
            current.right = left;

            if (current.left != null)
                Q.add(current.left);

            if (current.right != null)
                Q.add(current.right);
        }

        return root;
    }

    public TreeNode invertTreeRecursion(TreeNode current) {
        if (current == null)
            return null;

        var left = current.left;
        var right = current.right;

        current.left = right;
        current.right = left;

        if (current.left != null)
            invertTreeRecursion(current.left);

        if (current.right != null)
            invertTreeRecursion(current.right);

        return current;
    }

    public String toString(TreeNode node)
    {
        Queue<TreeNode> Q = new LinkedList<>();
        Q.add(node);
        StringBuilder sb = new StringBuilder();
        int count = 1;
        int layerLimit = (int)Math.pow(2, 0);

        while(!Q.isEmpty())
        {
            var curr = Q.poll();
            sb.append(curr);

            if (curr.left != null)
                Q.add(curr.left);

            if (curr.right != null)
                Q.add(curr.right);

            //.out.println(count + " >= " + layerLimit);
            if(count >= layerLimit)
            {
                //System.out.println("BREAK");
                layerLimit = (int)Math.pow(2, layerLimit) + 1;
                sb.append("\n");
            }
            count++;
        }

        return sb.toString();
    }

    public static void main(String[] args)
    {
        TreeNode f = new TreeNode(6);
        TreeNode g = new TreeNode(9);

        TreeNode d = new TreeNode(1);
        TreeNode e = new TreeNode(3);

        TreeNode b = new TreeNode(2, d, e);
        TreeNode c = new TreeNode(7, f, g);

        TreeNode root = new TreeNode(4, b, c);

        InvertBinaryTree tree = new InvertBinaryTree();
        System.out.println(tree.toString(root));
        tree.invertTreeRecursion(root);
        System.out.println(tree.toString(root));
    }
}
