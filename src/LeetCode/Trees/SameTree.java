package LeetCode.Trees;

public class SameTree {

    public static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode() {
        }

        TreeNode(int val) {
            this.val = val;
        }

        TreeNode(int val, TreeNode left, TreeNode right) {
            this.val = val;
            this.left = left;
            this.right = right;
        }
    }

    public boolean isSameTree(TreeNode p, TreeNode q) {

        if (p == null && q == null)
            return true;
        else if (p == null || q == null)
            return false;

        if (p.val == q.val)
        {
            return isSameTree(p.left, q.left) && isSameTree(p.right, q.right);
        }

        return false;
    }

    public static void main(String[] args)
    {
        SameTree st = new SameTree();

        TreeNode c = new TreeNode(3);
        TreeNode b = new TreeNode(2);
        TreeNode a = new TreeNode(1, b, c);

        TreeNode c2 = new TreeNode(3);
        TreeNode b2 = new TreeNode(2);
        TreeNode a2 = new TreeNode(1, b2, c2);

        TreeNode a3 = new TreeNode(1);

        System.out.println(st.isSameTree(a, a2));
        System.out.println(st.isSameTree(a, a));
        System.out.println(st.isSameTree(a, a3));
    }
}
