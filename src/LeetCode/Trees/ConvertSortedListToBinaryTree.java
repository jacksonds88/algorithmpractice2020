package LeetCode.Trees;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Queue;

public class ConvertSortedListToBinaryTree {

    public static class ListNode {
        int val;
        ListNode next;
        ListNode() {}
        ListNode(int val) { this.val = val; }
        ListNode(int val, ListNode next) { this.val = val; this.next = next; }
    }

    public static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode() {
        }

        TreeNode(int val) {
            this.val = val;
        }

        TreeNode(int val, TreeNode left, TreeNode right) {
            this.val = val;
            this.left = left;
            this.right = right;
        }

        public String toString()
        {
            return "[ " + val + " ]";
        }
    }

    public TreeNode sortedListToBST(ListNode head) {
        if (head == null)
            return null;

        ListNode middleNode = getMiddleListNode(head);

        TreeNode current = new TreeNode(middleNode.val);

        if (head == middleNode)
            return current;

        current.left = sortedListToBST(head);
        current.right = sortedListToBST(middleNode.next);

        return current;
    }

    public ListNode getMiddleListNode(ListNode head)
    {
        ListNode fastPt = head;
        ListNode slowPt = head;
        ListNode prevSlowPt = slowPt;

        int i = 0;
        while(fastPt != null)
        {
            fastPt = fastPt.next;

            if (i % 2 == 1) {
                prevSlowPt = slowPt;
                slowPt = slowPt.next;
            }
            i++;
        }

        if (prevSlowPt != null)
            prevSlowPt.next = null;

        return slowPt;
    }

    static String toStringBFS(TreeNode root)
    {
        ArrayList<LinkedList<String>> layers = new ArrayList<>();
        Queue<TreeNode> Q = new LinkedList<>();
        Q.add(root);
        HashMap<TreeNode, Integer> depthMap = new HashMap<>();
        depthMap.put(root, 0);

        while(!Q.isEmpty())
        {
            TreeNode current = Q.poll();
            if (depthMap.get(current) >= layers.size())
            {
                layers.add(depthMap.get(current), new LinkedList<>());
            }
            LinkedList<String> layer = layers.get(depthMap.get(current));
            layer.add(current.toString());

            System.out.println(depthMap.get(current) + ": " + current.toString());

            if (current.left != null)
            {
                Q.add(current.left);
                depthMap.putIfAbsent(current.left, depthMap.get(current) + 1);
            }

            if (current.right != null)
            {
                Q.add(current.right);
                depthMap.putIfAbsent(current.right, depthMap.get(current) + 1);
            }
        }

        StringBuilder sb = new StringBuilder();
        for(int depth = 0; depth < layers.size(); depth++)
        {
            sb.append("depth=").append(depth).append(": ").append(String.join(",", layers.get(depth))).append("\n");
        }

        return sb.toString();
    }

    public static void main(String[] args)
    {
        // [-10,-3,0,5,9]
        ListNode e = new ListNode(9);
        ListNode d = new ListNode(5, e);
        ListNode c = new ListNode(0, d);
        ListNode b = new ListNode(-3, c);
        ListNode a = new ListNode(-10, b);

        ConvertSortedListToBinaryTree myClass = new ConvertSortedListToBinaryTree();
        TreeNode root = myClass.sortedListToBST(a);

        System.out.println(toStringBFS(root));
    }
}
