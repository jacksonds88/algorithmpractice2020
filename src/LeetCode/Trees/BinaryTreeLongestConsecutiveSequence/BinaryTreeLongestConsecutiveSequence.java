package LeetCode.Trees.BinaryTreeLongestConsecutiveSequence;

// https://leetcode.com/problems/binary-tree-longest-consecutive-sequence/

public class BinaryTreeLongestConsecutiveSequence {
    public static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode() {
        }

        TreeNode(int val) {
            this.val = val;
        }

        TreeNode(int val, TreeNode left, TreeNode right) {
            this.val = val;
            this.left = left;
            this.right = right;
        }
    }

    public int longestConsecutive(TreeNode root) {
        if (root == null)
            return 0;

        return longestConsecutive(root, 0) + 1;
    }

    public int longestConsecutive(TreeNode current, int sequenceLength) {
        int leftSequenceLength = 0;
        int rightSequenceLength = 0;

        //System.out.println(sequenceLength);

        if (current.left != null)
        {
            if (current.val == (current.left.val - 1))
                leftSequenceLength = longestConsecutive(current.left, sequenceLength + 1);
            else
                leftSequenceLength = longestConsecutive(current.left, 0);
        }

        if (current.right != null)
        {
            if (current.val == (current.right.val - 1))
                rightSequenceLength = longestConsecutive(current.right, sequenceLength + 1);
            else
                rightSequenceLength = longestConsecutive(current.right, 0);
        }

        if (leftSequenceLength > 0 || rightSequenceLength > 0)
            return Math.max(leftSequenceLength, rightSequenceLength);

        return sequenceLength;
    }

    public int longestConsecutive2(TreeNode root) {
        if (root == null)
            return 0;
        longestPath2(root);
        return maxValue;
    }

    public int longestConsecutive3(TreeNode root) {
        if (root == null)
            return 0;

        return longestPath3(root, 1);
    }

    int maxValue = 0;
    public int longestPath2(TreeNode current)
    {
        if (current == null)
            return 0;

        int seq = 1;
        if (current.left != null)
        {
            int left = longestPath2(current.left);
            if (current.val == (current.left.val - 1))
                seq = left + 1;
        }

        if (current.right != null)
        {
            int right = longestPath2(current.right);
            if (current.val == (current.right.val - 1))
                seq = Math.max(seq,  right + 1);
        }

        maxValue = Math.max(maxValue, seq);
        System.out.println("Max Value: " + maxValue);
        return seq;
    }

    public int longestPath3(TreeNode current, int seq)
    {
        if (current == null)
            return 0;

        int thisSeq = 1;
        if (current.left != null)
        {
            int left;
            if (current.val == (current.left.val - 1))
                left = longestPath3(current.left, seq + 1);
            else
                left = longestPath3(current.left, 1);

            thisSeq = left;
        }

        if (current.right != null)
        {
            int right;
            if (current.val == (current.right.val - 1))
                right = longestPath3(current.right, seq + 1);
            else
                right = longestPath3(current.right, 1);

            thisSeq = Math.max(thisSeq, right);
        }

        thisSeq = Math.max(thisSeq, seq);
        return thisSeq;
    }

    public static void main(String[] args)
    {
        //[1,null,3,2,4,null,null,null,5]

        TreeNode f = new TreeNode(5);
        TreeNode e = new TreeNode(4, null, f);
        TreeNode d = new TreeNode(2);
        TreeNode c = new TreeNode(3, d, e);
        TreeNode root = new TreeNode(1, null, c);

        BinaryTreeLongestConsecutiveSequence bt = new BinaryTreeLongestConsecutiveSequence();
        System.out.println(bt.longestConsecutive(root));

        System.out.println(bt.longestConsecutive2(root));
        System.out.println(bt.longestConsecutive3(root));
    }
}
