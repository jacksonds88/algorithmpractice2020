package LeetCode.Trees.BinaryTreeLongestConsecutiveSequence;

public class BinaryTreeLongestConsecutiveSequenceII {

    public static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode() {
        }

        TreeNode(int val) {
            this.val = val;
        }

        TreeNode(int val, TreeNode left, TreeNode right) {
            this.val = val;
            this.left = left;
            this.right = right;
        }
    }

    int _longestSequence = 0;
    public int longestConsecutive(TreeNode root) {
        if (root == null)
            return 0;

        longestPath(root);
        return _longestSequence;
    }

    public int[] longestPath(TreeNode current) {
        int increase = 1;
        int decrease = 1;

        if (current.left != null)
        {
            int[] left = longestPath(current.left);
            if (current.val == (current.left.val + 1))
                decrease = left[1] + 1;
            else if (current.val == (current.left.val - 1))
                increase = left[0] + 1;
        }

        if (current.right != null)
        {
            int[] right = longestPath(current.right);
            if (current.val == (current.right.val + 1))
                decrease = right[1] + 1;
            else if (current.val == (current.right.val - 1))
                increase = right[0] + 1;
        }

        _longestSequence = Math.max(_longestSequence, increase + decrease - 1);
        return new int[] {increase, decrease};
    }

    public static void main(String[] args)
    {
        TreeNode f = new TreeNode(5);
        TreeNode e = new TreeNode(4, null, f);
        TreeNode d = new TreeNode(2);
        TreeNode c = new TreeNode(3, d, e);
        TreeNode root = new TreeNode(1, null, c);

        BinaryTreeLongestConsecutiveSequenceII bt = new BinaryTreeLongestConsecutiveSequenceII();
        System.out.println(bt.longestConsecutive(root));
    }
}
