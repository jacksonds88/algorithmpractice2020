package LeetCode.Trees;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Queue;

public class ConvertSortedListToBinaryTreeFirstTry {

    public static class ListNode {
        int val;
        ListNode next;
        ListNode() {}
        ListNode(int val) { this.val = val; }
        ListNode(int val, ListNode next) { this.val = val; this.next = next; }
    }

    public static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode() {
        }

        TreeNode(int val) {
            this.val = val;
        }

        TreeNode(int val, TreeNode left, TreeNode right) {
            this.val = val;
            this.left = left;
            this.right = right;
        }

        public String toString()
        {
            return "[ " + val + " ]";
        }
    }

    public TreeNode sortedListToBST(ListNode head) {
        if (head == null)
            return null;
        int n = getListSize(head);
        ListNode middleNode = head;
        for(int i = 0; i < n/2; i++)
            middleNode = middleNode.next;

        TreeNode root = new TreeNode(middleNode.val);
        partition(root, head, 0, (n/2) - 1);
        partition(root, middleNode.next, (n/2) + 1, n);

        return root;
    }

    public int getListSize(ListNode head)
    {
        int size = 0;
        while(head != null)
        {
            head = head.next;
            size++;
        }

        return size;
    }

    public void partition(TreeNode root, ListNode head, int left, int right)
    {
        if (right - left <= 0 || head == null)
            return;

        ListNode midListNode = head;
        int mid = (right - left) / 2;
        for(int i = 0; i < mid; i++)
            midListNode = midListNode.next;
        insert(root, midListNode.val);

        partition(root, head, left, mid - 1);

        ListNode rightHead = midListNode.next;
        partition(root, rightHead, mid + 1, right);
    }

    public void insert(TreeNode current, int val)
    {
        if (current == null)
            return;

        if (val < current.val)
        {
            if (current.left == null)
            {
                current.left = new TreeNode(val);
            }
            else
                insert(current.left, val);
        }

        if (val > current.val)
        {
            if (current.right == null)
            {
                current.right = new TreeNode(val);
            }
            else
                insert(current.right, val);
        }
    }

    static String toStringBFS(TreeNode root)
    {
        ArrayList<LinkedList<String>> layers = new ArrayList<>();
        Queue<TreeNode> Q = new LinkedList<>();
        Q.add(root);
        HashMap<TreeNode, Integer> depthMap = new HashMap<>();
        depthMap.put(root, 0);

        while(!Q.isEmpty())
        {
            TreeNode current = Q.poll();
            if (depthMap.get(current) >= layers.size())
            {
                layers.add(depthMap.get(current), new LinkedList<>());
            }
            LinkedList<String> layer = layers.get(depthMap.get(current));
            layer.add(current.toString());

            System.out.println(depthMap.get(current) + ": " + current.toString());

            if (current.left != null)
            {
                Q.add(current.left);
                depthMap.putIfAbsent(current.left, depthMap.get(current) + 1);
            }

            if (current.right != null)
            {
                Q.add(current.right);
                depthMap.putIfAbsent(current.right, depthMap.get(current) + 1);
            }
        }

        StringBuilder sb = new StringBuilder();
        for(int depth = 0; depth < layers.size(); depth++)
        {
            sb.append("depth=").append(depth).append(": ").append(String.join(",", layers.get(depth))).append("\n");
        }

        return sb.toString();
    }

    public static void main(String[] args)
    {
        // [-10,-3,0,5,9]
        ListNode e = new ListNode(9);
        ListNode d = new ListNode(5, e);
        ListNode c = new ListNode(0, d);
        ListNode b = new ListNode(-3, c);
        ListNode a = new ListNode(-10, b);

        ConvertSortedListToBinaryTreeFirstTry myClass = new ConvertSortedListToBinaryTreeFirstTry();
        TreeNode root = myClass.sortedListToBST(a);

        System.out.println(toStringBFS(root));
    }
}
