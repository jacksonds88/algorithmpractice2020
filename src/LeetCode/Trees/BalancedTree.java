package LeetCode.Trees;

import com.sun.source.tree.Tree;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Queue;

public class BalancedTree {
    public static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode() {
        }

        TreeNode(int val) {
            this.val = val;
        }

        TreeNode(int val, TreeNode left, TreeNode right) {
            this.val = val;
            this.left = left;
            this.right = right;
        }

        public String toString()
        {
            return "[ " + val + " ]";
        }
    }

    public boolean isBalanced(TreeNode root) {
        if (root == null)
            return true;

        return Math.abs(getHeight2(root.left) - getHeight2(root.right)) < 2 && isBalanced(root.left) && isBalanced(root.right);
    }

    public int getHeight(TreeNode current, int depth) {
        if (current == null)
            return -1;

        int left = getHeight(current.left, depth + 1);
        int right = getHeight(current.right, depth + 1);

        return Math.max(depth, Math.max(left, right));
    }

    public int getHeight2(TreeNode current) {
        if (current == null)
            return -1;

        return 1 + Math.max(getHeight2(current.left), getHeight2(current.right));
    }

    static String toStringBFS(TreeNode root)
    {
        ArrayList<LinkedList<String>> layers = new ArrayList<>();
        Queue<TreeNode> Q = new LinkedList<>();
        Q.add(root);
        HashMap<TreeNode, Integer> depthMap = new HashMap<>();
        depthMap.put(root, 0);

        while(!Q.isEmpty())
        {
            TreeNode current = Q.poll();
            if (depthMap.get(current) >= layers.size())
            {
                layers.add(depthMap.get(current), new LinkedList<>());
            }
            LinkedList<String> layer = layers.get(depthMap.get(current));
            layer.add(current.toString());

            System.out.println(depthMap.get(current) + ": " + current.toString());

            if (current.left != null)
            {
                Q.add(current.left);
                depthMap.putIfAbsent(current.left, depthMap.get(current) + 1);
            }

            if (current.right != null)
            {
                Q.add(current.right);
                depthMap.putIfAbsent(current.right, depthMap.get(current) + 1);
            }
        }

        StringBuilder sb = new StringBuilder();
        for(int depth = 0; depth < layers.size(); depth++)
        {
            sb.append("depth=").append(depth).append(": ").append(String.join(",", layers.get(depth))).append("\n");
        }

        return sb.toString();
    }

    public static void main(String[] args)
    {
        TreeNode e = new TreeNode(7);
        TreeNode d = new TreeNode(15);
        TreeNode c = new TreeNode(20, d, e);
        TreeNode b = new TreeNode(9);
        TreeNode a = new TreeNode(1, b, c);

        TreeNode g2 = new TreeNode(4);
        TreeNode f2 = new TreeNode(4);
        TreeNode e2 = new TreeNode(3);
        TreeNode d2 = new TreeNode(3, f2, g2);
        TreeNode c2 = new TreeNode(2);
        TreeNode b2 = new TreeNode(2, d2, e2);
        TreeNode a2 = new TreeNode(1, b2, c2);

        TreeNode c3 = new TreeNode(3);
        TreeNode b3 = new TreeNode(2, null, c3);
        TreeNode a3 = new TreeNode(1, null, b3);

        System.out.println(toStringBFS(a));
        System.out.println();
        System.out.println(toStringBFS(a2));

        BalancedTree bt = new BalancedTree();
        System.out.println(bt.isBalanced(a));
        System.out.println(bt.isBalanced(a2));


        System.out.println();
        System.out.println(toStringBFS(a3));
        System.out.println(bt.isBalanced(a3));

    }
}
