package LeetCode;

import Core.ListNode;

import java.util.Iterator;
import java.util.LinkedList;

// https://leetcode.com/problems/add-two-numbers/

public class AddTwoNumbers {

    public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        if (l1 == null && l2 == null)
            return null;

        if (l1 == null)
            return l2;
        if (l2 == null)
            return l1;

        ListNode head = new ListNode();
        ListNode current = head;
        int carry = 0;
        do
        {
            int digitSum = (l1 != null ? l1.val : 0)
                            + (l2 != null ? l2.val : 0)
                            + carry;
            carry = digitSum > 9 ? 1 : 0;
            current.next = new ListNode(digitSum % 10);

            l1 = l1 != null ? l1.next : null;
            l2 = l2 != null ? l2.next : null;
            current = current.next;
        } while ((l1 != null || l2 != null) || carry > 0);

        return head.next;
    }

    public boolean equals(ListNode l1, ListNode l2)
    {
        if (l1 == null && l2 == null)
            return true;

        if (l1 == null || l2 == null)
            return false;

        while (l1 != null && l2 != null)
        {
            if (l1.val != l2.val)
                return false;

            l1 = l1.next;
            l2 = l2.next;
        }

        if ((l1 == null && l2 != null)
                || (l1 != null && l2 == null))
            return false;

        return true;
    }

    public String toString(ListNode l)
    {
        if (l == null)
            return "";

        StringBuilder sb = new StringBuilder();
        while (l != null)
        {
            sb.append(l.val).append(",");
            l = l.next;
        }

        return String.join(",", sb.toString().split(","));
    }

    public static void main(String[] args)
    {
        ListNode l1 = new ListNode(0);
        ListNode l1b = new ListNode(5);
        l1.next = l1b;

        ListNode l2 = new ListNode(0);
        ListNode l2b = new ListNode(5);
        l2.next = l2b;

        AddTwoNumbers atn = new AddTwoNumbers();
        String result = atn.toString(atn.addTwoNumbers(l1, l2));
        System.out.println(result);

        System.out.println(atn.equals(l1, l1));
        System.out.println(atn.equals(l1, l2b));
    }
}
