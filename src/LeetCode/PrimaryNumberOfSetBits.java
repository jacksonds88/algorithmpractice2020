package LeetCode;

/*
https://leetcode.com/problems/prime-number-of-set-bits-in-binary-representation/
 */

public class PrimaryNumberOfSetBits {

    int[] primeWithin32range = {2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31};

    public int countPrimeSetBits(int L, int R) {
        int primeCount = 0;
        for(int i = L; i <= R; i++)
        {
            int bitCount = countBits(i);
            System.out.println(i + ": " + bitCount);
            if (isPrimeWithin32range(bitCount))
                primeCount++;
        }

        return primeCount;
    }

    public int countBits(int x)
    {
        int count = 0;
        while(x != 0)
        {
            if((x & 1) == 1)
            {
                count++;
            }

            x = x >> 1;
        }

        return count;
    }

    public boolean isPrimeWithin32range(int x)
    {
        for(int p : primeWithin32range)
            if(p == x)
                return true;

        return false;
    }

    public static void main(String[] args)
    {
        PrimaryNumberOfSetBits P = new PrimaryNumberOfSetBits();

        System.out.println(P.countPrimeSetBits(6, 10));
        System.out.println(P.countPrimeSetBits(11, 15));
    }
}
