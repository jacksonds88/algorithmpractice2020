package LeetCode;

import java.util.*;

// Word Break I: https://www.programcreek.com/2012/12/leetcode-solution-word-break/
// Word Break II: https://www.programcreek.com/2014/03/leetcode-word-break-ii-java/

public class WordBreak {
    public static void main(String[] args)
    {
        var wb = new WordBreak();

        List<String> list = Arrays.asList("cats", "dogs", "sand", "and", "cat");
        String s = "catsandog";

        //System.out.println(wb.recursion(s, list));

        list = Arrays.asList("apple", "pen");
        s = "applepenapple";

        //System.out.println(wb.bruteForce(s, list));

        list = Arrays.asList("car", "ca", "rs");
        s = "cars";

        //System.out.println(wb.recursion(s, list));

        list = Arrays.asList("i", "a", "am", "ace");
        s = "iamace";

        list = Arrays.asList("harry", "potter");
        s = "harrypotter";


        System.out.println(wb.wordBreak(s, list));

        ArrayList<String> words = wb.wordBreak2(s, list);

        for(String word : words)
            System.out.println(word);
    }

    public boolean recursion(String s, List<String> wordDict)
    {
        return recursionHelper(s, wordDict, 0);
    }

    // cars
    // cars, ca, rs
    public boolean recursionHelper(String s, List<String> wordDict, int p)
    {
        if (p == s.length())
            return true;

        for(String word : wordDict)
        {
            if (p + word.length() > s.length())
                continue;

            String w = s.substring(p, p + word.length()); //rs
            if(word.equals(w))
            {
                boolean isDone = recursionHelper(s, wordDict, p + word.length());
                if(isDone)
                    return true;
            }
        }

        return false;
    }

    public boolean wordBreak(String s, List<String> wordDict) {
        boolean[] pos = new boolean[s.length()+1];
        Set<String> S = new HashSet<>(wordDict);

        Arrays.fill(pos, false);

        pos[0]=true;

        System.out.println(Arrays.toString(pos));

        for(int i=0; i<s.length(); i++){
            if(pos[i]){
                for(int j=i+1; j<=s.length(); j++){
                    String sub = s.substring(i, j);
                    System.out.println(sub);
                    if(!pos[j] && S.contains(sub)){
                        pos[j]=true;
                        System.out.println(Arrays.toString(pos));
                    }
                }
            }
        }

        return pos[s.length()];
    }

    public ArrayList<String> wordBreak2(String s, List<String> wordDict) {
        String[] W = new String[s.length()+1];
        Set<String> S = new HashSet<>(wordDict);
        W[0]="";

        for(int i = 0; i < s.length(); i++)
        {
            if(W[i] != null)
            {
                for (int j = i + 1; j <= s.length(); j++)
                {
                    String sub = s.substring(i, j);
                    if(S.contains(sub))
                    {
                        W[j] = sub;
                    }
                }
            }
        }

        if (W[s.length()] != null)
        {
            ArrayList<String> words = new ArrayList<>();
            for(String w : W)
                if(w != null && !w.isBlank())
                    words.add(w);

            return words;
        }

        return null;
    }
}
