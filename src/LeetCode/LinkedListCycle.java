package LeetCode;

import Core.ListNode;

// https://leetcode.com/problems/linked-list-cycle/

public class LinkedListCycle {
    public boolean hasCycle(ListNode head) {
        if (head == null || head.next == null)
            return false;

        ListNode tortoise = head;
        ListNode hare = head.next;
        while (hare != null)
        {
            if (tortoise == hare)
                return true;

            tortoise = tortoise.next;
            hare = hare.next;
            if (hare != null)
                hare = hare.next;
        }

        return false;
    }

    public static void main(String[] args)
    {
        LinkedListCycle LLC = new LinkedListCycle();
        ListNode b = new ListNode(2);
        ListNode a = new ListNode(1, b);
        System.out.println(LLC.hasCycle(a)); // false
        b.next = a;
        System.out.println(LLC.hasCycle(a)); // true

        ListNode d2 = new ListNode(-4);
        ListNode c2 = new ListNode(0, d2);
        ListNode b2 = new ListNode(2, c2);
        ListNode a2 = new ListNode(3, b2);
        d2.next = b2;
        System.out.println(LLC.hasCycle(a2)); // true

        ListNode a3 = new ListNode(1);
        System.out.println(LLC.hasCycle(a3)); // false
    }
}
