package LeetCode;

public class AlienDictionary {

    public static void main(String[] args)
    {
        AlienDictionary ad = new AlienDictionary();

        String order = "hlabcdefgijkmnopqrstuvwxyz";
        String[] words = {"hello", "leetcode"};

        System.out.println(ad.isAlienSorted(words, order));

        order = "hlabcdefgijkmnopqrstuvwxyz";
        words = new String[]{"hello", "hell"};
        System.out.println(ad.isAlienSorted(words, order));

        order = "hlabcdefgijkmnopqrstuvwxyz";
        words = new String[]{"hel", "hell"};
        System.out.println(ad.isAlienSorted(words, order));

        order = "worldabcefghijkmnpqstuvxyz";
        words = new String[]{"word", "world", "row"};
        System.out.println(ad.isAlienSorted(words, order));
    }

    public boolean isAlienSorted(String[] words, String order) {
        if (words.length <= 1)
            return true;

        int[] map = new int[order.length()];

        for(int i = 0; i < order.length(); i++)
            map[order.charAt(i) - 'a'] = i;

        for(int i = 1; i < words.length; i++)
        {
            int length = Math.min(words[i - 1].length(), words[i].length());
            boolean isSubset = true;
            for(int j = 0; j < length; j++)
            {
                if (map[words[i - 1].charAt(j) - 'a'] > map[words[i].charAt(j) - 'a'])
                {
                    return false;
                }

                if(map[words[i - 1].charAt(j) - 'a'] < map[words[i].charAt(j) - 'a'])
                {
                    isSubset = false;
                    break;
                }
            }

            if (isSubset && words[i].length() < words[i - 1].length())
                return false;
        }

        return true;
    }
}
