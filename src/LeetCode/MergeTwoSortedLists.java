package LeetCode;

// https://leetcode.com/problems/merge-two-sorted-lists/

public class MergeTwoSortedLists {
    public static void main(String[] args)
    {
        ListNode l1a = new ListNode(1);
        ListNode l1b = new ListNode(2);
        ListNode l1c = new ListNode(4);
        l1a.next = l1b;
        l1b.next = l1c;

        ListNode l2a = new ListNode(1);
        ListNode l2b = new ListNode(3);
        ListNode l2c = new ListNode(4);
        l2a.next = l2b;
        l2b.next = l2c;

        var m = new MergeTwoSortedLists();
        ListNode ret = m.mergeTwoLists(null, l2a);

        while (ret != null)
        {
            System.out.println(ret.val);
            ret = ret.next;
        }
    }

    public ListNode mergeTwoLists(ListNode l1, ListNode l2) {
        ListNode l3 = new ListNode(Integer.MIN_VALUE);
        ListNode head = l3;

        while(l1 != null && l2 != null)
        {
            if(l1.val <= l2.val)
            {
                l3.next = new ListNode(l1.val);
                l3 = l3.next;
                l1 = l1.next;
            }
            else
            {
                l3.next = new ListNode(l2.val);
                l3 = l3.next;
                l2 = l2.next;
            }
        }

        if (l1 == null)
            l3.next = l2;
        if (l2 == null)
            l3.next = l1;

        return head.next;
    }

    // this doesn't work
    /*public void addAndMove(ListNode R, ListNode S)
    {
        R.next = new ListNode(S.val);
        R = R.next;
        S = S.next;
    }*/

    public static class ListNode
    {
        int val;
        ListNode next;

        public ListNode(int val)
        {
            this.val = val;
        }

        public String toString()
        {
            return "val=" + val + "; next=" + (next != null ? next.val : null);
        }
    }
}
