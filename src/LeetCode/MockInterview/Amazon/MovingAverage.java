package LeetCode.MockInterview.Amazon;

/*
Given a stream of integers and a window size, calculate the moving average of all integers in the sliding window.

MovingAverage m = new MovingAverage(3);
m.next(1) = 1
m.next(10) = (1 + 10) / 2
m.next(3) = (1 + 10 + 3) / 3
m.next(5) = (10 + 3 + 5) / 3
 */

import java.util.LinkedList;

public class MovingAverage {

    LinkedList<Integer> list = new LinkedList<>();
    int size;
    double sum;

    public MovingAverage(int size) {
        this.size = size;
    }

    public double next(int val) {
        if (list.size() < size)
        {
            list.add(val);
            sum += val;
            return sum / (double) list.size();
        }

        int head = list.removeFirst();
        sum -= head;
        list.add(val); // add to tail
        sum += val;

        return sum / (double) list.size();
    }

    public static void main(String[] args)
    {
        MovingAverage ma = new MovingAverage(3);
        System.out.println(ma.next(1));
        System.out.println(ma.next(10));
        System.out.println(ma.next(3));
        System.out.println(ma.next(5));
    }
}
