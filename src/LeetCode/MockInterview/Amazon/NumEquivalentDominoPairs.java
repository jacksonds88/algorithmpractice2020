package LeetCode.MockInterview.Amazon;

import java.util.*;

/*
Given a list of dominoes, dominoes[i] = [a, b] is equivalent to dominoes[j] = [c, d] if and only if
either (a==c and b==d), or (a==d and b==c) - that is, one domino can be rotated to be equal to another domino.

Return the number of pairs (i, j) for which 0 <= i < j < dominoes.length, and dominoes[i] is equivalent to dominoes[j].

Example
Input: dominoes = [[1,2],[2,1],[3,4],[5,6]]
Output: 1

Constraints:
1 <= dominoes.length <= 40000
1 <= dominoes[i][j] <= 9
 */

public class NumEquivalentDominoPairs {

    // solved in linear time. The sort used here is considered constant because it's always 2 elements
    public int numEquivDominoPairs(int[][] dominoes) {
        if(dominoes == null || dominoes.length == 0)
            return 0;

        Map<String, Integer> E = new HashMap<>(dominoes.length);
        for(int[] pair : dominoes)
        {
            Arrays.sort(pair);
            String s = Arrays.toString(pair);
            if(E.containsKey(s))
            {
                E.put(s, E.get(s) + 1);
            }
            else
            {
                E.put(s, 0);
            }
        }

        Map<Integer, Integer> C = new HashMap<>(dominoes.length);
        int count = 0;
        for(Map.Entry<String, Integer> pairCount : E.entrySet())
        {
            if (!C.containsKey(pairCount.getValue()))
            {
                int c = 0;
                for(int i = pairCount.getValue(); i > 0; i--)
                {
                    c += i;
                }

                C.put(pairCount.getValue(), c);
            }

            count += C.get(pairCount.getValue());
        }

        return count;
    }

    public static void main(String[] args)
    {
        NumEquivalentDominoPairs N = new NumEquivalentDominoPairs();
        int[][] dominoPairs = {
                { 1, 2 },
                { 2, 1 },
                { 3, 4 },
                { 5, 6 }
        };

        System.out.println(N.numEquivDominoPairs(dominoPairs)); // 1

        dominoPairs = new int[][]
        {
                { 1, 2 },
                { 1, 2 },
                { 1, 1 },
                { 1, 2 },
                { 2, 2 }
        };

        System.out.println(N.numEquivDominoPairs(dominoPairs)); // 3

        dominoPairs = new int[][]
                {
                        { 1, 2 },
                        { 1, 2 },
                        { 1, 1 },
                        { 1, 2 },
                        { 2, 2 },
                        { 2, 1}
                };

        System.out.println(N.numEquivDominoPairs(dominoPairs)); // 6
    }
}
