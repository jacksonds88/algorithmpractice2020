package LeetCode.MockInterview.Apple;

/*
https://leetcode.com/problems/car-pooling/

You are driving a vehicle that has capacity empty seats initially available for passengers.  The vehicle only drives east (ie. it cannot turn around and drive west.)

Given a list of trips, trip[i] = [num_passengers, start_location, end_location] contains information about the i-th trip: the number of passengers that must be picked up, and the locations to pick them up and drop them off.  The locations are given as the number of kilometers due east from your vehicle's initial location.

Return true if and only if it is possible to pick up and drop off all passengers for all the given trips.
 */

import java.util.Arrays;

public class CarPooling {

    static final int NUM_PASSENGERS_INDEX = 0;
    static final int START_LOCATION_INDEX = 1;
    static final int END_LOCATION_INDEX = 2;

    public boolean carPooling(int[][] trips, int capacity) {

        int min = Integer.MAX_VALUE;
        int max = Integer.MIN_VALUE;
        for (int i = 0; i < trips.length; i++)
        {
            if (trips[i][START_LOCATION_INDEX] < min)
                min = trips[i][START_LOCATION_INDEX];

            if (trips[i][END_LOCATION_INDEX] > max)
                max = trips[i][END_LOCATION_INDEX];
        }

        for (int i = min; i <= max; i++)
        {
            int numPassengers = 0;
            for (int[] trip : trips)
            {
                if (trip[START_LOCATION_INDEX] <= i  && trip[END_LOCATION_INDEX] > i)
                    numPassengers += trip[NUM_PASSENGERS_INDEX];
            }

            if (numPassengers > capacity)
                return false;
        }

        return true;
    }

    public static void main(String[] args)
    {
        CarPooling cp = new CarPooling();
        int[][] trips = {
                {2, 1, 5},
                {3, 3, 7}
        };

        int capacity = 4;
        System.out.println(cp.carPooling(trips, capacity));

        trips = new int[][]{
                {3, 2, 7},
                {3, 7, 9},
                {8, 3, 9}
        };

        capacity = 11;
        System.out.println(cp.carPooling(trips, capacity));
    }
}
