package LeetCode.MockInterview.Apple;

import Core.ListNode;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

// https://leetcode.com/problems/remove-duplicates-from-sorted-list-ii/

public class DeleteDuplicates {

    public ListNode deleteDuplicates(ListNode head) {
        if(head==null)
            return null;

        ListNode current = head;
        ListNode newCurrent = new ListNode(0);
        ListNode newHead = newCurrent;
        boolean duplicateFound = false;
        while(current!=null)
        {
            while(current.next!=null && current.val==current.next.val)
            {
                duplicateFound = true;
                current=current.next;
            }

            if(!duplicateFound)
            {
                newCurrent.next=current;
                newCurrent= newCurrent.next;
            }

            duplicateFound = false;
            current=current.next;
        }

        newCurrent.next=null; // to prevent cycles
        return newHead.next;
    }

    public static void main(String[] args)
    {
        DeleteDuplicates D = new DeleteDuplicates();

        ListNode e = new ListNode(3);
        ListNode d = new ListNode(2);
        ListNode c = new ListNode(1);
        ListNode b = new ListNode(1);
        ListNode a = new ListNode(1);
        a.next = b;
        b.next = c;
        c.next = d;
        d.next = e;

        System.out.println(ListNode.toString(a));
        System.out.println(ListNode.toString(D.deleteDuplicates(a)));

        // Consider input of [1, 1] -> null
        ListNode b2 = new ListNode(1);
        ListNode a2 = new ListNode(1);
        a2.next = b2;

        System.out.println();
        System.out.println(ListNode.toString(a2));
        System.out.println(ListNode.toString(D.deleteDuplicates(a2)));

        // [-3,-1,0,0,0,3,3]
        // Must use a tree map to preserve order [-3, -1]
        // [-1, -3] is wwrong
        ListNode g3 = new ListNode(3);
        ListNode f3 = new ListNode(3);
        ListNode e3 = new ListNode(0);
        ListNode d3 = new ListNode(0);
        ListNode c3 = new ListNode(0);
        ListNode b3 = new ListNode(-1);
        ListNode a3 = new ListNode(-3);
        a3.next = b3;
        b3.next = c3;
        c3.next = d3;
        d3.next = e3;
        e3.next = f3;
        f3.next = g3;

        System.out.println(ListNode.toString(a3));
        System.out.println(ListNode.toString(D.deleteDuplicates(a3)));
    }
}
