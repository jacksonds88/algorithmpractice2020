package LeetCode.MockInterview.Apple;

/*
https://leetcode.com/problems/strobogrammatic-number/

A strobogrammatic number is a number that looks the same when rotated 180 degrees (looked at upside down).

Write a function to determine if a number is strobogrammatic. The number is represented as a string.

The first few strobogrammatic numbers are:
0, 1, 8, 11, 69, 88, 96, 101, 111, 181, 609, 619, 689, 808, 818, 888, 906, 916, 986, 1001, 1111, 1691, 1881, 1961,
6009, 6119, 6699, 6889, 6969, 8008, 8118, 8698, 8888, 8968, 9006, 9116, 9696, 9886, 9966, ...
 */

public class StrobogrammaticNumber {
    public boolean isStrobogrammatic(String num) {
        if (num == null || num.isEmpty())
            return true;

        int lastPosition = num.length() - 1;
        for (int i = lastPosition; i >= 0; i--)
        {
            //System.out.println(num.charAt(i) + " " + num.charAt(lastPosition - i));

            if (num.charAt(i) == '2' || num.charAt(i) == '3' || num.charAt(i) == '4' || num.charAt(i) == '5' || num.charAt(i) == '7')
                return false;

            if (num.charAt(i) == '6' && num.charAt(lastPosition - i) == '6')
                return false;

            if (num.charAt(i) == '9' && num.charAt(lastPosition - i) == '9')
                return false;

            if (num.charAt(i) == '6' && num.charAt(lastPosition - i) == '9')
                continue;

            if (num.charAt(i) == '9' && num.charAt(lastPosition - i) == '6')
                continue;

            if (num.charAt(i) != num.charAt(lastPosition - i))
                return false;
        }

        return true;
    }

    public static void main(String[] args)
    {
        StrobogrammaticNumber SN = new StrobogrammaticNumber();

        System.out.println(SN.isStrobogrammatic("1001"));
        System.out.println(SN.isStrobogrammatic("88"));
        System.out.println(SN.isStrobogrammatic("69"));
        System.out.println(SN.isStrobogrammatic("34"));
        System.out.println(SN.isStrobogrammatic("6889"));
        System.out.println(SN.isStrobogrammatic("73"));
        System.out.println(SN.isStrobogrammatic("1"));
        System.out.println(SN.isStrobogrammatic("6"));
        System.out.println(SN.isStrobogrammatic("9"));
        System.out.println(SN.isStrobogrammatic("1"));
        System.out.println(SN.isStrobogrammatic("96"));
    }
}
