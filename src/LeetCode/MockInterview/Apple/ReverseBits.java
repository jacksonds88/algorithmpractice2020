package LeetCode.MockInterview.Apple;

public class ReverseBits {

    // you need treat n as an unsigned value
    public int reverseBits(int n) {
        return Integer.reverse(n);
    }

    public static void main(String[] args)
    {
        ReverseBits rb = new ReverseBits();
        System.out.println(rb.reverseBits(-3));
        System.out.println(rb.reverseBits(43261596));
    }
}
