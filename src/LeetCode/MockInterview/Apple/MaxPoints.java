package LeetCode.MockInterview.Apple;

import java.util.HashMap;

public class MaxPoints {
    static final int X = 0;
    static final int Y = 1;

    String pointToString(int[] p)
    {
        return "(" + p[X] + "," + p[Y] + ")";
    }

    // flawed solution
    public int maxPoints(int[][] points) {
        if (points.length <= 2)
            return points.length;

        int maxSum = 0;
        for (int[] p : points)
        {
            for (int[] o : points)
            {
                if (p == o) // itself
                    continue;

                // p -> o
                boolean divisionByZero = p[X] - o[X] == 0;
                double m = !divisionByZero ? (double)(p[Y] - o[Y]) / (double)(p[X] - o[X]) : 1; // slope
                int sum = 0;

                for (int[] o2 : points)
                {
                    if (p == o2 || o == o2) {
                        sum++;
                    }
                    else if (!divisionByZero && o2[Y] == p[Y] + m*(o2[X]-p[X])) {
                        sum++;
                    }
                    else if (divisionByZero && p[X] == o2[X]) {
                        sum++;
                    }
                }

                if (sum > maxSum)
                    maxSum = sum;
            }
        }

        return maxSum;
    }

    // this works
    public int maxPoints2(int[][] points) {
        if(points.length<3) return points.length;
        int maxres = 0;
        for(int i =0; i< points.length; i++){
            int same = 0;
            int temp_max =0;
            HashMap<String,Integer> map = new HashMap<>();
            for(int j= 0; j< points.length; j++){
                if(i!=j){
                    int dx = points[i][0] - points[j][0];
                    int dy = points[i][1] - points[j][1];
                    if(dx==0 && dy==0){
                        same++;
                        continue;
                    }
                    int gcd = gcd(dx,dy);
                    String key = dx/gcd + "/" + dy/gcd;
                    map.put(key,map.getOrDefault(key,0)+1);
                    temp_max = Math.max(temp_max, map.get(key));
                }
            }
            maxres = Math.max(maxres,temp_max+same+1);
        }
            return maxres;
    }

    public int gcd(int a, int b){
        while(b!=0){
            int temp = a%b;
            a=b;
            b=temp;
        }
        return a;
    }

    public static void main(String[] args)
    {
        MaxPoints mp = new MaxPoints();

        int[][] points = {
                {1, 1},
                {2, 2},
                {3, 3}
        };

        System.out.println(mp.maxPoints(points));

        int[][] points2 = {
                {1, 1},
                {3, 2},
                {5, 3},
                {4, 1},
                {2, 3},
                {1, 4}
        };

        System.out.println(mp.maxPoints(points2));
    }
}
