package LeetCode.MockInterview.Apple;

import java.util.*;

/*
https://leetcode.com/problems/online-election/

In an election, the i-th vote was cast for persons[i] at time times[i].

Now, we would like to implement the following query function: TopVotedCandidate.q(int t) will return the number of the person that was leading the election at time t.

Votes cast at time t will count towards our query.  In the case of a tie, the most recent vote (among tied candidates) wins.
 */

public class TopVotedCandidate {

    int[] persons;
    int[] times;
    int[] leadingAtTime;

    public TopVotedCandidate(int[] persons, int[] times) {
        this.persons = persons;
        this.times = times;
        leadingAtTime = new int[times.length];

        Set<Integer> S = new HashSet<>();
        for (int p : persons)
            S.add(p);

        int[] votes = new int[S.size()];
        for(int i = 0; i < persons.length; i++)
        {
            votes[persons[i]]++;
            boolean leads = true;
            for (int vote : votes) {
                if (vote > votes[persons[i]]) {
                    leads = false;
                    break;
                }
            }

            if (leads)
            {
                leadingAtTime[i] = persons[i];
            }
            else
            {
                if (i > 0)
                    leadingAtTime[i] = leadingAtTime[i - 1];
            }
        }

        //System.out.println(Arrays.toString(votes));
    }

    public int q(int t) {
        int time = 0;
        if (t >= times[times.length - 1]){
            time = times.length - 1;
        }
        else
        {
            for(int i = 0; i < times.length - 1; i++) {
                if (t < times[i+1]) {
                    time = i;
                    break;
                }
            }
        }
        //System.out.println("Time index: " + time);
        //System.out.println(Arrays.toString(leadingAtTime));
        return leadingAtTime[time];
    }

    public static void main(String[] args)
    {
        int[] persons = { 0, 1, 1, 0, 0, 1, 0 };
        int[] times = { 0, 5, 10, 15, 20, 25, 30 };
        TopVotedCandidate T = new TopVotedCandidate(persons, times);
        /*System.out.println(T.q(3));
        System.out.println(T.q(12));
        System.out.println(T.q(25));
        System.out.println(T.q(30));
        System.out.println(T.q(15));
        System.out.println(T.q(24));
        System.out.println(T.q(8));*/

        persons = new int[]{ 0, 0, 1, 1, 2 };
        times = new int[]{ 0, 67, 69, 74, 87 };
        T = new TopVotedCandidate(persons, times);
        System.out.println(T.q(62));
        System.out.println(T.q(100));
    }
}
