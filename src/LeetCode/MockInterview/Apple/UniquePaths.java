package LeetCode.MockInterview.Apple;

public class UniquePaths {

    public int uniquePaths(int m, int n) {
        if (m == 0 || n == 0)
            return 0;
        int[][] grid = new int[n][m];
        return uniquePaths(grid, 0, 0);
    }

    int uniquePaths(int[][] grid, int row, int col)
    {
        if (row == grid.length - 1 && col == grid[row].length - 1)
            return 1;

        int uniquePaths = 0;
        if (isValidMove(grid, row + 1, col)) // down
            uniquePaths += uniquePaths(grid, row + 1, col);

        if (isValidMove(grid, row, col + 1)) // right
            uniquePaths += uniquePaths(grid, row, col + 1);

        return uniquePaths;
    }

    boolean isValidMove(int[][] grid, int row, int col)
    {
        return row >= 0 && row < grid.length && col >= 0 && col < grid[row].length;
    }

    int uniquePathsDP(int m, int n)
    {
        int[][] grid = new int[m][n];
        for(int i=0; i<m; i++) grid[i][0] = 1;
        for(int j=0; j<n; j++) grid[0][j] = 1;

        for(int i=1; i<m; i++)
        {
            for(int j=1; j<n; j++)
            {
                grid[i][j] = grid[i][j-1]+grid[i-1][j];
            }
        }

        return grid[m-1][n-1];
    }

    public static void main(String[] args)
    {
        UniquePaths up = new UniquePaths();
        System.out.println(up.uniquePaths(3, 2));
        System.out.println(up.uniquePaths(7, 3));
    }
}
