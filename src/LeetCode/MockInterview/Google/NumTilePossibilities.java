package LeetCode.MockInterview.Google;

import java.util.*;

public class NumTilePossibilities {
    public int numTilePossibilities(String tiles) {
        Set<String> V = new HashSet<>();
        LinkedList<String> L = new LinkedList<>();
        for (char c : tiles.toCharArray())
            L.add(c + "");

        numTilePossibilities(L, V);

        for (String s : V)
            System.out.println(s);

        return V.size();
    }

    public List<LinkedList<String>> numTilePossibilities(LinkedList<String> tiles, Set<String> V) {
        if (tiles.isEmpty())
        {
            LinkedList<String> inner = new LinkedList<>();
            List<LinkedList<String>> outer = new LinkedList<>();
            outer.add(inner);
            return outer;
        }

        List<LinkedList<String>> outer = new LinkedList<>();
        for (String c : tiles)
        {
            LinkedList<String> newTiles = new LinkedList<>(tiles);
            newTiles.remove(c);
            if (!newTiles.isEmpty())
                V.add(returnAsString(newTiles));

            for (LinkedList<String> subPerm : numTilePossibilities(newTiles, V))
            {
                subPerm.addFirst(c);
                V.add(returnAsString(subPerm));
                outer.add(subPerm);
            }
        }

        return outer;
    }

    static String returnAsString(List<String> l)
    {
        StringBuilder sb = new StringBuilder(l.size());
        for (String x : l)
        {
            sb.append(x);
        }

        return sb.toString();
    }

    public static void main(String[] args)
    {
        NumTilePossibilities NP = new NumTilePossibilities();
        System.out.println(NP.numTilePossibilities("AAB"));
    }
}
