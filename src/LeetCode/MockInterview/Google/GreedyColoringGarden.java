package LeetCode.MockInterview.Google;

// https://leetcode.com/problems/flower-planting-with-no-adjacent/

// The Leetcode challenge is based on the Greedy Coloring graph problem:
// https://en.wikipedia.org/wiki/Greedy_coloring

public class GreedyColoringGarden {

    public static void main(String[] args)
    {
        LeetCode.GreedyColoringGarden.runTests();
    }
}
