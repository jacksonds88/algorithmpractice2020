package LeetCode.MockInterview.Microsoft;

import Core.TreeNode;

import java.util.LinkedList;
import java.util.List;

public class TrimBST {
    public TreeNode trimBST(TreeNode root, int L, int R) {
        if (root == null)
            return null;

        List<Integer> valid = new LinkedList<>();
        getValidNodes(root, L, R, valid);

        if (valid.isEmpty())
            return null;

        TreeNode newRoot = new TreeNode(valid.get(0));
        valid.remove(0);
        System.out.println(newRoot);

        for (Integer i : valid) {
            System.out.println(i);
            insert(newRoot, i);
        }

        return newRoot;
    }

    private void getValidNodes(TreeNode current, int L, int R, List<Integer> valid)
    {
        if (current.val >= L && current.val <= R)
            valid.add(current.val);

        if (current.left != null)
        {
            getValidNodes(current.left, L, R, valid);
        }

        if (current.right != null)
        {
            getValidNodes(current.right, L, R, valid);
        }
    }

    private TreeNode insert(TreeNode current, int val)
    {
        if (val < current.val)
        {
            if (current.left == null)
                current.left = new TreeNode(val);
            else
                insert(current.left, val);
        }

        if (val > current.val)
        {
            if (current.right == null)
                current.right = new TreeNode(val);
            else
                insert(current.right, val);
        }

        return current;
    }

    public static void main(String[] args)
    {
        TrimBST t = new TrimBST();

        TreeNode e = new TreeNode(1);
        TreeNode d = new TreeNode(2, e, null);
        TreeNode c = new TreeNode(0, null, d);

        TreeNode b = new TreeNode(4);

        TreeNode a = new TreeNode(3, b, c);

        t.trimBST(a, 1, 3);

        TreeNode c2 = new TreeNode(2);
        TreeNode b2 = new TreeNode(0);
        TreeNode a2 = new TreeNode(1, b2, c2);

        t.trimBST(a2, 1, 2);
    }
}
