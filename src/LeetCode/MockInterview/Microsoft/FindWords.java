package LeetCode.MockInterview.Microsoft;

import java.util.LinkedList;
import java.util.List;

// https://leetcode.com/problems/word-search-ii/submissions/

public class FindWords {

    static final int ROW = 0;
    static final int COL = 1;
    static final int[][] DIRECTIONS = {
            {1, 0}, // down
            {0, 1}, // right
            {-1, 0}, // up
            {0, -1} // left
    };

    public List<String> findWords(char[][] board, String[] words) {
        if (board == null || words == null)
            return new LinkedList<>();

        List<String> foundWords = new LinkedList<>();

        for (String word : words)
        {
            boolean found = false;
            for (int i = 0; i < board.length; i++)
            {
                if (!found) {
                    for (int j = 0; j < board[i].length; j++) {
                        if (word.charAt(0) == board[i][j])  // this is required
                        {
                            found = findWords(board, word, i, j, 1);

                            if (found) {
                                foundWords.add(word);
                                break;
                            }
                        }
                    }
                }
            }
        }

        return foundWords;
    }

    boolean findWords(char[][] board, String word, int i, int j, int pos) {
        if (pos >= word.length())
            return true;

        char temp = board[i][j];
        board[i][j] = '.';
        boolean found = false;
        for(int[] dir : DIRECTIONS)
        {
            int newRow = i + dir[ROW];
            int newCol = j + dir[COL];

            if (isValidPosition(board, newRow, newCol)
                    && board[newRow][newCol] == word.charAt(pos))
            {
                found = findWords(board, word, newRow, newCol, pos + 1);
                if (found) {
                    break;
                }
            }
        }
        board[i][j] = temp; // backtrack

        return found;
    }

    boolean isValidPosition(char[][] board, int i, int j)
    {
        return i >= 0 && i < board.length && j >= 0 && j < board[i].length;
    }

    public static void main(String[] args)
    {
        char[][] board = {
                {'o', 'a', 'a', 'n'},
                {'e', 't', 'a', 'e'},
                {'i', 'h', 'k', 'r'},
                {'i', 'f', 'l', 'v'}
        };

        String[] words = {
                "oath",
                "pea",
                "eat",
                "rain"
        };

        FindWords fw = new FindWords();
        List<String> foundWords = fw.findWords(board, words);
        for (String w : foundWords)
            System.out.println(w);

        board = new char[][]{
            {'a'}
        };
        words = new String[] { "a" };

        foundWords = fw.findWords(board, words);
        for (String w : foundWords)
            System.out.println(w);

        board = new char[][]{
                {'a'},
                {'a'}
        };
        words = new String[] { "aaa" };

        foundWords = fw.findWords(board, words);
        for (String w : foundWords)
            System.out.println(w);

        board = new char[][]{
                {'a'},
                {'a'}
        };
        words = new String[] { "aa" };

        foundWords = fw.findWords(board, words);
        for (String w : foundWords)
            System.out.println(w);
    }
}
