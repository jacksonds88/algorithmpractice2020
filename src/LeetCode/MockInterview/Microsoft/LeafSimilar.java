package LeetCode.MockInterview.Microsoft;

import Core.TreeNode;

import java.util.Iterator;
import java.util.LinkedList;

public class LeafSimilar {
    public boolean leafSimilar(TreeNode root1, TreeNode root2) {
        LinkedList<TreeNode> list1 = new LinkedList<>();
        LinkedList<TreeNode> list2 = new LinkedList<>();

        getInOdderTraversalOf(root1, list1);
        getInOdderTraversalOf(root2, list2);

        return isEqual(list1, list2);
    }

    void getInOdderTraversalOf(TreeNode current, LinkedList<TreeNode> list)
    {
        if (current.left != null)
            getInOdderTraversalOf(current.left, list);

        if (current.left == null && current.right == null)
            list.add(current);

        if (current.right != null)
            getInOdderTraversalOf(current.right, list);
    }

    boolean isEqual(LinkedList<TreeNode> list1, LinkedList<TreeNode> list2)
    {
        if (list1 == null && list2 == null)
            return true;

        if (list1 == null || list2 == null)
            return false;

        if (list1.size() != list2.size())
            return false;

        Iterator<TreeNode> it1 = list1.iterator();
        Iterator<TreeNode> it2 = list2.iterator();
        while (it1.hasNext() && it2.hasNext())
        {
            TreeNode t1 = it1.next();
            TreeNode t2 = it2.next();

            if (t1.val != t2.val)
                return false;
        }

        return true;
    }

    public static void main(String[] args)
    {
        TreeNode e = new TreeNode(7);
        TreeNode d = new TreeNode(15);
        TreeNode c = new TreeNode(20, d, e);
        TreeNode b = new TreeNode(9);
        TreeNode a = new TreeNode(1, b, c);

        TreeNode g2 = new TreeNode(4);
        TreeNode f2 = new TreeNode(4);
        TreeNode e2 = new TreeNode(3);
        TreeNode d2 = new TreeNode(3, f2, g2);
        TreeNode c2 = new TreeNode(2);
        TreeNode b2 = new TreeNode(2, d2, e2);
        TreeNode a2 = new TreeNode(1, b2, c2);

        LeafSimilar L = new LeafSimilar();
        System.out.println(L.leafSimilar(a, a));
        System.out.println(L.leafSimilar(a, a2));
    }
}
