package LeetCode.MockInterview.Microsoft;

import java.util.Arrays;

public class FlipAndInvertImage {
    public int[][] flipAndInvertImage(int[][] A) {
        int[][] B = new int [A.length][A[0].length];

        // flip
        for (int i = 0; i < B.length; i++)
        {
            int j2 = A[i].length - 1;
            for(int j = 0; j < B[i].length; j++, j2--)
                B[i][j] = A[i][j2];
        }

        // invert
        for (int i = 0; i < B.length; i++)
        {
            for(int j = 0; j < B[i].length; j++)
                B[i][j] = B[i][j] == 1 ? 0 : 1;
        }

        return B;
    }

    public static void main(String[] args)
    {
        int[][] A = {
                {1, 1, 0},
                {1, 0, 1},
                {0, 0, 0}
        };
        FlipAndInvertImage F = new FlipAndInvertImage();
        int[][] B = F.flipAndInvertImage(A);

        for(int[] b : B)
            System.out.println(Arrays.toString(b));
    }
}
