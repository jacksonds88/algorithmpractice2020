package LeetCode.MockInterview.Microsoft;

import Core.ListNode;

import java.util.Iterator;
import java.util.LinkedList;

public class AddTwoNumbersII {
    public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        if (l1 == null && l2 == null)
            return null;

        if (l1 == null)
            return l2;

        if (l2 == null)
            return l1;

        LinkedList<Integer> reverseOrder1 = new LinkedList<>();
        LinkedList<Integer> reverseOrder2 = new LinkedList<>();
        setRevserseLists(l1, l2, reverseOrder1, reverseOrder2);

        Iterator<Integer> it1 = reverseOrder1.iterator();
        Iterator<Integer>  it2 = reverseOrder2.iterator();
        LinkedList<Integer> valuesToReturn = new LinkedList<>();
        int carry = 0;
        while (it1.hasNext() || it2.hasNext())
        {
            int firstOperand = (it1.hasNext() ? it1.next() : 0);
            int secondOperand = (it2.hasNext() ? it2.next() : 0);
            //System.out.println(firstOperand + " + " + secondOperand);
            int digitSum = firstOperand
                    + secondOperand
                    + carry;
            carry = digitSum > 9 ? 1 : 0;
            valuesToReturn.addFirst(digitSum % 10);
        }

        if (carry > 0)
            valuesToReturn.addFirst(1);

        ListNode head = new ListNode(0);
        ListNode current = head;
        for (Integer x : valuesToReturn)
        {
            current.next = new ListNode(x);
            current = current.next;
        }

        return head.next;
    }

    void setRevserseLists(ListNode l1, ListNode l2, LinkedList<Integer> reverseOrder1, LinkedList<Integer> reverseOrder2)
    {
        while (l1 != null)
        {
            reverseOrder1.addFirst(l1.val);
            l1 = l1.next;
        }

        while (l2 != null)
        {
            reverseOrder2.addFirst(l2.val);
            l2 = l2.next;
        }

        int maxSize = Math.max(reverseOrder1.size(), reverseOrder2.size());
        while (maxSize < reverseOrder1.size())
            reverseOrder1.addFirst(0);
        while (maxSize < reverseOrder2.size())
            reverseOrder2.addFirst(0);
    }

    public String toString(ListNode l)
    {
        if (l == null)
            return "";

        StringBuilder sb = new StringBuilder();
        while (l != null)
        {
            sb.append(l.val).append(",");
            l = l.next;
        }

        return String.join(",", sb.toString().split(","));
    }

    public static void main(String[] args)
    {
        ListNode d = new ListNode(3);
        ListNode c = new ListNode(4);
        ListNode b = new ListNode(2);
        ListNode a = new ListNode(7);
        a.next = b;
        b.next = c;
        c.next = d;

        ListNode c2 = new ListNode(4);
        ListNode b2 = new ListNode(6);
        ListNode a2 = new ListNode(5);
        a2.next = b2;
        b2.next = c2;

        AddTwoNumbersII A = new AddTwoNumbersII();
        System.out.println(A.toString(A.addTwoNumbers(a, a2)));
    }
}
