package LeetCode.MockInterview.Microsoft;

import Core.ListNode;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class SwapPairs {
    // the ideal "in-place" solution
    public ListNode swapPairs(ListNode head) {
        if (head == null)
            return null;

        ListNode last = null;
        ListNode current = head; // 1
        while (current != null && current.next != null)
        {
            ListNode first = current; // 1, 3
            ListNode second = current.next; // 2, 4
            if (last != null) // alternatively, we could instantiate last = new Node(-1), and return last.next as Head
            {
                last.next = second;
            }
            else
            {
                head = second;
            }

            ListNode secondNext = second.next; // 3, null
            second.next = first; // 2 -> 1, 4 -> 3
            first.next = secondNext; // 1 -> 3, 3 -> null

            current = secondNext; // 1 -> 3,
            last = first;
        }

        return head;
    }

    public ListNode swapPairsRecursive(ListNode head) {
        System.out.println(head);
        if (head == null || head.next == null)
            return head;

        ListNode third = swapPairsRecursive(head.next.next);
        ListNode second = head.next;
        head.next = third;
        second.next = head;
        return second;
    }

    // the not "in-place" solution
    public ListNode swapPairs2(ListNode head) {
        if (head == null)
            return head;

        List<ListNode> nodes = new LinkedList<>();

        ListNode current = head;
        while (current != null)
        {
            nodes.add(current);
            current = current.next;
        }

        List<ListNode> swappedNodes = new ArrayList<>();
        Iterator<ListNode> nodeIterator = nodes.iterator();
        while (nodeIterator.hasNext()){
            ListNode first = nodeIterator.next();
            if (nodeIterator.hasNext()) {
                ListNode second = nodeIterator.next();
                swappedNodes.add(second);
                swappedNodes.add(first);
            }
            else
            {
                swappedNodes.add(first);
            }
        }

        ListNode newHead = swappedNodes.get(0);
        for(int i = 0; i < swappedNodes.size(); i++)
        {
            current = swappedNodes.get(i);
            current.next = i + 1 < swappedNodes.size() ? swappedNodes.get(i + 1) : null;
        }

        return newHead;
    }

    // infinite loop
    // 2->1->
    public String toString(ListNode head)
    {
        StringBuilder sb = new StringBuilder();

        while (head != null)
        {
            sb.append(head.toString()).append("\n");
            head = head.next;
        }

        return sb.toString();
    }

    public static void main(String[] args)
    {
        SwapPairs sp = new SwapPairs();

        ListNode a = new ListNode(1);
        ListNode b = new ListNode(2);
        ListNode c = new ListNode(3);
        ListNode d = new ListNode(4);
        a.next = b;
        b.next = c;
        c.next = d;

        System.out.println(sp.toString(sp.swapPairsRecursive(a)));


        ListNode e = new ListNode(1);
        ListNode f = new ListNode(2);
        ListNode g = new ListNode(3);
        e.next = f;
        f.next = g;

        ListNode newHead = sp.swapPairs(e);
        System.out.println(sp.toString(newHead));

        ListNode h = new ListNode(1);
        newHead = sp.swapPairs(h);
        System.out.println(sp.toString(newHead));
    }
}
