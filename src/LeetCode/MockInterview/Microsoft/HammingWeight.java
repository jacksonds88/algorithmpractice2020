package LeetCode.MockInterview.Microsoft;

/*

Input: 00000000000000000000000000001011
Output: 3
Explanation: The input binary string 00000000000000000000000000001011 has a total of three '1' bits.


Input: 11111111111111111111111111111101
Output: 31
Explanation: The input binary string 11111111111111111111111111111101 has a total of thirty one '1' bits.

Note:

Note that in some languages such as Java, there is no unsigned integer type. In this case, the input will be given as signed integer type and should not affect your implementation, as the internal binary representation of the integer is the same whether it is signed or unsigned.
In Java, the compiler represents the signed integers using 2's complement notation. Therefore, in Example 3 above the input represents the signed integer -3.

Follow up
If this function is called many times, how would you optimize it?
 */

public class HammingWeight {
    public int hammingWeight(int n) {
        int count = 0;

        while (n != 0) {
            count += (n & 1);
            n = n >>> 1; // >>> is the UNSIGNED right bit shift
        }

        return count;
    }

    public int hammingWeightRecursion(int n)
    {
        if (n == 0)
            return n;

        return (n & 1) + hammingWeightRecursion(n >>> 1);
    }

    public static void main(String[] args)
    {
        HammingWeight HW = new HammingWeight();
        System.out.println(HW.hammingWeight(7));
        System.out.println(HW.hammingWeight(3));
        System.out.println(HW.hammingWeight(-1));
        System.out.println(HW.hammingWeight(8));
    }
}
