package LeetCode.MockInterview.Microsoft;

public class ReverseWords {
    public String reverseWords(String s) {
        if (s == null)
            return "";

        s = s.trim();

        String[] split = s.split(" ");
        String[] reverse = new String[split.length];

        for(int i = 0; i < split.length; i++)
        {
            StringBuilder sb = new StringBuilder(split[i].length());
            for(int j = split[i].length() - 1; j >= 0; j--)
                sb.append(split[i].charAt(j));
            reverse[i] = sb.toString();
        }

        return String.join(" ", reverse);
    }

    public static void main(String[] args)
    {
        ReverseWords rw = new ReverseWords();
        System.out.print(rw.reverseWords("Let's take LeetCode contest"));
    }
}
