package LeetCode.MockInterview.Microsoft;

import Core.ListNode;

public class DeleteNode {
    public void deleteNode(ListNode node) {
        if (node == null)
            return;

        ListNode current = node;
        ListNode last = null;
        while(current != null)
        {
            if (current.next != null)
                current.val = current.next.val;
            else
                if (last != null)
                    last.next = null;

            last = current;
            current = current.next;
        }
    }

    public static void main(String[] args)
    {
        DeleteNode D = new DeleteNode();

        ListNode d = new ListNode(9);
        ListNode c = new ListNode(1);
        ListNode b = new ListNode(5);
        ListNode a = new ListNode(4);
        a.next = b;
        b.next = c;
        c.next = d;

        System.out.println(ListNode.toString(a));
        D.deleteNode(b);
        System.out.println(ListNode.toString(a));
        D.deleteNode(b);
        System.out.println(ListNode.toString(a));
        D.deleteNode(a);
        System.out.println(ListNode.toString(a));
    }
}
