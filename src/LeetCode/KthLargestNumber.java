package LeetCode;

import java.util.Arrays;


public class KthLargestNumber {

    public static void main(String[] args)
    {
        int[] nums = {3,2,1,5,6,4};
        int[] nums2 = {3,2,3,1,2,4,5,5,6};
        int[] nums3 = {3,3,3,3,4,3,3,3,3};
        int[] nums4 = {7,6,5,4,3,2,1};
        int[] nums5 = {-1, -1};
        int[] nums6 = {3,2,3,1,2,4,5,5,6,7,7,8,2,3,1,1,1,10,11,5,6,2,4,7,8,5,6};

        //printTest(nums, 2);
        //printTest(nums2, 4);
        //printTest(nums3, 1);
        //printTest(nums4, 2);
        printTest(nums5, 2); // fail
        //printTest(nums6, 20);
    }

    public static void printTest(int[] a, int k)
    {
        System.out.println();
        System.out.println(Arrays.toString(a));
        int valueOfK = findKthLargest(a, k);
        System.out.println("length: " + a.length);
        System.out.println("valueOfK: " + valueOfK);
        System.out.println(Arrays.toString(a));
    }

    public static int findKthLargest(int[] nums, int k) {
        int left = 0;
        int right = nums.length - 1;
        int target = nums.length - k;

        while (left < right) {
            int pivot = partition(nums, left, right);

            if (pivot < target)
                left = pivot + 1;
            else if (pivot > target)
                right = pivot - 1;
            else
                return nums[pivot];
        }

        return nums[left];
    }

    private static int partition(int[] nums, int left, int right) {
        int i = left;
        int j = right;
        int pivot = i;

        while (i <= j) {
            while (i <= j && nums[i] <= nums[pivot]) i++;
            while (i <= j && nums[j] > nums[pivot]) j--;

            if (i > j)
                break;

            swap(nums, i, j);
        }
        swap(nums, pivot, j);
        return j;
    }

    private static void swap(int[] nums, int i, int j)
    {
        int temp = nums[i];
        nums[i] = nums[j];
        nums[j] = temp;
    }
}
