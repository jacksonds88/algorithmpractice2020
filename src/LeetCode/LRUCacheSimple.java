package LeetCode;

import java.util.LinkedHashMap;

// https://leetcode.com/problems/lru-cache/

public class LRUCacheSimple {
    public static void main(String[] args) {
        var cache = new LRUCacheSimple(2);

        cache.put(1, 1);
        cache.put(2, 2);
        System.out.println(cache.get(1));       // returns 1
        cache.put(3, 3);    // evicts key 2
        System.out.println(cache.get(2));       // returns -1 (not found)
        cache.put(4, 4);    // evicts key 1
        System.out.println(cache.get(1));       // returns -1 (not found)
        System.out.println(cache.get(3));       // returns 3
        System.out.println(cache.get(4));       // returns 4

        System.out.println("Size: " + cache.size);
    }

    int capacity;
    int size;
    LinkedHashMap<Integer, Integer> cache;

    public LRUCacheSimple(int capacity) {
        this.capacity = capacity;
        cache = new LinkedHashMap<>(capacity);
    }

    public int get(int key) {
        var value = cache.get(key);
        if(value != null)
        {
            cache.remove(key, value);
            cache.put(key, value);
            return value;
        }

        return -1;
    }

    public void put(int key, int value)
    {
        if(size >= capacity)
        {
            var it = cache.entrySet().iterator();
            if(it.hasNext())
            {
                it.next();
                it.remove();
                size--;
            }
        }

        cache.put(key, value);
        size++;
    }

    public int getSize()
    {
        return size;
    }

    public int getCapacity()
    {
        return capacity;
    }
}