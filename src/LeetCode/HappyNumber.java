package LeetCode;

import java.util.HashSet;
import java.util.Set;

// https://leetcode.com/problems/happy-number/

public class HappyNumber {
    public boolean isHappy(int n) {
        Set<Integer> S = new HashSet<>();
        while(true)
        {
            int sumOfSquares = getSumOfSquares(n);
            if (S.contains(sumOfSquares))
                return false;

            if (sumOfSquares == 1)
                return true;

            S.add(sumOfSquares);
            n = sumOfSquares;
        }
    }

    int getSumOfSquares(int n)
    {
        int sumOfSquares = 0;
        while(n > 0)
        {
            int d = n % 10;
            n = n / 10;
            sumOfSquares += d * d;
        }

        return sumOfSquares;
    }

    // Out of memory
    public boolean isHappy2(int n) {
        Set<Integer> S = new HashSet<>();
        while (true)
        {
            String number = n + "";
            int[] digits = new int[n];
            int i = 0;
            for(char c : number.toCharArray())
            {
                digits[i++] = Integer.parseInt(c + "");
            }

            int sumOfSquares = 0;
            for (int x : digits)
            {
                sumOfSquares += Math.pow(x, 2);
            }

            //System.out.println(sumOfSquares);
            if (S.contains(sumOfSquares))
                return false;

            if (sumOfSquares == 1)
                return true;

            S.add(sumOfSquares);
            n = sumOfSquares;
        }
    }

    public static void main(String[] args)
    {
        HappyNumber HN = new HappyNumber();
        System.out.println(HN.isHappy(19)); // true
        System.out.println(HN.isHappy(116)); // false
    }
}
