package LeetCode;

import java.util.Arrays;

// https://leetcode.com/problems/plus-one/

public class PlusOne {
    public int[] plusOne(int[] digits) {
        if (digits == null || digits.length == 0)
            return null;

        int index = digits.length - 1;
        int carry = 1; // just add the "Plus One" to the initial carry to reduce code

        while(carry > 0 && index >= 0)
        {
            int leastSignificantDigit = digits[index] + 1;
            digits[index] = leastSignificantDigit % 10;
            carry = leastSignificantDigit > 9 ? 1 : 0;
            index--;
        }

        if (carry > 0 && index < 0)
        {
            int[] digits2 = new int[digits.length + 1];
            for(int i = 0; i < digits.length; i++)
                digits2[i+1] = digits[i];
            digits2[0] = carry;
            return digits2;
        }

        return digits;
    }

    public int[] plusOneFirstAttempt(int[] digits) {
        if (digits == null || digits.length == 0)
            return null;

        int index = digits.length - 1;
        int leastSignificantDigit = digits[index] + 1; // the "Plus One"
        digits[index] = leastSignificantDigit % 10;
        index--;
        int carry = leastSignificantDigit > 9 ? 1 : 0;

        while(carry > 0 && index >= 0)
        {
            leastSignificantDigit = digits[index] + 1;
            digits[index] = leastSignificantDigit % 10;
            carry = leastSignificantDigit > 9 ? 1 : 0;
            index--;
        }

        if (carry > 0 && index < 0)
        {
            int[] digits2 = new int[digits.length + 1];
            for(int i = 0; i < digits.length; i++)
                digits2[i+1] = digits[i];
            digits2[0] = carry;
            return digits2;
        }

        return digits;
    }

    public static void main(String[] args)
    {
        PlusOne po = new PlusOne();
        System.out.println(Arrays.toString(po.plusOne(new int[]{1, 2, 3})));
        System.out.println(Arrays.toString(po.plusOne(new int[]{4, 3, 2, 1})));
        System.out.println(Arrays.toString(po.plusOne(new int[]{9, 9})));
    }
}
