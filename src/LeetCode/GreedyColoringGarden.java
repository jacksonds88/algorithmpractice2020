package LeetCode;

import java.util.*;

// https://leetcode.com/problems/flower-planting-with-no-adjacent/

// The Leetcode challenge is based on the Greedy Coloring graph problem:
// https://en.wikipedia.org/wiki/Greedy_coloring

public class GreedyColoringGarden {
    public int[] gardenNoAdj(int N, int[][] paths) {
        Map<Integer, Set<Integer>> adjacencyList = buildAgencyList(paths);

        int[] colors = new int[N];
        for(int current : adjacencyList.keySet()) // nodes start at 1, not 0
        {
            // find colors connected to current node
            Set<Integer> usedColors = new HashSet<>();
            for (int neighbor : adjacencyList.getOrDefault(current, new HashSet<>()))
            {
                usedColors.add(colors[neighbor - 1]);
            }

            // first available color
            for (int i = 1; i <= usedColors.size(); i++)
            {
                if (!usedColors.contains(i)) {
                    colors[current - 1] = i;
                    break;
                }
            }
        }

        for (int i = 0; i < colors.length; i++)
            if (colors[i] == 0)
                colors[i] = 1;

        return colors;
    }

    Map<Integer, Set<Integer>> buildAgencyList(int[][] paths)
    {
        // using a HashSet to avoid duplicates
        Map<Integer, Set<Integer>> adjacencyList = new HashMap<>();

        for (int[] path : paths)
        {
            Set<Integer> neighbors = adjacencyList.getOrDefault(path[0], new HashSet<>());
            neighbors.add(path[0]);
            neighbors.add(path[1]);
            adjacencyList.put(path[0], neighbors);

            Set<Integer> neighbors2 = adjacencyList.getOrDefault(path[1], new HashSet<>());
            neighbors2.add(path[0]);
            neighbors2.add(path[1]);
            adjacencyList.put(path[1], neighbors2);
        }

        return adjacencyList;
    }

    void printGraph(Map<Integer, Set<Integer>> adjacencyList)
    {
        for(int current : adjacencyList.keySet())
        {
            System.out.print(current + ": ");
            for (int neighbor : adjacencyList.getOrDefault(current, new HashSet<>()))
            {
                System.out.print(neighbor + ",");
            }
            System.out.println();
        }
    }

    public static void runTests()
    {
        GreedyColoringGarden GNA = new GreedyColoringGarden();

        int[][] input0 = {
                {1, 2},
                {2, 3},
                {3, 4},
                {4, 1},
                {1, 3},
                {2, 4}
        };

        System.out.println(Arrays.toString(GNA.gardenNoAdj(4, input0)));

        int[][] input1 = {
                {1, 2},
                {2, 3},
                {3, 1}
        };

        System.out.println(Arrays.toString(GNA.gardenNoAdj(3, input1)));

        int[][] input2 = {
                {1, 2},
                {3, 4}
        };

        System.out.println(Arrays.toString(GNA.gardenNoAdj(4, input2)));

        int[][] input3 = {
                {1, 2},
                {2, 3},
                {3, 4},
                {4, 1},
                {1, 3},
                {2, 4}
        };

        System.out.println(Arrays.toString(GNA.gardenNoAdj(4, input3)));

        int[][] input4 = {
                {4, 1},
                {4, 2},
                {4, 3},
                {2, 5},
                {1, 2},
                {1, 5}
        };

        System.out.println(Arrays.toString(GNA.gardenNoAdj(5, input4)));

        int[][] input5 = {
                {7, 4},
                {3, 7},
                {1, 5},
                {5, 4},
                {7, 1},
                {3, 1},
                {4, 3},
                {6, 5}
        };

        System.out.println(Arrays.toString(GNA.gardenNoAdj(8, input5)));
    }

    public static void main(String[] args)
    {
        runTests();
    }
}
