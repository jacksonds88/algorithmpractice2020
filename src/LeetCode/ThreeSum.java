package LeetCode;

import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

public class ThreeSum {
    public static void main(String[] args)
    {
        int[] nums = {-1, 0, 1, 2, -1, -4};
        int[] nums2 = {-4,-2,-2,-2,0,1,2,2,2,3,3,4,4,6,6};
        int[] nums3 = {-1,0,1,0};

        List<List<Integer>> ret = threeSum(nums);
        List<List<Integer>> ret2 = threeSum(nums2);
        List<List<Integer>> ret3 = threeSum(nums3);

        print(ret);
        print(ret2);
        print(ret3);
    }

    public static void print(List<List<Integer>> ret)
    {
        for (List<Integer> integers : ret) {
            System.out.print(integers.get(0) + "," + integers.get(1) + "," + integers.get(2));
            System.out.println();
        }
        System.out.println("-------------------");
    }

    public static List<List<Integer>> threeSum(int[] nums) {
        Arrays.sort(nums);
        List<List<Integer>> ret = new LinkedList<>();
        HashMap<String, List<Integer>> map = new HashMap<>();

        for(int i = 0; i < nums.length; i++)
        {
            int j = i + 1;
            int k = nums.length - 1;
            //System.out.println(i + "/" + j + "/" + k);
            while(k > j)
            {
                int sum = nums[i] + nums[j] + nums[k];
                if(sum == 0)
                {
                    LinkedList<Integer> triplet = new LinkedList<>();
                    triplet.add(nums[i]);
                    triplet.add(nums[j]);
                    triplet.add(nums[k]);

                    String key = String.valueOf(triplet.get(0)) + triplet.get(1) + triplet.get(2);
                    boolean exists = map.containsKey(key);
                    if(!exists)
                    {
                        ret.add(triplet);
                        map.put(key, triplet);
                    }

                    j++;
                    k--;
                }
                else if (sum > 0)
                    k--;
                else// if (sum < 0)
                    j++;
            }
        }

        return ret;
    }
}
