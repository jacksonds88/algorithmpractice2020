package LeetCode;

import java.util.HashSet;
import java.util.Set;

public class LongestSubstringWithoutRepeatingCharacters {
    public int lengthOfLongestSubstring(String s) {
        if (s == null || s.isEmpty())
            return 0;

        Set<Character> H = new HashSet<>();
        int maxLength = 0;
        char[] C = s.toCharArray();
        for (int i = 0; i < C.length; i++)
        {
            H.clear();
            int length = 0;
            for(int j = i; j < C.length; j++)
            {
                char c = C[j];
                if (H.contains(c))
                {
                    break;
                }

                H.add(c);
                length++;
                if (length > maxLength)
                    maxLength = length;

            }
        }

        return maxLength;
    }

    // DOES NOT WORK for case dvdf
    public int lengthOfLongestSubstring2(String s) {
        if (s == null || s.isEmpty())
            return 0;

        Set<Character> H = new HashSet<>();
        int length = 0;
        int maxLength = 0;
        for (Character c : s.toCharArray())
        {
            if (H.contains(c))
            {
                H.clear();
                H.add(c);
                length = 1;
            }
            else
            {
                H.add(c);
                length++;
                if (length > maxLength)
                    maxLength = length;
            }
        }

        return maxLength;
    }

    public static void main(String[] args)
    {
        var L = new LongestSubstringWithoutRepeatingCharacters();
        System.out.println(L.lengthOfLongestSubstring("abcabcbb"));
        System.out.println(L.lengthOfLongestSubstring("bbbbb"));
        System.out.println(L.lengthOfLongestSubstring("pwwkew"));
        System.out.println(L.lengthOfLongestSubstring("aab"));
        System.out.println(L.lengthOfLongestSubstring(" "));
        System.out.println(L.lengthOfLongestSubstring("dvdf"));
        System.out.println(L.lengthOfLongestSubstring("asjrgapa"));
    }
}
