package LeetCode;

import java.util.*;

// Source: https://leetcode.com/problems/two-sum/submissions/

/*
Given an array of integers, return indices of the two numbers such that they add up to a specific target.

You may assume that each input would have exactly one solution, and you may not use the same element twice.

Similar to target sum
 */

public class TwoSum {
    public static void main(String[] args)
    {
        int[] input = {2, 7, 11, 19};
        //run(input, 9);

        input = new int[]{3, 2, 4};
        //run(input, 6);

        //input = new int[]{-3, 4, 3, 90};
        //run(input, 0);

        input = new int[]{-1, -2, -3, -4, -5};
        run(input, -8);
    }

    static void run(int[] nums, int target)
    {
        int[] ret = twoSum(nums, target);
        if (ret != null)
            System.out.println(ret[0] + ", " + ret[1]);
    }

    static int[] twoSumBruteForce(int[] nums, int target) {
        for (int i = 0 ; i < nums.length; i++)
            for (int j = i + 1; j < nums.length; j++)
                if (nums[i] + nums[j] == target)
                    return new int[]{i, j};

        return null;
    }

    // cannot use the same element twice to sum to target
    static int[] twoSum(int[] nums, int target) {
        HashMap<Integer, Integer> set = new HashMap<>();

        for (int i = 0; i < nums.length; i++)
            //if (nums[i] <= target) // does not work when target is a small or negative number like 0 or -8
                set.put(nums[i], i);

        for (int i = 0 ; i < nums.length; i++)
        {
            //if (nums[i] > target) // does not work when target is a small or negative number like 0 or -8
            //    continue;

            // INTERESTING trick here; do not forget!
            int targetDiff = target - nums[i];
            if (set.containsKey(targetDiff))
            {
                int j = set.get(targetDiff);
                if (i != j)
                    return new int[]{i, set.get(targetDiff)};
            }
        }

        return null;
    }
}
