package LeetCode;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Queue;

// https://leetcode.com/problems/maximum-depth-of-binary-tree/

public class MaximumDepthOfBinaryTree {
    public static class TreeNode {
      int val;
      TreeNode left;
      TreeNode right;

      TreeNode() {
      }

      TreeNode(int val) {
          this.val = val;
      }

      TreeNode(int val, TreeNode left, TreeNode right) {
          this.val = val;
          this.left = left;
          this.right = right;
      }
    }

    public int maxDepth(TreeNode root) {
        if (root == null)
            return 0;

        return maxDepth(root, 1);
    }

    public int maxDepth(TreeNode current, int depth) {
        int leftDepth = 0;
        int rightDepth = 0;

        if (current.left != null)
            leftDepth = maxDepth(current.left, depth + 1);

        if (current.right != null)
            rightDepth = maxDepth(current.right, depth + 1);

        if (leftDepth > 0 || rightDepth > 0)
            return Math.max(leftDepth, rightDepth);

        return depth;
    }

    public int maxDepthBFS(TreeNode root)
    {
        Queue<TreeNode> Q = new LinkedList<>();
        Q.add(root);
        HashMap<TreeNode, Integer> D = new HashMap<>();
        D.put(root, 1);

        int maxDepth = 0;
        while (!Q.isEmpty())
        {
            TreeNode current = Q.poll();
            int depth = D.get(current);

            if (current.left != null)
            {
                D.put(current.left, depth + 1);
                Q.add(current.left);
            }

            if (current.right != null)
            {
                D.put(current.right, depth + 1);
                Q.add(current.right);
            }

            if (depth > maxDepth)
                maxDepth = depth;
        }

        return maxDepth;
    }

    public static void main(String[] args)
    {
        // [3,9,20,null,null,15,7]

        TreeNode e = new TreeNode(7);
        TreeNode d = new TreeNode(15);
        TreeNode c = new TreeNode(20, e, d);
        TreeNode b = new TreeNode(9);
        TreeNode root = new TreeNode(3, b, c);

        MaximumDepthOfBinaryTree bt = new MaximumDepthOfBinaryTree();
        System.out.println(bt.maxDepthBFS(root));
    }
}
