package LeetCode;

import Core.ListNode;

import java.util.HashSet;
import java.util.Set;

// https://leetcode.com/problems/linked-list-cycle-ii/

public class LinkedListCycleII {
    public ListNode detectCycle(ListNode head) {
        Set<ListNode> visited = new HashSet<>();

        ListNode node = head;
        while (node != null) {
            if (visited.contains(node)) {
                return node;
            }
            visited.add(node);
            node = node.next;
        }

        return null;
    }

    public static void main(String[] args)
    {
        LinkedListCycleII LLC = new LinkedListCycleII();
        ListNode b = new ListNode(2);
        ListNode a = new ListNode(1, b);
        System.out.println(LLC.detectCycle(a)); // false
        b.next = a;
        System.out.println(LLC.detectCycle(a)); // true

        ListNode d2 = new ListNode(-4);
        ListNode c2 = new ListNode(0, d2);
        ListNode b2 = new ListNode(2, c2);
        ListNode a2 = new ListNode(3, b2);
        d2.next = b2;
        System.out.println(LLC.detectCycle(a2)); // true

        ListNode a3 = new ListNode(1);
        System.out.println(LLC.detectCycle(a3)); // false
    }
}
