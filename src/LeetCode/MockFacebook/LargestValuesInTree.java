package LeetCode.MockFacebook;

import Core.TreeNode;

import java.util.LinkedList;
import java.util.List;

public class LargestValuesInTree {
    public List<Integer> largestValues(TreeNode root) {
        List<Integer> largestRowValues = new LinkedList<>();

        if (root == null)
            return largestRowValues;

        List<List<Integer>> rowValues = new LinkedList<List<Integer>>();
        setRowValues(rowValues, root, 0);

        for (List<Integer> row : rowValues)
        {
            Integer max = row.get(0);
            for (Integer x : row)
                if (x > max)
                    max = x;

            largestRowValues.add(max);
        }

        return largestRowValues;
    }

    private void setRowValues(List<List<Integer>> rowValues, TreeNode current, int depth)
    {
        if (rowValues.size() <= depth)
            rowValues.add(new LinkedList<>());
        rowValues.get(depth).add(current.val);

        if (current.left != null)
            setRowValues(rowValues, current.left, depth + 1);

        if (current.right != null)
            setRowValues(rowValues, current.right, depth + 1);
    }

    public static void main(String[] args)
    {
        LargestValuesInTree l = new LargestValuesInTree();

        TreeNode f = new TreeNode(9);
        TreeNode e = new TreeNode(3);
        TreeNode d = new TreeNode(5);

        TreeNode c = new TreeNode(2, null, f);
        TreeNode b = new TreeNode(3, d, e);

        TreeNode a = new TreeNode(1, b, c);

        System.out.println(l.largestValues(a));

        c = new TreeNode(2);
        b = new TreeNode(3, null, c);
        a = new TreeNode(1, null, b);

        System.out.println(l.largestValues(a));

    }
}
