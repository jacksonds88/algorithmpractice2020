package LeetCode.MockFacebook;

// Write a function that takes a string as input and reverse only the vowels of a string.

public class ReverseVowels {
    public String reverseVowels(String s) {
        if (s == null || s.isEmpty())
            return s;

        char[] C = s.toCharArray();
        int i = 0, j = s.length() - 1;
        while (i < j)
        {
            for (; i < j; i++)
            {
                if(isVowel(C[i]))
                    break;
            }

            for (; i < j; j--)
            {
                if(isVowel(C[j]))
                    break;
            }

            swap(C, i, j);
            i++;
            j--;
        }

        return new String(C);
    }

    public void swap(char[] C, int i, int j)
    {
        char temp = C[i];
        C[i] = C[j];
        C[j] = temp;
    }

    public boolean isVowel(char c)
    {
        String vowels = "aeiouAEIOU";
        for(int i = 0; i < vowels.length(); i++)
        {
            if (c == vowels.charAt(i))
                return true;
        }

        return false;
    }

    public static void main(String[] args)
    {
        ReverseVowels rv = new ReverseVowels();
        //System.out.println(rv.reverseVowels("hello"));
        //System.out.println(rv.reverseVowels("leetcode"));
        System.out.println(rv.reverseVowels("aA"));
    }
}
