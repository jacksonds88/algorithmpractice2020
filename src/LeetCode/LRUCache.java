package LeetCode;

import java.util.HashMap;

// https://leetcode.com/problems/lru-cache/

public class LRUCache {
    public static void main(String[] args)
    {
        var cache = new LRUCache(2);

        cache.put(1, 1);
        cache.put(2, 2);
        cache.get(1);       // returns 1
        cache.put(3, 3);    // evicts key 2
        cache.get(2);       // returns -1 (not found)
        cache.put(4, 4);    // evicts key 1
        cache.get(1);       // returns -1 (not found)
        cache.get(3);       // returns 3
        cache.get(4);       // returns 4

        System.out.println(cache);
    }

    private HashMap<Integer, ListNode> cache;
    private ListNode head = new ListNode(Integer.MIN_VALUE, 0);
    private ListNode tail = new ListNode(Integer.MAX_VALUE, 0);
    private int capacity, size;

    public LRUCache(int capacity) {
        this.capacity = capacity;
        cache = new HashMap<>(capacity);
        head.next = tail;
        tail.previous = head;
    }

    int get(int key) {
        var node = cache.get(key);
        if (node == null)
            return - 1;

        // remove from current position
        node.previous.next = node.next;
        node.next.previous = node.previous;

        // add to tail
        node.next = tail;
        node.previous = tail.previous;
        tail.previous.next = node;
        tail.previous = node;

        return node.value;
    }

    void put(int key, int value)
    {
        var current = cache.get(key);
        if(current != null)
        {
            current.value = value;
            cache.put(key, current);
            get(key);
            return;
        }

        if(size >= capacity)
        {
            //System.out.println("Removing from head: " + head.next);
            var toRemove = head.next;

            head.next = toRemove.next;
            toRemove.next.previous = head;

            cache.remove(toRemove.key);
            size--;
        }

        //System.out.println("Adding to tail: " + key);
        add(key, value);
        //System.out.println(this);
        size++;
    }

    public void add(int key, int value)
    {
        var newNode = new ListNode(key, value);

        newNode.next = tail;
        newNode.previous = tail.previous;

        tail.previous.next = newNode;
        tail.previous = newNode;

        cache.put(key, newNode);
    }

    public String toString()
    {
        StringBuilder sb = new StringBuilder();
        var current = head;
        while (current != null)
        {
            if(current != head || current != tail)
                System.out.println(current);
            current = current.next;
        }

        return sb.toString();
    }

    class ListNode
    {
        Integer key;
        Integer value;
        ListNode next;
        ListNode previous;

        public ListNode(int key, int value)
        {
            this.key = key;
            this.value = value;
        }

        public String toString()
        {
            return key + ": " + value + ", next: " + (next != null ? next.key : null);
        }
    }
}
