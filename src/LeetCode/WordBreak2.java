package LeetCode;

import java.util.*;

// Word Break I: https://www.programcreek.com/2012/12/leetcode-solution-word-break/
// Word Break II: https://www.programcreek.com/2014/03/leetcode-word-break-ii-java/

public class WordBreak2 {
    public static void main(String[] args)
    {
        var wb = new WordBreak();

        List<String> list = Arrays.asList("cats", "dogs", "sand", "and", "cat");
        String s = "catsandog";

        //System.out.println(wb.recursion(s, list));

        list = Arrays.asList("apple", "pen");
        s = "applepenapple";

        //System.out.println(wb.bruteForce(s, list));

        list = Arrays.asList("car", "ca", "rs");
        s = "cars";

        System.out.println(dynamicProgramming(s, list));

        list = Arrays.asList("i", "a", "am", "ace");
        s = "iamace";

        System.out.println(dynamicProgramming(s, list));

        list = Arrays.asList("harry", "potter");
        s = "harrypotter";


        System.out.println(dynamicProgramming(s, list));

        //ArrayList<String> words = dfs(s, list);

        //for(String word : words) System.out.println(word);
    }

    public static boolean dynamicProgramming(String s, List<String> words)
    {
        Set<String> S = new HashSet<>(words);
        boolean[] pos = new boolean[s.length() + 1];
        pos[0] = true;

        for(int i = 0; i < s.length(); i++)
        {
            if (pos[i])
                for (int j = i + 1; j <= s.length(); j++)
                {
                    String sub = s.substring(i, j);
                    System.out.println(sub);
                    if (S.contains(sub))
                    {
                        System.out.println("FOUND: " + sub);
                        pos[j] = true;
                    }
                }
        }

        return pos[s.length()];
    }

    public static boolean dfs(String s, List<String> words)
    {
        if (s == null || s.isEmpty())
            return true;

        if (words == null || words.size() == 0)
            return false;

        System.out.println(s.length());

        return dfs(s, words, 0);
    }

    private static boolean dfs(String s, List<String> words, int current)
    {
        if (current == s.length()) // current index reached the end
            return true;

        for(String word : words)
        {
            if (current + word.length() > s.length())
                continue;

            String sub = s.substring(current, current + word.length());
            if (sub.equalsIgnoreCase(word))
            {
                boolean done = dfs(s, words, current + word.length());
                if (done)
                    return true;
            }
        }

        return false;
    }
}
