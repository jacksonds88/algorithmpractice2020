package LeetCode.Floodfill;

import java.util.*;

// https://leetcode.com/problems/flood-fill/

/*
An image is represented by a 2-D array of integers, each integer representing the pixel value of the image (from 0 to 65535).

Given a coordinate (sr, sc) representing the starting pixel (row and column) of the flood fill, and a pixel value newColor, "flood fill" the image.

To perform a "flood fill", consider the starting pixel, plus any pixels connected 4-directionally to the starting pixel of the same color as the starting pixel, plus any pixels connected 4-directionally to those pixels (also with the same color as the starting pixel), and so on. Replace the color of all of the aforementioned pixels with the newColor.

At the end, return the modified image.
 */

public class Floodfill {
    public static void main(String[] args)
    {
        int[][] matrix = new int[][]{{1,1,1},{1,1,0},{1,0,1}};
        test(matrix);
        matrix = new int [][]{{0, 0, 0},{0, 1, 1}};
        test(matrix);
    }

    public static void test(int[][] matrix)
    {
        var bfs = new Floodfill();
        bfs.floodFill(matrix, 0, 0, 9);
        print(matrix);
        System.out.println();
    }

    public static void print(int[][] matrix)
    {
        for(int i = 0; i < matrix.length; i++)
        {
            for(int j = 0; j < matrix[0].length; j++)
                System.out.println(matrix[i][j]);
            System.out.println();
        }
    }

    static int[][] directions = new int[][]{{1, 0}, {0, -1}, {-1, 0}, {0, 1}};

    public int[][] floodFill(int[][] image, int sr, int sc, int newColor) {
        int startingColor = image[sr][sc];

        if (startingColor == newColor) // Time limit exceeded without this!
            return image;

        Queue<Tuple> Q = new LinkedList<>();
        Q.add(new Tuple(sr, sc));

        while(!Q.isEmpty())
        {
            var c = Q.poll();
            image[c.x][c.y] = newColor;

            for (int[] d : directions) {
                int x = c.x + d[0];
                int y = c.y + d[1];
                if (!outOfBounds(x, y, image) && image[x][y] == startingColor)
                    Q.add(new Tuple(x, y));
            }
        }

        return image;
    }

    private boolean outOfBounds(int x, int y, int[][] M)
    {
        return x < 0 || x >= M.length || y < 0 || y >= M[0].length;
    }

    public class Tuple
    {
        int x;
        int y;

        public Tuple(int x, int y)
        {
            this.x = x;
            this.y = y;
        }
    }
}
