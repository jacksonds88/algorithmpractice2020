package LeetCode.Floodfill;

// https://leetcode.com/problems/island-perimeter/
/*
You are given a map in form of a two-dimensional integer grid where 1 represents land and 0 represents water.

Grid cells are connected horizontally/vertically (not diagonally). The grid is completely surrounded by water, and there is exactly one island (i.e., one or more connected land cells).

The island doesn't have "lakes" (water inside that isn't connected to the water around the island). One cell is a square with side length 1. The grid is rectangular, width and height don't exceed 100. Determine the perimeter of the island.
 */

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;

public class IslandPerimeter {
    public static void main(String[] args)
    {
        var M = new int[][]{{0,1,0,0},
                {1,1,1,0},
                {0,1,0,0},
                {1,1,0,0}};

        M = new int[][]{{1, 1}, {1,1}};

        test(M);
    }

    public static void test(int[][] matrix)
    {
        var ip = new IslandPerimeter();
        int perimeter = ip.islandPerimeter(matrix);
        System.out.println(perimeter);
        print(matrix);
        System.out.println();
    }

    public static void print(int[][] matrix)
    {
        for(int i = 0; i < matrix.length; i++)
        {
            for(int j = 0; j < matrix[0].length; j++)
                System.out.println(matrix[i][j]);
            System.out.println();
        }
    }

    // right, up, left, down
    static final int[][] DIRECTIONS = new int[][]{{1, 0}, {0, 1}, {-1, 0}, {0, -1}};
    static final int ISLAND = 1;
    static final int VISITED_ISLAND = 2;
    static final int WATER = 0;

    public int islandPerimeter(int[][] grid) {
        Queue<Tuple> Q = new LinkedList<>();
        var startingLocation = findStartingLocation(grid);
        if (startingLocation == null)
            return 0; // no islands found
        else {
            Q.add(startingLocation);
            grid[startingLocation.row][startingLocation.col] = VISITED_ISLAND;
        }

        int sum = 0;
        while(!Q.isEmpty())
        {
            var c = Q.poll();

            for(var d : DIRECTIONS)
            {
                var t = new Tuple(c.row + d[0], c.col + d[1]);
                if(!isOutOfBounds(t, grid) && grid[t.row][t.col] == ISLAND)
                {
                    Q.add(t);
                    grid[t.row][t.col] = VISITED_ISLAND;
                }
                else if(isUnvisitedWater(t, grid))
                    sum++;
            }
        }

        return sum;
    }

    private boolean isOutOfBounds(Tuple t, int[][] grid)
    {
        return t.row < 0 || t.row >= grid.length || t.col < 0 || t.col >= grid[t.row].length;
    }

    private boolean isUnvisitedWater(Tuple t, int[][] grid)
    {
        return isOutOfBounds(t, grid) || grid[t.row][t.col] == WATER;
    }

    private Tuple findStartingLocation(int[][] grid)
    {
        for (int i = 0; i < grid.length; i++)
            for (int j = 0; j < grid[i].length; j++)
                if (grid[i][j] == ISLAND)
                    return new Tuple(i, j);

         return null;
    }

    public class Tuple
    {
        int row;
        int col;
        int perimeter;
        public Tuple(int row, int col)
        {
            this.row = row;
            this.col = col;
        }

        public boolean equals(Object o)
        {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Tuple t = (Tuple) o;
            return row == t.row && col == t.col;
        }

        public int hashCode() {
            return 0;
        }

        public String toString()
        {
            return "row: " + row + ", col: " + col + "; perimeter: " + perimeter;
        }
    }
}
