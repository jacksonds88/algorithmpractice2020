package LeetCode;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;

// https://leetcode.com/problems/subarray-sums-divisible-by-k/

public class SubarrraySumsDivisibleByK {
    // https://leetcode.com/problems/subarray-sums-divisible-by-k/discuss/614453/Simple-Java-Solution-O(n)-using-HashMap
    public int subarraysDivByK(int[] A, int K) {
        HashMap<Integer,Integer> map = new HashMap<>();
        int count = 0;
        int sum = 0;
        map.put(0,1);
        for(int i =0; i < A.length; i++){
            sum += A[i];
            System.out.println("sum: " + sum);
            int temp = sum % K;
            if(temp < 0) temp += K;
            System.out.println("temp: " + temp);
            if(map.containsKey(temp)){
                count += map.get(temp);
                System.out.println("count: " + count);
            }
            map.put(temp, map.getOrDefault(temp, 0) + 1);
        }
        return count;
    }

    HashSet<String> V = new HashSet<>();
    // solves it for non-contiguous, but the requirements want contiguous sub-arrays
    public int subarraysDivByK2(int[] A, int K) {
        if (A == null || A.length == 0)
            return 0;

        Arrays.sort(A);
        V.add(Arrays.toString(A));
        int sum = 0;
        for (int x : A) {
            sum += x;
        }

        int count = sum % K == 0 ? 1 : 0;
        if (count == 1)
            System.out.println(Arrays.toString(A));
        for (int i = 0; i < A.length; i++)
        {
            int[] subArray = getNewSubarray(A, i);
            if (!V.contains(Arrays.toString(subArray)))
                count = count + subarraysDivByK(subArray, K);
        }

        return count;
    }

    int[] getNewSubarray(int[] A, int skip)
    {
        int[] B = new int[A.length - 1];
        int j = 0;
        for(int i = 0; i < A.length; i++)
        {
            if (i != skip)
                B[j++] = A[i];
        }

        return B;
    }

    public static void main(String[] args)
    {
        SubarrraySumsDivisibleByK s = new SubarrraySumsDivisibleByK();

        System.out.println(s.subarraysDivByK(new int[]{4, 5, 0, -2, -3, 1}, 5));
    }
}
