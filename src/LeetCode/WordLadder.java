package LeetCode;

import java.util.*;

public class WordLadder {
    public static void main(String[] args)
    {
        //var ladder = new String[]{"hot","dot","dog","lot","log","cog"};
        var list = new LinkedList<String>();
        list.add("hot");
        list.add("dot");
        list.add("dog");
        list.add("lot");
        list.add("cog");

        var beginWord = "hit";
        var endWord = "cog";

        var wordLadder = new WordLadder();

        System.out.println(wordLadder.ladderLength(beginWord, endWord, list)); // 5

        list = new LinkedList<String>();
        list.add("hot");
        list.add("dot");
        list.add("dog");
        list.add("lot");
        list.add("log");
        System.out.println(wordLadder.ladderLength(beginWord, endWord, list)); // 0

        beginWord = "a";
        endWord = "c";
        list = new LinkedList<String>();
        list.add("a");
        list.add("b");
        list.add("c");
        System.out.println(wordLadder.ladderLength(beginWord, endWord, list)); // 2

        beginWord = "hit";
        endWord = "cog";
        list = new LinkedList<String>();
        list.add("hot");
        list.add("dot");
        list.add("dog");
        list.add("lot");
        list.add("cog");
        System.out.println(wordLadder.ladderLengthRecursion(beginWord, endWord, list, 0));  // 5
    }

    public int ladderLength(String beginWord, String endWord, List<String> wordList) {
        Queue<WordNode> Q = new LinkedList<>();
        Q.add(new WordNode(beginWord, 1));

        while(!Q.isEmpty())
        {
            var current = Q.poll();
            System.out.println(current);

            var it = wordList.iterator();
            while(it.hasNext())
            {
                String next = it.next();
                if(!isGreaterThan1Diff(current.word, next))
                {
                    if(next.equals(endWord))
                        return current.step + 1;

                    Q.add(new WordNode(next, current.step + 1));
                    it.remove();
                }
            }
        }

        return 0;
    }

    public int ladderLengthRecursion(String beginWord, String endWord, List<String> wordList, int length)
    {
        if (beginWord.equals(endWord))
            return length + 1;

        for(var word : wordList)
            if (!isGreaterThan1Diff(beginWord, word))
            {
                var newWordList = new LinkedList<String>();
                for(var w : wordList)
                    if (!w.equals(word))
                        newWordList.add(w);
                return ladderLengthRecursion(word, endWord, newWordList, length+1);
            }

        return 0;
    }

    // all words have the same length so no need to check length
    private boolean isGreaterThan1Diff(String current, String next)
    {
        int diff = 0;
        for(int i = 0; i < current.length(); i++)
        {
            if(current.charAt(i) != next.charAt(i))
                diff++;

            if(diff > 1)
                return true;

        }

        return false;
    }

    class WordNode
    {
        String word;
        int step;

        public WordNode(String word, int step)
        {
            this.word = word;
            this.step = step;
        }

        public String toString()
        {
            return word + "; step: " + step;
        }
    }
}
