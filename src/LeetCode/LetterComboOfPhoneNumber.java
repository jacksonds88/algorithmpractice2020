package LeetCode;

import java.util.*;

public class LetterComboOfPhoneNumber {
    public static void main(String[] args)
    {
        LetterComboOfPhoneNumber lcop = new LetterComboOfPhoneNumber();
        var solution = lcop.letterCombinationsRecursion("234");

        for(var x : solution)
            System.out.println(x);
    }

    HashMap<String, String> map = new HashMap<>() {{
        put("2", "abc");
        put("3", "def");
        put("4", "ghi");
        put("5", "jkl");
        put("6", "mno");
        put("7", "pqrs");
        put("8", "tuv");
        put("9", "wxyz");
    }};

    public List<String> letterCombinations(String digits) {
        if(!isValidInput(digits))
            return new ArrayList<>();

        var combos = new LinkedList<String>();
        var combos2 = new LinkedList<String>();
        combos.add("");
        int d = 0;
        while(d < digits.length())
        {
            for(var combo : combos) // building the combo up
            {
                var chars = map.get(digits.charAt(d) + "");
                for(var c : chars.toCharArray()) // adding the next character to combo
                {
                    var comboNext = combo + c;
                    combos2.add(comboNext);
                }
            }

            combos = new LinkedList<>(combos2);
            combos2 = new LinkedList<>();
            d++;
        }

        var finalizedCombos = new LinkedList<String>();
        for(var combo : combos)
            if(combo.length() == digits.length())
                finalizedCombos.add(combo);

        return finalizedCombos;
    }

    public List<String> letterCombinationsRecursion(String digits) {
        if(!isValidInput(digits))
            return new ArrayList<>();

        var ret = new LinkedList<String>();
        comboBuilder(digits, "", ret);
        return ret;
    }

    public void comboBuilder(String digits, String comboIncomplete, List<String> combos) {
        if(comboIncomplete.length() == digits.length())
        {
            combos.add(comboIncomplete);
            return;
        }

        var d = digits.charAt(comboIncomplete.length()) + "";
        for(var c : map.get(d).toCharArray())
        {
            comboBuilder(digits, comboIncomplete + c, combos);
        }
    }

    boolean isValidInput(String digits)
    {
        if (digits == null || digits.trim().isEmpty())
            return false;

        for(var c : digits.toCharArray())
            if (!Character.isDigit(c) || c == '1')
                return false;

        return true;
    }
}
