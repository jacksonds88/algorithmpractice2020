import java.util.Arrays;
import java.util.LinkedList;

public class KClosestElements {
    public static int[] findKClosestElements(int[] arr, int k, int x)
    {
        return findKClosestElements(arr, k, x, 0, arr.length - 1);
    }

    public static int[] findKClosestElements(int[] arr, int k, int x, int l, int r)
    {
        int range = (r - l) / 2;
        int mid = range + l;
        if (arr[mid] == x || range <= 1)
        {
            LinkedList<Integer> L = new LinkedList<>();

            int i = mid;
            int j = mid - 1;
            while (k > 0)
            {
                int tempK = k;
                if (i < arr.length)
                {
                    L.add(arr[i]);
                    i++;
                    k--;
                }

                if (j >= 0 && k > 0)
                {
                    L.addFirst(arr[j]);
                    j--;
                    k--;
                }

                // to check k is larger than arr.length
                if (tempK == k)
                    break;
            }

            int[] ret = new int[L.size()];
            int count = 0;
            for (Integer val : L)
            {
                ret[count++] = val;
            }

            return ret;
        }
        else if (arr[mid] < x)
        {
            return findKClosestElements(arr, k, x, mid + 1, r);
        }
        else
        {
            return findKClosestElements(arr, k, x, l, mid - 1);
        }
    }

    public static void main(String[] args)
    {
        int[] arr = {1, 2, 3, 4, 5};
        System.out.println(Arrays.toString(findKClosestElements(arr, 4, 3)));

        System.out.println(Arrays.toString(findKClosestElements(arr, 4, -1)));

        System.out.println(Arrays.toString(findKClosestElements(arr, 4, 8)));

        System.out.println(Arrays.toString(findKClosestElements(arr, 2, 3)));

    }
}
