import java.util.Stack;

// https://www.youtube.com/watch?v=kHWy5nEfRIQ
public class TowerHopper {

    public static boolean traverseIteration(int[] T)
    {
        if (T == null || T.length == 0)
            return true;

        Stack<Integer> S = new Stack<>();
        int current = 0;
        S.add(current);
        while (!S.isEmpty()){
            current = S.pop();

            if (current == T.length - 1)
                return true;

            if (T[current] > 0)
            {
                for(int i = current + 1; i <= Math.min(current + T[current], T.length - 1); i++)
                {
                    if (T[current] >= T[i])
                    {
                        S.add(i);
                    }
                }
            }

        }

        return false;
    }

    public static boolean traverse(int[] T)
    {
        if (T == null || T.length == 0)
            return true;

        return traverse(T, 0);
    }

    static boolean traverse(int[] T, int current)
    {
        if (current == T.length - 1)
            return true;

        int height = T[current];
        int range = Math.min(current + height, T.length - 1);
        for (int i = current + 1; i <= range; i++) {
            if (height > 0 && height >= T[i] /*&& traverse(T, i)*/)
            {
                boolean foundPath = traverse(T, i);
                if (foundPath)
                    return true;
            }
        }

        return false;
    }

    public static void main(String[] args)
    {
        int[] towers = {4, 2, 0, 0, 2, 0};
        System.out.println(traverseIteration(towers));

        towers = new int[]{4, 2, 0, 0, 0, 1};
        System.out.println(traverseIteration(towers));

        towers = new int[]{1, 1, 1, 1, 1, 1};
        System.out.println(traverseIteration(towers));
    }
}
