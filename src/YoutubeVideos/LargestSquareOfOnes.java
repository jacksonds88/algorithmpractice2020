package YoutubeVideos;

// Largest Square of 1's in A Matrix (Dynamic Programming)
// Amazon interview question
// https://www.youtube.com/watch?v=FO7VXDfS8Gk

import java.util.Arrays;

public class LargestSquareOfOnes {
    public static int solve(int[][] M)
    {
        if (!validate(M))
            return 0;

        int[][] D = initialize(M);

        int max = 0;
        for (int i = 1; i < M.length; i++)
        {
            for (int j = 1; j < M[i].length; j++)
            {
                if (M[i][j] == 1)
                {
                    int[] lookAround =
                            {
                                D[i-1][j-1],
                                D[i][j-1],
                                D[i-1][j]
                            };

                    int min = lookAround[0];
                    for (int k = 1; k < lookAround.length; k++)
                    {
                        if (min > lookAround[k])
                        {
                            min = lookAround[k];
                        }
                    }
                    D[i][j] = min + M[i][j];

                    if (max < D[i][j])
                    {
                        max = D[i][j];
                    }
                }
            }
        }

        printMatrix(D);

        return max;
    }

    public static boolean validate(int[][] M)
    {
        if (M == null || M.length == 0)
            return false;

        int numColumns = M[0].length;
        for (int i = 0; i < M.length; i++)
        {
            if (M[i] == null || M[i].length != numColumns)
                return false;
        }

        return true;
    }

    public static int[][] initialize(int[][] M)
    {
        int[][] M2 = new int[M.length][M[0].length];
        for (int i = 0; i < M[0].length; i++)
        {
            M2[0][i] = M[0][i];
        }

        for (int i = 0; i < M.length; i++)
        {
            M2[i][0] = M[i][0];
        }

        return M2;
    }

    public static void printMatrix(int[][] M)
    {
        for(int[] m : M)
        {
            System.out.println(Arrays.toString(m));
        }
    }

    public static void main(String[] args)
    {
        int[][] M = {
            { 1, 1, 0, 1, 0 },
            { 0, 1, 1, 1, 0 },
            { 1, 1, 1, 1, 0 },
            { 0, 1, 1, 1, 1 }
        };
        System.out.println(solve(M));
    }
}
