package Interviews.Twitter;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class PhoneDigitToLetterCombos {

    public static void main(String[] args)
    {
        Map<Character, String> map = new HashMap<>() {{
            put('0', "0");
            put('1', "1");
            put('2', "abc");
            put('3', "def");
            put('4', "ghi");
            put('5', "jkl");
            put('6', "mno");
            put('7', "pqrs");
            put('8', "tuv");
            put('9', "wxyz");
        }};

        printTest("", map);
        printTest("1", map);
        printTest("12", map);
        printTest("23", map);
        printTest("234", map);
    }

    public static void printTest(String digits, Map<Character, String> map)
    {
        var solution = solve(digits, map);

        StringBuilder sb = new StringBuilder((int)Math.pow(digits.length(), 3));
        for(var x : solution)
            sb.append(x).append(",");
        String[] splitString = sb.toString().split(",");
        System.out.println(String.join(",", splitString));
    }

    public static List<String> solve(String digits, Map<Character, String> map)
    {
        return solve(digits, map, 0, "");
    }

    public static List<String> solve(String digits, Map<Character, String> map, int position, String word)
    {
        List<String> solution = new LinkedList<>();
        if (word.length() == digits.length())
        {
            solution.add(word);
            return solution;
        }


        for (char c : map.get(digits.charAt(position)).toCharArray())
        {
            solution.addAll(solve(digits, map, position + 1, word + c));
        }

        return solution;
    }
}
