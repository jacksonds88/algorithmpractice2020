package Interviews.TikTok;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class Permutations {
    // given input 1, 2, 3
    // [1] + [2,3]
    // [1] + [3,2]
    // [2] + [1,3]
    // ...
    public static List<List<Integer>> getPermutations(List<Integer> input)
    {
        List<List<Integer>> ret = new LinkedList<>();

        if (input == null || input.size() <= 1)
        {
            if (input != null)
                ret.add(input);
            return ret;
        }

        for (Integer x : input)
        {
            // input is [1, 2, 3]
            LinkedList<Integer> subList = new LinkedList<>(input);
            subList.remove(x); // remove [1], and permute [2, 3]

            for (List<Integer> subPerm : getPermutations(subList))
            {
                LinkedList<Integer> newPerm = new LinkedList<>();
                newPerm.addFirst(x);
                newPerm.addAll(subPerm);
                ret.add(newPerm);
            }
        }

        return ret;
    }

    // permuting with an array
    static ArrayList<int[]> getPermutations(int[] input, int index)
    {
        ArrayList<int[]> ret = new ArrayList<>();

        if (input == null || index >= input.length - 1)
        {
            ret.add(input);
            return ret;
        }

        for (int i = index; i < input.length; i++)
        {
            // input is [1, 2, 3]
            int[] subArray = Arrays.copyOf(input, input.length);
            swap(subArray, i, index); // [1] removed from [2, 3]

            // OR ret.addAll(getPermutations(subArray, index + 1));
            for (int[] subPerm : getPermutations(subArray, index + 1))
            {
                ret.add(subPerm);
            }
        }

        return ret;
    }

    static void swap(int[] A, int i, int j)
    {
        int temp = A[i];
        A[i] = A[j];
        A[j] = temp;
    }


    static void print(List<List<Integer>> P)
    {
        for (List<Integer> p : P)
        {
            String[] numbers = new String[p.size()];
            int i = 0;
            for (Integer x : p)
                numbers[i++] = x + "";
            System.out.println(String.join(",", numbers));
        }
    }

    static void print(ArrayList<int[]> P)
    {
        for (var p : P)
        {
            String[] numbers = new String[p.length];
            int i = 0;
            for (Integer x : p)
                numbers[i++] = x + "";
            System.out.println(String.join(",", numbers));
        }
    }

    public static void main(String[] args)
    {
        List<Integer> L = new LinkedList<>();
        L.add(1);
        L.add(2);
        L.add(3);
        L.add(4);

        print(getPermutations(L));

        int[] A = {1, 2, 3, 4};
        print(getPermutations(A, 0));
    }
}
