package Interviews.Facebook;

import java.util.Comparator;
import java.util.PriorityQueue;

public class MedianStream {
    // Add any helper functions you may need here
    void addToQueue(PriorityQueue<Integer> minHeap, PriorityQueue<Integer> maxHeap, int x)
    {
        if (maxHeap.isEmpty())
        {
            maxHeap.add(x);
            return;
        }
        else if (minHeap.isEmpty())
        {
            minHeap.add(x);
            return;
        }

        if (x <= maxHeap.peek())
        {
            if (maxHeap.size() > minHeap.size())
            {
                minHeap.add(maxHeap.remove());
            }

            System.out.println("Adding " + x + " to maxHeap");
            maxHeap.add(x);
        }
        else
        {
            if (minHeap.size() > maxHeap.size())
            {
                maxHeap.add(minHeap.remove());
            }

            System.out.println("Adding " + x + " to minHeap");
            minHeap.add(x);
        }

        if (minHeap.peek() < maxHeap.peek())
        {
            minHeap.add(maxHeap.remove());
            maxHeap.add(minHeap.remove());
        }
    }

    int[] findMedian(int[] arr) {
        // Write your code here
        PriorityQueue<Integer> minHeap = new PriorityQueue<>();
        PriorityQueue<Integer> maxHeap = new PriorityQueue<>(new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                return o2.compareTo(o1);
            }
        });

        if(arr.length == 1)
            return arr;

        int[] medians = new int[arr.length];

        for(int i = 0; i < arr.length; i++)
        {
            if (i % 2 == 1) // even because we start at 0
            {
                addToQueue(minHeap, maxHeap, arr[i]);
                System.out.println("EVEN: " + maxHeap.peek() + " " + minHeap.peek());
                medians[i] = (minHeap.peek() + maxHeap.peek()) / 2;
            }
            else
            {
                addToQueue(minHeap, maxHeap, arr[i]);
                System.out.println("ODD: " + maxHeap.peek() + ( !minHeap.isEmpty() ? " *" + minHeap.peek() : ""));
                medians[i] = maxHeap.size() > minHeap.size() ? maxHeap.peek() : minHeap.peek();
            }
        }

        return medians;
    }












    // These are the tests we use to determine if the solution is correct.
    // You can add your own at the bottom, but they are otherwise not editable!
    int test_case_number = 1;
    void check(int[] expected, int[] output) {
        int expected_size = expected.length;
        int output_size = output.length;
        boolean result = true;
        if (expected_size != output_size) {
            result = false;
        }
        for (int i = 0; i < Math.min(expected_size, output_size); i++) {
            result &= (output[i] == expected[i]);
        }
        char rightTick = '\u2713';
        char wrongTick = '\u2717';
        if (result) {
            System.out.println(rightTick + " Test #" + test_case_number);
        }
        else {
            System.out.print(wrongTick + " Test #" + test_case_number + ": Expected ");
            printIntegerArray(expected);
            System.out.print(" Your output: ");
            printIntegerArray(output);
            System.out.println();
        }
        test_case_number++;
    }
    void printIntegerArray(int[] arr) {
        int len = arr.length;
        System.out.print("[");
        for(int i = 0; i < len; i++) {
            if (i != 0) {
                System.out.print(", ");
            }
            System.out.print(arr[i]);
        }
        System.out.print("]");
    }
    public void run() {
        int[] arr_1 = {5, 15, 1, 3};
        int[] expected_1 = {5, 10, 5, 4};
        int[] output_1 = findMedian(arr_1);
        check(expected_1, output_1);

        int[] arr_2 = {2, 4, 7, 1, 5, 3};
        int[] expected_2 = {2, 3, 4, 3, 4, 3};
        int[] output_2 = findMedian(arr_2);
        check(expected_2, output_2);

        int[] arr_3 = {10, 4, 7};
        int[] expected_3 = {10, 7, 7};
        int[] output_3 = findMedian(arr_3);
        check(expected_3, output_3);

        // Add your own test cases here

    }
    public static void main(String[] args) {
        new MedianStream().run();
    }
}
