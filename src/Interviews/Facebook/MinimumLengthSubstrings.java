package Interviews.Facebook;

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;

public class MinimumLengthSubstrings {
    class C
    {
        String c;
        int count;
        int firstPosition = -1;
        int lastPosition = -1;
        LinkedList<Integer> positions = new LinkedList<>();

        public C(String c, int count)
        {
            this.c = c;
            this.count = count;
        }

        public boolean isValid()
        {
            return count <= positions.size();
        }

        public void add(int position)
        {
            if (firstPosition == -1)
                firstPosition = position;

            lastPosition = position;
            positions.add(position);
        }

        public boolean canBeReduced()
        {
            return count < positions.size();
        }

        public void reduce(int min, int max)
        {
            if (positions.size() <= 1)
                return;

            int next = 0;
            int prev = positions.size() - 1;
            for(int reducePotential = positions.size() - count; reducePotential >= 0; reducePotential--)
            {
                int p1 = positions.get(next);
                int p2 = positions.get(next + 1);
                int diff1 = p2 - p1;

                int p3 = positions.get(prev);
                int p4 = positions.get(prev - 1);
                int diff2 = p3 - p4;

                if (diff1 > diff2 && p2 >= min)
                {
                    positions.remove(p1);
                    firstPosition = p2;
                }
                else if (diff1 < diff2 && p3 <= max)
                {
                    positions.remove(p4);
                    lastPosition = p3;
                }
            }
        }
    }

    int minLengthSubstring(String s, String t) {
        HashMap<String, C> map = new HashMap<>();

        if(t.length() > s.length())
            return -1;

        for (var c : t.toCharArray())
        {
            var item = map.get(c + "");
            if(item != null)
            {
                item.count++;
            }
            else
            {
                String cString = c + "";
                map.put(cString, new C(cString, 1));
            }
        }

        for(int i = 0; i < s.length(); i++)
        {
            var item = map.get(s.charAt(i) + "");
            if(item != null)
            {
                item.add(i);
            }
        }

        for(var entry : map.entrySet())
            if(!entry.getValue().isValid())
                return -1;

        for(var c : map.values())
        {
            int min = c.firstPosition;
            int max = c.lastPosition;
            for(var d : map.values())
            {
                if (c.c.equalsIgnoreCase(d.c) && d.canBeReduced()) {
                    d.reduce(min, max);
                }
            }
        }

        int firstPosition = Integer.MAX_VALUE;
        int lastPosition = Integer.MIN_VALUE;
        for(var c : map.values()) {
            if(c.firstPosition < firstPosition)
                firstPosition = c.firstPosition;

            if(c.lastPosition > lastPosition)
                lastPosition = c.lastPosition;
        }

        //System.out.println(firstPosition);
        //System.out.println(lastPosition);
        return lastPosition - firstPosition + 1;
    }

    // These are the tests we use to determine if the solution is correct.
    // You can add your own at the bottom, but they are otherwise not editable!
    int test_case_number = 1;
    void check(int expected, int output) {
        boolean result = (expected == output);
        char rightTick = '\u2713';
        char wrongTick = '\u2717';
        if (result) {
            System.out.println(rightTick + " Test #" + test_case_number);
        }
        else {
            System.out.print(wrongTick + " Test #" + test_case_number + ": Expected ");
            printInteger(expected);
            System.out.print(" Your output: ");
            printInteger(output);
            System.out.println();
        }
        test_case_number++;
    }
    void printInteger(int n) {
        System.out.print("[" + n + "]");
    }
    public void run() throws IOException {
        String s_1 = "dcbefebce";
        String t_1 = "fd";
        int expected_1 = 5;
        int output_1 = minLengthSubstring(s_1, t_1);
        check(expected_1, output_1);

        String s_2 = "bfbeadbcbcbfeaaeefcddcccbbbfaaafdbebedddf";
        String t_2 = "cbccfafebccdccebdd";
        int expected_2 = -1;
        int output_2 = minLengthSubstring(s_2, t_2);
        check(expected_2, output_2);

        // Add your own test cases here

    }
    public static void main(String[] args) throws IOException {
        new MinimumLengthSubstrings().run();
    }
}
