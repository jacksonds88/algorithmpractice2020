package Interviews.Facebook;

import java.util.*;

/*
In this problem, you are given an integer N, and a permutation, P of the integers from 1 to N, denoted as (a_1, a_2, ..., a_N).
You want to rearrange the elements of the permutation into increasing order, repeatedly making the following operation:
Select a sub-portion of the permutation, (a_i, ..., a_j), and reverse its order.
Your goal is to compute the minimum number of such operations required to return the permutation to increasing order.
Signature
int minOperations(int[] arr)
Input
Size N is between 1 and 8
Array arr is a permutation of all integers from 1 to N
Output
An integer denoting the minimum number of operations required to arrange the permutation in increasing order
Example
If N = 3, and P = (3, 1, 2), we can do the following operations:
Select (1, 2) and reverse it: P = (3, 2, 1).
Select (3, 2, 1) and reverse it: P = (1, 2, 3).
output = 2
 */

public class MinimizingPermutations {

    // Add any helper functions you may need here
    class Pair
    {
        int s;
        int e;

        public Pair(int s, int e)
        {
            this.s = s;
            this.e = e;
        }

        public String toString()
        {
            return "start: " + s + ", end: " + e;
        }
    }

    public ArrayList<Pair> generateCombinations(int N)
    {
        var pairs = new ArrayList<Pair>();
        for(int i = 0; i < N - 1; i++)
        {
            for(int j = i + 1; j < N; j++)
            {
                pairs.add(new Pair(i, j));
            }
        }

        return pairs;
    }

    public void swap(int[] arr, int i, int j)
    {
        int temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }

    public boolean isSorted(int[] arr)
    {
        if(arr == null || arr.length <= 1)
            return true;

        for(int i = 1; i < arr.length; i++)
            if(arr[i - 1] > arr[i])
                return false;

        return true;
    }

    void reverse(int[] arr, ArrayList<Pair> pairs, HashSet<Integer> V, LinkedList<Integer> S, int steps)
    {
        if(isSorted(arr))
            return;

        for(int i = 0; i < pairs.size(); i++)
        {
            if(V.contains(i))
                continue;

            var pair = pairs.get(i);
            V.add(i);
            int[] newArr = Arrays.copyOf(arr, arr.length);

            System.out.println(pair);
            System.out.println("steps: " + steps);
            for(int j = pair.s, k = pair.e; j < k; j++, k--)
            {
                swap(newArr, j, k);
            }

            System.out.println(Arrays.toString(newArr));
            if(isSorted(newArr))
            {
                S.add(steps);
                V.remove(i);
                return;
            }
            else
                reverse(newArr, pairs, V, S, steps + 1);

            V.remove(i);
        }
    }

    int minOperations(int[] arr) {
        // Write your code here
        var pairs = generateCombinations(arr.length);

        if(isSorted(arr))
            return 0;

        var S = new LinkedList<Integer>();
        reverse(arr, pairs, new HashSet<>(), S, 1);
        for(var s : S)
            System.out.println(s);

        return 1;
    }












    // These are the tests we use to determine if the solution is correct.
    // You can add your own at the bottom, but they are otherwise not editable!
    int test_case_number = 1;
    void check(int expected, int output) {
        boolean result = (expected == output);
        char rightTick = '\u2713';
        char wrongTick = '\u2717';
        if (result) {
            System.out.println(rightTick + " Test #" + test_case_number);
        }
        else {
            System.out.print(wrongTick + " Test #" + test_case_number + ": Expected ");
            printInteger(expected);
            System.out.print(" Your output: ");
            printInteger(output);
            System.out.println();
        }
        test_case_number++;
    }
    void printInteger(int n) {
        System.out.print("[" + n + "]");
    }
    public void run() {

        /*int n_0 = 3;
        int[] arr_0 = {3, 1, 2};
        int expected_0 = 2;
        int output_0 = minOperations(arr_0);
        check(expected_0, output_0);*/

        int n_1 = 5;
        int[] arr_1 = {1, 2, 5, 4, 3};
        int expected_1 = 1;
        int output_1 = minOperations(arr_1);
        check(expected_1, output_1);

        /*int n_2 = 3;
        int[] arr_2 = {3, 1, 2};
        int expected_2 = 2;
        int output_2 = minOperations(arr_2);
        check(expected_2, output_2);*/

        // Add your own test cases here

    }
    public static void main(String[] args) {
        new MinimizingPermutations().run();
    }
}
