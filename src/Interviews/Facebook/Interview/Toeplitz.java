package Interviews.Facebook.Interview;

public class Toeplitz {

    static boolean isToeplitz(int[][] M)
    {
        // validation for edge cases
        if (M == null || M.length == 0)
            return false;

        for (var m : M)
            if (m == null || m.length == 0)
                return false;

        for (int i = 1; i < M.length; i++)
        {
            for (int j = 1; j < M[i].length; j++)
            {
                if (M[i-1][j-1] != M[i][j])
                    return false;
            }
        }

        return true;
    }

    public static void main(String[] args)
    {
        int[][] M = {
                {1, 3, 6, 4},
                {2, 1, 3, 6},
                {5, 2, 1, 3},
        };

        System.out.println(isToeplitz(M));

        M = new int[][]{
                {1, 3, 6, 4},
                {2, 1, 3, 6},
                {5, 2, 0, 3},
        };

        System.out.println(isToeplitz(M));
    }
}
