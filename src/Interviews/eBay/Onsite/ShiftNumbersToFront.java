package Interviews.eBay.Onsite;

import java.util.Arrays;

public class ShiftNumbersToFront {
    public static int[] solve(int[] numbers)
    {
        int next = 0;
        int zeroCount = 0;
        for (int i = 0; i < numbers.length; i++)
        {
            if (numbers[i] > 0)
            {
                numbers[next++] = numbers[i];
                zeroCount++;
            }
        }

        for (int i = numbers.length - 1; i >= zeroCount; i--)
        {
            numbers[i] = 0;
        }

        return numbers;
    }

    public static void main(String[] args)
    {
        int[] numbers = { 0, 2, 0, 0, 4, 6, 7, 0, 0}; // should look like this [2, 4, 6, 7, 0, 0, 0, 0, 0]
        solve(numbers);
        System.out.println(Arrays.toString(numbers));
    }
}
