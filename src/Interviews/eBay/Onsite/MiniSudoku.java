package Interviews.eBay.Onsite;

/*
    Instructions:

    Grid is 4x4
    Numbers range from 1 to 4
    Rows, columns, and 2x2 grids must contain 1-4

    It's not working due to the is2x2Valid() failing, but the idea is correct:
    https://www.baeldung.com/java-sudoku
 */

import java.util.Arrays;

public class MiniSudoku {


    public static int[][] sudoku()
    {
        int[][] grid = new int[4][4];
        sudoku(grid);
        return grid;
    }

    public static boolean sudoku(int[][] grid)
    {
        for(int row = 0; row < 4; row++)
        {
            for(int col = 0; col < 4; col++)
            {
                if (grid[row][col] == 0)
                {
                    for(int i = 1; i <= 4; i++)
                    {
                        grid[row][col] = i;
                        if (isValid(grid, row, col) && sudoku(grid))
                        {
                            return true;
                        }
                        grid[row][col] = 0;
                    }

                    return false;
                }
            }
        }

        return true;
    }

    public static boolean isValid(int[][] grid, int row, int col)
    {
        return isRowValid(grid, row)
                && isColValid(grid, col)
                && is2x2GridValid(grid);
    }

    public static boolean isRowValid(int[][] grid, int row)
    {
        int[] count = new int[4];
        for (int i = 0; i < grid[row].length; i++)
        {
            if (grid[row][i] > 0)
                count[grid[row][i] - 1]++;
        }

        for (int c : count)
            if (c > 1)
                return false;

        return true;
    }

    public static boolean isColValid(int[][] grid, int col)
    {
        int[] count = new int[4];
        for (int i = 0; i < grid.length; i++)
        {
            if (grid[i][col] > 0)
                count[grid[i][col] - 1]++;
        }

        for (int c : count)
            if (c > 1)
                return false;

        return true;
    }

    public static boolean is2x2GridValid(int[][] grid)
    {
        for (int row = 0; row < grid.length - 1; row++)
        {
            for (int col = 0; col < grid[row].length - 1; col++)
            {
                boolean valid = is2x2GridValid(grid, row, col);
                if (!valid)
                    return false;
            }
        }

        return true;
    }

    public static boolean is2x2GridValid(int[][] grid, int row, int col)
    {
        int[] count = new int[4];
        /*for (int i = 0; i < 4; i++)
        {
            int newRow = row;
            int newCol = col + (i % 2);
            if (col + i >= 2)
                newRow++;

            if (newRow < 4 && newCol < 4 && grid[newRow][newCol] > 0)
                count[grid[newRow][newCol] - 1]++;
        }*/

        if (grid[row][col] > 0)
            count[grid[row][col] - 1]++;
        if (grid[row+1][col] > 0)
            count[grid[row+1][col] - 1]++;
        if (grid[row][col+1] > 0)
            count[grid[row][col+1] - 1]++;
        if (grid[row+1][col+1] > 0)
            count[grid[row+1][col+1] - 1]++;

        System.out.println(Arrays.toString(count));

        for (int c : count)
            if (c > 1)
                return false;

        return true;
    }

    public static void main(String[] args)
    {
        int[][] grid = sudoku();

        for(int[] row : grid)
        {
            System.out.println(Arrays.toString(row));
        }
    }
}
