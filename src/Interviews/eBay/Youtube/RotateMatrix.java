package Interviews.eBay.Youtube;

import java.util.Arrays;

public class RotateMatrix {
    public static void rotateInPlace(int[][] M)
    {
        for(int i = 0; i < M.length - i; i++)
        {
            for (int j = i; j < M.length - i - 1; j++)
            {
                int a = M[i][j]; // starts at top left corner
                int b = M[j][M.length - i - 1]; // top right corner
                int c = M[M.length - i - 1][M.length - j - 1]; //
                int d = M[M.length - j - 1][i];

                M[i][j] = d;
                M[j][M.length - i - 1] = a;
                M[M.length - i - 1][M.length - j - 1] = b;
                M[M.length - j - 1][i] = c;
            }

            print(M);
        }
    }

    private static int[] getColumn(int[][] M, int column)
    {
        int[] newColumn = new int[M[column].length];

        for(int i = 0; i < M[column].length; i++)
        {
            newColumn[i] = M[i][column];
        }

        return newColumn;
    }

    public static void print(int[][] M)
    {
        for(int [] m : M)
        {
            System.out.println(Arrays.toString(m));
        }
        System.out.println();
    }

    public static void main(String[] args)
    {
        int[][] M = {
                {1, 2, 3, 4},
                {5, 6, 7, 8},
                {9, 10, 11, 12},
                {13, 14, 15, 16}
        };

        print(M);
        System.out.println();
        rotateInPlace(M);
        print(M);
    }
}
