package Interviews.eBay.Youtube;

import java.util.*;

public class Polynomial {
    public static class Term implements Comparable<Term>
    {
        int coefficient;
        int exponent;

        public Term(int coefficient, int exponent)
        {
            this.coefficient = coefficient;
            this.exponent = exponent;
        }

        public int calculate(int x)
        {
            if (coefficient == 0 || x == 0)
                return 0;
            return coefficient * (int)Math.pow(x, exponent);
        }

        public String toString()
        {
            return exponent > 0 ? coefficient + "x^" + exponent : coefficient + "x";
        }

        @Override
        public int compareTo(Term o) {
            if (this == o)
                return 0;

            return Integer.compare(this.exponent, o.exponent);
        }
    }

    // 3x^2 - 2x + 7 would have an input of [7, -2, 3] (reverse order)
    // inefficient because we could have many zero coefficients in the array
    public static int solve(int[] terms, int x)
    {
        if (terms == null || terms.length == 0)
            return 0;

        int total = 0;
        for (int i = 0; i < terms.length; i++)
        {
            if (terms[i] != 0)
                total += terms[i] * (int)Math.pow(x, i);
        }

        return total;
    }

    public static int solve(Term[] terms, int x)
    {
        if (terms == null || terms.length == 0)
            return 0;

        int total = 0;
        for (Term term : terms) {
            total += term.calculate(x);
        }

        return total;
    }

    // the key is the exponent and the value is the coefficient
    public static int solve(Map<Integer, Integer> terms, int x)
    {
        if (terms == null || terms.isEmpty())
            return 0;

        int total = 0;
        for (Map.Entry<Integer, Integer> t : terms.entrySet())
        {
            int exp = t.getKey();
            int coefficient = t.getValue();
            total += coefficient * Math.pow(x, exp);
        }

        return total;
    }

    public static String getPolynomialAsString(int[] terms)
    {
        if (terms == null || terms.length == 0)
            return "";

        StringBuilder sb = new StringBuilder();
        int n = terms.length - 1;
        sb.append(terms[n]);
        if (n > 1)
            sb.append("x^").append(n);

        for (int i = terms.length - 2; i > 0; i--)
        {
            if (terms[i] != 0)
            {
                if( terms[i] >= 0)
                    sb.append("+");

                sb.append(terms[i]).append("x^").append(i);
            }
        }

        if(terms[0] >= 0)
            sb.append("+");
        sb.append(terms[0]);

        return sb.toString();
    }

    public static String getPolynomialAsString(Term[] terms)
    {
        if (terms == null || terms.length == 0)
            return "";

        Arrays.sort(terms);
        StringBuilder sb = new StringBuilder();
        int n = terms.length - 1;
        if(n > 1 && terms[n].coefficient > 0)
            sb.append(terms[n].toString());

        for (int i = terms.length - 2; i >= 0; i--)
        {
            if (terms[i].coefficient > -1)
                sb.append("+");
            sb.append(terms[i].toString());
        }

        return sb.toString();
    }

    public static String getPolynomialAsString(TreeMap<Integer, Integer> terms)
    {
        if (terms == null || terms.isEmpty())
            return "";


        ArrayList<String> termStrings = new ArrayList<String>(terms.size());

        for (Map.Entry<Integer, Integer> t : terms.entrySet())
        {
            int exponent = t.getKey();
            int coefficient = t.getValue();

            String termString = exponent > 0 ? coefficient + "x^" + exponent : coefficient + "x";
            termStrings.add(termString);

        }

        StringBuilder sb = new StringBuilder();
        for(int i = termStrings.size() - 1; i >= 0; i--)
        {
            String termString = termStrings.get(i);
            if (termString.charAt(0) != '-')
            {
                sb.append("+");
            }
            sb.append(termString);
        }

        return sb.charAt(0) == '+' ? sb.substring(1, sb.length()) : sb.toString();
    }

    public static void main(String[] args)
    {
        int x = 2;
        int[] P = {7, -2, 3};

        System.out.println(getPolynomialAsString(P));
        System.out.println(solve(P, x));

        Term[] T =
                {
                    new Term(7, 0),
                    new Term(-2, 1),
                    new Term(3, 2)
                };

        System.out.println(getPolynomialAsString(T));
        System.out.println(solve(T, x));

        TreeMap<Integer, Integer> H = new TreeMap<>(); // TreeMap preserves order of keys
        H.put(0, 7);
        H.put(2, 3);
        H.put(1, -2);
        System.out.println(getPolynomialAsString(H));
        System.out.println(solve(H, x));

    }
}
