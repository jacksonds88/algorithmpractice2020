package Interviews.eBay.TechScreen;
/*

    2 Problems

    Given a SINGLY linked list of 1->2->3->4->5, return a list ordered as such:
    1->5->2->4->3

    Given a balanced binary tree, reverse all odd layers. For example:

           a
         b   c
      d   e   f  g
     h i j k l m n o

    Would become

           a
         c   b
      d   e   f  g
     o n m l k j i h

     Create a new tree. Do not modify the existing root
 */

import Core.BinarySearchTree;
import Core.TreeNode;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class ReorderTree {
    public static TreeNode reorderTree(TreeNode root)
    {
        TreeNode clonedRoot = BinarySearchTree.cloneTree(root);

        List<List<TreeNode>> layers = new LinkedList<>();
        setLayers(layers, clonedRoot, 0);

        Iterator<List<TreeNode>> layerIterator = layers.iterator();
        int depth = -1;
        while(layerIterator.hasNext())
        {
            List<TreeNode> layer = layerIterator.next();
            depth++;
            if(depth % 2 == 1)
                continue;

            LinkedList<Integer> reverseLayer = new LinkedList<>();
            for (TreeNode node : layer)
            {
                if (node != null)
                {
                    if (node.left != null)
                        reverseLayer.addFirst(node.left.val);

                    if (node.right != null)
                        reverseLayer.addFirst(node.right.val);
                }
            }

            Iterator<TreeNode> itLayer = layer.iterator();
            Iterator<Integer> itChildren = reverseLayer.iterator();
            while (itLayer.hasNext()) {
                TreeNode parent = itLayer.next();

                if (itChildren.hasNext() && parent.left != null)
                    parent.left.val = itChildren.next();
                if (itChildren.hasNext() && parent.right != null)
                    parent.right.val = itChildren.next();
            }

        }

        return clonedRoot;
    }

    private static void setLayers(List<List<TreeNode>> layers, TreeNode current, int depth)
    {
        if (current.left != null)
            setLayers(layers, current.left, depth + 1);

        while (layers.size() <= depth) {
            layers.add(new LinkedList<>());
        }
        layers.get(depth).add(current);

        if (current.right != null)
            setLayers(layers, current.right, depth + 1);
    }

    public static List<Integer> reorderList(List<Integer> list)
    {
        if (list == null)
            return new LinkedList<>();

        LinkedList<Integer> reverseList = new LinkedList<>();
        for(Integer x : list)
            reverseList.addFirst(x);

        Iterator<Integer> itForward = list.iterator();
        Iterator<Integer> itReverse = reverseList.iterator();
        int i = 0;
        int j = reverseList.size() - 1;
        List<Integer> reorderedList = new LinkedList<>();

        while (i <= j)
        {
            reorderedList.add(itForward.next());
            if(i != j)
                reorderedList.add(itReverse.next());

            i++;
            j--;
        }

        return reorderedList;
    }

    public static void print(List<Integer> list)
    {
        System.out.println(list.stream().map(Object::toString).collect(Collectors.joining(",")));
    }

    public static void main(String[] args)
    {
        List<Integer> list = new LinkedList<>();
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(5);

        print(reorderList(list));

        TreeNode o = new TreeNode(15);
        TreeNode n = new TreeNode(14);
        TreeNode m = new TreeNode(13);
        TreeNode l = new TreeNode(12);
        TreeNode k = new TreeNode(11);
        TreeNode j = new TreeNode(10);
        TreeNode i = new TreeNode(9);
        TreeNode h = new TreeNode(8);

        TreeNode g = new TreeNode(7, n, o);
        TreeNode f = new TreeNode(6, l, m);
        TreeNode e = new TreeNode(5, j, k);
        TreeNode d = new TreeNode(4, h, i);

        TreeNode c = new TreeNode(3, f, g);
        TreeNode b = new TreeNode(2, d, e);

        TreeNode root = new TreeNode(1, b, c);

        System.out.println(BinarySearchTree.toStringBFS(root));
        System.out.println(BinarySearchTree.toStringBFS(reorderTree(root)));
        // System.out.println(BinarySearchTree.toStringBFS(root)); to test cloneTree
    }
}
