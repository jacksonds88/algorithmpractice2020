package Interviews.eBay.TechScreen;

import java.io.*;
import java.util.*;

/*
 * To execute Java, please define "static void main" on a class
 * named Solution.
 *
 * If you need more classes, simply define them inline.
 */


// [2, 5, 1, 2, 3, 1, 6]
// find the len of longest increasing subsequence of the input arr
// 1,2,3,6 ==> 4

//dp[i]  len of LIS

class FindLargestIncreasingSequence {

    // O(n^3)
    public static int findLargestConsecutiveSequence(int[] numbers)
    {
        if (numbers == null || numbers.length == 0)
            return 0;

        int highestCount = 0;
        int[] M = new int[numbers.length];
        for (int i = 0; i < numbers.length - 1; i++) // O(n)
        {
            //int count = findLargestConsecutiveSequence(numbers, numbers[i], i, 1);
            int count = findLargestConsecutiveSequenceDP(numbers, numbers[i], i, 1, M);
            if (count > highestCount)
                highestCount = count;
        }

        return highestCount;
    }

    // 2^n
    // NO memoization
    private static int findLargestConsecutiveSequence(int[] numbers, int lastIncrease, int i, int count)
    {
        if (i >= numbers.length)
            return count;

        int temp = 0;
        if (numbers[i] > lastIncrease)
            temp = findLargestConsecutiveSequence(numbers, numbers[i], i+1, count + 1); // n

        // skip current number
        int temp2 = findLargestConsecutiveSequence(numbers, lastIncrease, i+1, count); // n
        int highestCount = Math.max(temp, temp2);

        return highestCount;
    }

    // n^2
    private static int findLargestConsecutiveSequenceDP(int[] numbers, int lastIncrease, int i, int count, int[] M)
    {
        if (i >= numbers.length)
            return count;

        int potentialHighCount = count + (numbers.length - (numbers.length - i));
        if (M[i] > potentialHighCount)
            return M[i];

        int temp = 0;
        if (numbers[i] > lastIncrease)
            temp = findLargestConsecutiveSequenceDP(numbers, numbers[i], i+1, count + 1, M); // n

        // skip current number
        int temp2 = findLargestConsecutiveSequenceDP(numbers, lastIncrease, i+1, count, M); // n
        int highestCount = Math.max(temp, temp2);
        M[i] = highestCount;

        return highestCount;
    }

    public static void main(String[] args) {
        int[] numbers = { 2, 5, 3, 4};

        System.out.println(findLargestConsecutiveSequence(numbers));
        System.out.println(findLargestConsecutiveSequence(new int[]{2, 5, 1, 2, 3, 1, 6}));
        System.out.println(findLargestConsecutiveSequence(new int[]{1, 2, 3, 4, 5, 6}));
        System.out.println(findLargestConsecutiveSequence(new int[]{6, 5, 4, 3, 2, 1}));
        System.out.println(findLargestConsecutiveSequence(new int[]{1, 5, 2, 4, 3, 6}));
    }
}
