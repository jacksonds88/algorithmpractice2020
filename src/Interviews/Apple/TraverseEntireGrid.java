package Interviews.Apple;

/*
Objective is to find a path that traverses every position on the grid without moving on a visited position
Classic backtracking problem
 */

public class TraverseEntireGrid {

    public static final int[][] DIR = new int[][]{
            {1, 0},
            {0, 1},
            {-1, 0},
            {0, -1}
    };

    public static void main(String[] args)
    {
        char[][] grid = new char[][]{
                {'O','X'},
                {'X','O'}
        };

        System.out.println(solve(grid));

        grid = new char[][]{
                {'O','O','X'},
                {'X','O','O'},
                {'X','X','O'},
        };

        System.out.println(solve(grid));


        grid = new char[][]{
                {'X','X','X'},
                {'X','O','O'},
                {'X','X','X'},
        };

        System.out.println(solve(grid));

        grid = new char[][]{
                {'X','X','O'},
                {'X','O','X'},
                {'O','X','X'},
        };

        System.out.println(solve(grid));
    }

    public static boolean solve(char[][] grid)
    {
        int numOs = 0;
        for (char[] row : grid)
        {
            for (char c : row)
            {
                if (c == 'O')
                    numOs++;
            }
        }

        for (int row = 0; row < grid.length; row++)
        {
            for (int col = 0; col < grid[row].length; col++)
            {
                if (grid[row][col] == 'O' && solve(grid, row, col, numOs))
                    return true;
            }
        }

        return false;
    }

    static boolean solve(char[][] grid, int row, int col, int numOs)
    {
        numOs--;
        grid[row][col] = 'X';
        if (numOs <= 0)
            return true;

        for (int[] direction : DIR)
        {
            int newRow = row + direction[0];
            int newCol = col + direction[1];
            if (isValidMove(grid, newRow, newCol))
            {
                if (solve(grid, newRow, newCol, numOs))
                    return true;
            }
        }

        grid[row][col] = 'O';
        return false;
    }

    static boolean isValidMove(char[][] grid, int row, int col)
    {
        return row >= 0 && row < grid.length && col >= 0 && col < grid[row].length && grid[row][col] == 'O';
    }
}
