package Interviews.Google.FirstRound;

public class RobotVacuumCleaner {

    static final int[][] DIR = {{0, 1}, {1, 0}, {0, -1}, {-1, 0}};
    static final String[] DIRECTION = {"right", "down", "left", "up"};

    static int[][] M = {
            {0, 1, 1, 0, 1},
            {0, 0, 0, 0, 0},
            {1, 0, 0, 1, 1},
            {1, 0, 1, 1, 1},
            {0, 0, 0, 0, 1},
    };
    static final int NOT_VISITED = 0;
    static final int OBSTACLE = 1;
    static final int VISITED = 2;

    static int steps = 0;

    static boolean isValidMove(int r, int c)
    {
        return r >= 0 && r < M.length && c >= 0 && c < M[r].length && M[r][c] != OBSTACLE;
    }

    static class Robot
    {
        int r, c;
        int d;

        public Robot(int r, int c, int d)
        {
            this.r = r;
            this.c = c;
            this.d = d;
        }

        // move()
        // turnLeft()
        // turnRight()
        // vacuum()

        public boolean move()
        {
            int r = this.r + DIR[d][0];
            int c = this.c + DIR[d][1];
            if (isValidMove(r, c))
            {
                this.r = r;
                this.c = c;
                return true;
            }

            return false;
        }

        public void turnLeft()
        {
            d = (d - 1) % DIR.length;
            if (d < 0) d += DIR.length;
        }

        public void turnRight()
        {
            d = (d + 1) % DIR.length;
        }

        public void vacuum()
        {
            M[r][c] = VISITED;
        }

        public String toString()
        {
            return "(" + r + "," + c + "): " + DIRECTION[d];
        }
    }

    // backtracking traversal algorithm
    public static int solution(Robot robot, int dir, int r, int c)
    {
        System.out.println("________________________________________________________________________");
        System.out.println("________________________________________________________________________");

        if (steps > 50)
            System.exit(0);
        steps++;

        robot.vacuum();
        int oppositeDirection = (dir + 2) % DIR.length;

        print(robot, dir);

        System.out.println("Look around");
        reorientDirection(robot, 0, dir);
        for(int i = 0; i < 4; i++)
        {
            int nextR = r + DIR[i][0];
            int nextC = c + DIR[i][1];

            System.out.println();
            System.out.println("State Position(" + r + ", " + c + ")");
            System.out.println("Robot Position(" + robot.r + ", " + robot.c + ")");
            System.out.println("IsValidMove(" + nextR + ", " + nextC + ") " + isValidMove(nextR, nextC));
            System.out.println("Opposite Direction: " + DIRECTION[oppositeDirection]);
            System.out.println("CheckDirection: " + DIRECTION[i]);
            System.out.println("Robot Direction: " + DIRECTION[robot.d]);
            System.out.println();
            if(isValidMove(nextR, nextC))
            {
                System.out.println("M[nextR][nextC] = " + M[nextR][nextC]);
                if (M[nextR][nextC] == NOT_VISITED)
                {
                    System.out.println("Robot direction: " + DIRECTION[robot.d]);
                    boolean robotMoved = robot.move();
                    System.out.println("robot.move(): " + robotMoved);
                    if(robotMoved)
                    {
                        dir = solution(robot, i, nextR, nextC);

                        System.out.println("Reorient direction: lastDirection=" + DIRECTION[i] + " / currentDirection=" + DIRECTION[dir]);
                        System.out.println(robot);
                        System.out.println();
                        reorientDirection(robot, i, dir);
                    }
                }
            }

            robot.turnRight();
        }
        robot.turnLeft(); // undo last turn

        // move back to last position
        System.out.println("Go back to last position in direction: " + DIRECTION[oppositeDirection]);
        reorientDirection(robot, oppositeDirection, 3);

        var robotMoved = robot.move();
        System.out.println("Robot moved back: " + robotMoved);
        print();
        return oppositeDirection;
    }

    public static void reorientDirection(Robot robot, int targetDirection, int currentDirection)
    {
        System.out.println("reorientDirection (START): " + DIRECTION[targetDirection] + " / " + DIRECTION[currentDirection] + " / " + DIRECTION[robot.d]);

        for (int i = 0; i < DIR.length; i++)
        {
            if (targetDirection == currentDirection)
            {
                System.out.println("reorientDirection (END): " + DIRECTION[targetDirection] + " / " + DIRECTION[currentDirection] + " / " + DIRECTION[robot.d]);
                return;
            }

            robot.turnRight();
            currentDirection = (currentDirection + 1) % DIR.length;
        }
    }

    public static void print()
    {
        for(int i = 0; i < M.length; i++)
        {
            for(int j = 0; j < M[i].length; j++)
            {
                System.out.print(M[i][j]);
            }
            System.out.println();
        }
        System.out.println();
    }

    public static void print(Robot robot, int dir)
    {
        System.out.println("-----------------------------------------------");
        System.out.println("STEP: " + steps);
        System.out.println(robot);
        System.out.println("State direction: " + DIRECTION[dir]);

        for(int i = 0; i < M.length; i++)
        {
            for(int j = 0; j < M[i].length; j++)
            {
                System.out.print(M[i][j]);
            }
            System.out.println();
        }
        System.out.println();

        System.out.println("-----------------------------------------------");
    }

    public static void main(String[] args)
    {

        //System.out.println(-2 % 4);

        int direction = 0;
        int r = 0;
        int c = 0;
        Robot robot = new Robot(r, c, direction);
        solution(robot, direction, r, c);

        print();
    }
}
