package Interviews.Google.FirstRound;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;

public class Bricks {

    static class Pair
    {
        int r, c;

        public Pair(int r, int c)
        {
            this.r = r;
            this.c = c;
        }

        public String toString()
        {
            return "(" + r + "," + c + ")";
        }
    }

    final static int[][] DIR = {
            {1, 0}, // down
            {0, 1}, // right
            {-1, 0}, // up
            {0, -1} // left
    };

    final static int BRICK = 1;
    final static int SPACE = 0;


    public static LinkedList<Pair> solutionBFS(int[][] M, int r, int c)
    {
        LinkedList<Pair> ret = new LinkedList<>();
        Pair start = new Pair(r, c);
        Queue<Pair> Q = new LinkedList<>();
        HashSet<String> V = new HashSet<>();
        M[start.r][start.c] = SPACE;

        for (int[] d : DIR) {
            int adjR = start.r + d[0];
            int adjC = start.c + d[1];
            if (M[adjR][adjC] == BRICK) {
                Pair adjStart = new Pair(adjR, adjC);
                Q.add(adjStart);
                V.add(adjStart.toString());
            }

            LinkedList<Pair> route = new LinkedList<>();
            while(!Q.isEmpty())
            {
                Pair current = Q.poll();
                route.add(current);

                for (int[] d2 : DIR) {
                    Pair next = new Pair(current.r + d2[0], current.c + d2[1]);
                    if (inBound(M, next) && M[next.r][next.c] == BRICK && !V.contains(next.toString())) {
                        Q.add(next);
                        V.add(next.toString());
                    }
                }
            }

            if(!touchesGround(M, route))
                ret.addAll(route);
        }

        return ret;
    }

    static boolean inBound(int[][] M, Pair pair)
    {
        return pair.r >= 0 & pair.r < M.length && pair.c >= 0 && pair.c < M.length;
    }

    static boolean touchesGround(int[][] M, LinkedList<Pair> route)
    {
        for(Pair p : route)
            if (p.r == M.length - 1 && M[p.r][p.c] == BRICK) // the brick part is redundant
                return true;

        return false;
    }


    // SOMETHING DOES NOT WORK HERE!!!!!!
    public static LinkedList<Pair> solution(int[][] M, int r, int c)
    {
        if(!isValid(M, r, c))
            return new LinkedList<>();

        boolean[] touchFloor = new boolean[4];
        ArrayList<LinkedList<Pair>> B = new ArrayList<>(touchFloor.length);
        for(int i = 0; i < touchFloor.length; i++)
        {
            LinkedList<Pair> bricks = new LinkedList<>();
            int nr = r + DIR[i][0];
            int nc = c + DIR[i][1];
            touchFloor[i] = traverse(bricks, M, nr, nc);
            B.add(bricks);
        }

        System.out.println("Touched floor?");
        for(boolean b : touchFloor)
            System.out.println(b);

        LinkedList<Pair> ret = new LinkedList<>();
        for(int i = 0; i < touchFloor.length; i++)
            if(touchFloor[i])
            {
                B.remove(i);
            }

        System.out.println("Touched floor?");
        for(LinkedList<Pair> pairs : B)
        {
            for (Pair p : pairs)
            {
                System.out.print(p + ", ");
                ret.add(p);
            }

            System.out.println();
        }

        System.out.println(ret.size());
        return ret;
    }

    public static boolean traverse(LinkedList<Pair> bricks, int[][] M, int r, int c)
    {
        if(M[r][c] == 0)
            return false;

        System.out.println(r + " | " + c);
        Pair p = new Pair(r, c);
        bricks.add(p);
        System.out.println(bricks.size());
        boolean touchedFloor = (r == M.length - 1);
        for(int i = 0; i < DIR.length; i++)
        {
            M[r][c] = 2;
            int nr = r + DIR[i][0];
            int nc = c + DIR[i][1];
            if (isValid(M, nr, nc) && M[nr][nc] == BRICK){
                touchedFloor = traverse(bricks, M, nr, nc) || touchedFloor;
            }
        }

        return touchedFloor;
    }

    public static boolean isValid(int[][] M, int r, int c)
    {
        return r >= 0 && r < M.length && c >= 0 && c < M[r].length;
    }

    static void print(int[][] M)
    {
        for(int i = 0; i < M.length; i++)
        {
            for(int j = 0; j < M[i].length; j++)
            {
                System.out.print(M[i][j]);
            }
            System.out.println();
        }
    }

    public static void main(String[] args)
    {
        int[][] M = {
                { 1, 1, 1, 1, 1, 1},
                { 0, 0, 0, 1, 0, 0},
                { 0, 0, 0, 1, 0, 0},
                { 0, 1, 1, 1, 0, 0},
                { 0, 0, 0, 1, 0, 0},
                { 1, 1, 1, 1, 1, 1}
        };

        LinkedList<Pair> removed = solutionBFS(M, 3, 3);
        for(Pair p : removed)
            M[p.r][p.c] = 0;

        print(M);
    }
}
