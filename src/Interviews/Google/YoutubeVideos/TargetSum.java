package Interviews.Google.YoutubeVideos;

// Source: https://leetcode.com/problems/two-sum/submissions/

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class TargetSum {

    static int[] targetSum(int[] nums, int target)
    {
        if (nums == null || nums.length < 2)
            return null;

        HashMap<Integer, Integer> M = new HashMap<>();
        for(int i = 0; i < nums.length; i++)
        {
            M.put(nums[i], i);
        }

        for(Map.Entry<Integer, Integer> entry : M.entrySet())
        {
            int key = target - entry.getKey();
            if (M.containsKey(key))
                return new int[]{entry.getValue(), M.get(key)};
        }

        return null;
    }

    static class Pair implements Comparable
    {
        int value, index;

        public Pair(int value, int index)
        {
            this.value = value;
            this.index = index;
        }

        @Override
        public int compareTo(Object o) {
            Pair p = (Pair)o;
            return Integer.compare(value, p.value);
        }
    }

    // time complexity is O(n log n) because of the sort
    static int[] targetSumWithoutHash(int[] nums, int target)
    {
        if (nums == null || nums.length < 2)
            return null;

        Pair[] pairs = new Pair[nums.length];
        for(int i = 0; i < pairs.length; i++)
        {
            pairs[i] = new Pair(nums[i], i);
        }

        Arrays.sort(pairs);
        int i = 0;
        int j = nums.length - 1;

        while (i < j)
        {
            if (pairs[i].value + pairs[j].value > target)
                j--;
            else if (pairs[i].value + pairs[j].value < target)
                i++;
            else
            {
                if (pairs[i].index < pairs[j].index)
                    return new int[]{pairs[i].index, pairs[j].index};

                return new int[]{pairs[j].index, pairs[i].index};
            }
        }

        return null;
    }

    static void run(int[] nums, int target)
    {
        int[] ret = targetSum(nums, target);
        if (ret != null)
            System.out.println(ret[0] + ", " + ret[1]);
    }

    static void runTrgetSumWithoutHash(int[] nums, int target)
    {
        int[] ret = targetSumWithoutHash(nums, target);
        if (ret != null)
            System.out.println(ret[0] + ", " + ret[1]);
    }

    public static void main(String[] args)
    {
        int[] input = {2, 7, 11, 19};
        runTrgetSumWithoutHash(input, 9);

        input = new int[]{3, 2, 4};
        runTrgetSumWithoutHash(input, 6);

        input = new int[]{-3, 4, 3, 90};
        runTrgetSumWithoutHash(input, 0);

        input = new int[]{-1, -2, -3, -4, -5};
        runTrgetSumWithoutHash(input, -8);
    }
}
