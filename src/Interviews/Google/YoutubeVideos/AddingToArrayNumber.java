package Interviews.Google.YoutubeVideos;

import java.util.Arrays;

public class AddingToArrayNumber {

    static int[] solution(int[] A, int input)
    {
        int inputLength = (input + "").length();
        int zeros = A.length - inputLength;
        StringBuilder padding = new StringBuilder(A.length - inputLength);
        for(int i = 0; i < zeros; i++)
            padding.append("0");
        String s = padding.toString() + input;

        int carry = 0;
        for(int i = s.length() - 1; i >= 0; i--)
        {
            int digit = Integer.parseInt(s.charAt(i) + "");
            int b = A[i] + digit + carry;
            carry = b >= 10 ? 1 : 0;
            A[i] = b % 10;
        }

        if (carry > 0)
        {
            int[] B = new int[A.length + 1];
            B[0] = carry;
            for(int i = 1; i < B.length; i++)
                B[i] = A[i-1];
            return B;
        }

        return A;
    }

    // not well written
    static int[] addOne(int[] A)
    {
        int a = A[A.length - 1] + 1;
        if (a < 10)
        {
            return A;
        }
        A[A.length - 1] = a % 10;
        int carry = 1;

        for (int i = A.length - 2; i >= 0; i--)
        {
            a = A[i] + carry;
            carry = a >= 10 ? 1 : 0;
            if(carry == 0)
                break;
            A[i] = a % 10;
        }

        if (carry > 0)
        {
            int[] B = new int[A.length + 1];
            B[0] = carry;
            for(int i = 1; i < B.length; i++)
                B[i] = A[i-1];
            return B;
        }

        return A;
    }

    static int[] addOneBest(int[] A)
    {
        int carry = 1;
        for (int i = A.length - 1; i >= 0; i--)
        {
            int a = A[i] + carry;
            carry = a >= 10 ? 1 : 0;
            A[i] = a % 10;
            if(carry == 0)
                break;
        }

        if (carry > 0)
        {
            int[] B = new int[A.length + 1];
            B[0] = carry;
            for(int i = 1; i < B.length; i++)
                B[i] = A[i-1];
            return B;
        }

        return A;
    }

    public static void main(String[] args)
    {
        int[] A = { 1, 3, 2, 4 };
        System.out.println(Arrays.toString(solution(A, 9999)));
        A = new int[]{9, 9, 9};
        System.out.println(Arrays.toString(addOneBest(A)));
    }
}
