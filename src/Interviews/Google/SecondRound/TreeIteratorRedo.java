package Interviews.Google.SecondRound;

import Core.BinarySearchTree;
import Core.TreeNode;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;
import java.util.Stack;

public class TreeIteratorRedo {

    TreeNode root;
    TreeNode current;
    Stack<TreeNode> stack = new Stack<>();
    Set<TreeNode> V = new HashSet<>();
    int state = 0; // 0, 1, 2
    int sequence = 0;

    public TreeIteratorRedo(BinarySearchTree BST)
    {
        root = BST.getRoot();
    }

    public boolean hasNext()
    {
        //return getNext() != null;
        return true;
    }

    public void reset()
    {
        sequence = 0;
        current = null;
    }

    public TreeNode next()
    {
        current = getNext();
        if (current != null)
            V.add(current);
        return current;
    }

    private TreeNode getNext()
    {
        if (current == null)
        {
            if (sequence == 0)
            {
                stack.add(root);
                current = root;
            }
            else
            {
                return null;
            }
        }

        while (!stack.isEmpty() || current != null)
        {
            if (current == null)
                break;

            if (state == 0) // left
            {
                while(current.left != null && !V.contains(current.left))
                {
                    stack.add(current.left);
                    current = current.left;
                }

                state++;
            }
            else if (state == 1) // visit
            {
                state++;
                if (!V.contains(current))
                {
                    sequence++;
                    return current;
                }
            }
            else
            {
                if(current.right != null && !V.contains(current.right))
                {
                    stack.add(current.right);
                    current = current.right;
                }
                else
                {
                    current = !stack.empty() ? stack.pop() : null;
                }
                state = 0;
            }
        }

        return null;
    }

    public static void main(String[] args)
    {
        BinarySearchTree BST = new BinarySearchTree();
        BST.insert(5);
        BST.insert(3);
        BST.insert(7);
        BST.insert(1);
        BST.insert(4);

        System.out.println(BST.toString());

        TreeIteratorRedo ti = new TreeIteratorRedo(BST);

        for (int i = 0; i < 5; i++)
        {
            ti.getNext();
            TreeNode next = ti.next();
            System.out.println(next);
        }

        System.exit(0);

        System.out.println(ti.hasNext());
        System.out.println(ti.next());
        System.out.println();

        System.out.println(ti.hasNext());
        System.out.println(ti.next());
        System.out.println();

        System.out.println(ti.hasNext());
        System.out.println(ti.next());
        System.out.println();

        System.out.println(ti.hasNext());
        System.out.println(ti.next());
        System.out.println();

        System.out.println(ti.hasNext());
        System.out.println(ti.next());
        System.out.println();

        System.out.println(ti.hasNext());
        System.out.println(ti.next());
        System.out.println();

        System.out.println(ti.hasNext());
    }
}
