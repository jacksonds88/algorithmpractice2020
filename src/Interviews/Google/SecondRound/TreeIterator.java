package Interviews.Google.SecondRound;

import Core.BinarySearchTree;
import Core.TreeNode;

import java.util.HashSet;
import java.util.Stack;

public class TreeIterator {

    private static class State
    {
        static final int LEFT = 0;
        static final int MIDDLE = 1;
        static final int RIGHT = 2;

        static final int TOTAL_STATES = 3;
    }

    BinarySearchTree BST;
    Stack<TreeNode> S;
    HashSet<TreeNode> V;
    TreeNode next;
    int traverseState;

    public TreeIterator(BinarySearchTree BST)
    {
        this.BST = BST != null ? BST : new BinarySearchTree();
        S = new Stack<>();
        S.push(this.BST.getRoot());
        V = new HashSet<>();
    }

    public boolean hasNext()
    {
        return findNext(true) != null;
    }

    public TreeNode getNext()
    {
        next = findNext(false);
        V.add(next);
        return next;
    }

    private TreeNode findNext(boolean check)
    {
        if (BST.isEmpty() || (S.isEmpty() && !V.isEmpty())) // if BST is empty or if (stack is empty and visited is not)
            return null;

        TreeNode current = next != null ? next : BST.getRoot();
        Stack<TreeNode> tempS = new Stack<>();
        int initialTraverseState = traverseState;

        while (!S.isEmpty())
        {
            if (traverseState % State.TOTAL_STATES == State.LEFT)
            {
                while (current.hasLeft() && !V.contains(current.getLeft()))
                {
                    current = current.getLeft();
                    S.push(current);
                }
            }
            else if (traverseState % State.TOTAL_STATES == State.MIDDLE)
            {
                if (!V.contains(S.peek()))
                {
                    current = S.peek();
                    traverseState = (traverseState + 1) % State.TOTAL_STATES;
                    break;
                }
            }
            else if (traverseState % State.TOTAL_STATES == State.RIGHT)
            {
                if (current.hasRight() && !V.contains(current.getRight()))
                {
                    current = current.getRight();
                    S.add(current);
                }
                else
                {
                    tempS.push(S.pop());
                }
            }

            traverseState = (traverseState + 1) % State.TOTAL_STATES;
        }

        if (check)
        {
            traverseState = initialTraverseState;
            while(!tempS.isEmpty())
                S.push(tempS.pop());
        }

        if (current != null && !V.contains(current))
        {
            return current;
        }

        return null;
    }

    public static void main(String[] args)
    {
        BinarySearchTree BST = new BinarySearchTree();
        BST.insert(5);
        BST.insert(3);
        BST.insert(7);
        BST.insert(1);
        BST.insert(4);

        System.out.println(BST.toString());

        TreeIterator ti = new TreeIterator(BST);
        System.out.println(ti.hasNext());
        System.out.println(ti.getNext());
        System.out.println();

        System.out.println(ti.hasNext());
        System.out.println(ti.getNext());
        System.out.println();

        System.out.println(ti.hasNext());
        System.out.println(ti.getNext());
        System.out.println();

        System.out.println(ti.hasNext());
        System.out.println(ti.getNext());
        System.out.println();

        System.out.println(ti.hasNext());
        System.out.println(ti.getNext());
        System.out.println();

        System.out.println(ti.hasNext());
        System.out.println(ti.getNext());
        System.out.println();

        System.out.println(ti.hasNext());
    }
}
