package Interviews.Axon;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class BigMaterials2 {

    public static void main(String[] args)
    {
        double[] prices = { 5.40, 3.30, 5.00 };
        prices = new double[]{5.633023848823333,1.3396521505575998,5.697971559270048,8.5612092024298,9.121101646302495,4.444282680115714,8.194277017407684,7.542502706656737,0.42797137286716636,5.222456122582219,3.5103954280331506,8.989768110950111,0.8797425618538401,5.5696375867484305,7.968902345887181,4.202796763975519};
        var result = solution(prices);

        //[6, 1, 6, 9, 9, 4, 8, 8, 0, 5, 3, 9, 1, 6, 8, 4]
        System.out.println(Arrays.toString(result));
    }

    public static long[] solution(double[] prices) {
        // Type your solution here
        double sum = 0;
        long sumOfFloors = 0;
        ArrayList<Item> items = new ArrayList<>();
        ArrayList<Item> sortedItemsByDecimal = new ArrayList<>();
        for(int i = 0; i < prices.length; i++)
        {
            sum += prices[i];
            var item = new Item(prices[i], i);
            sumOfFloors += item.priceFloored;
            items.add(item);
            sortedItemsByDecimal.add(item);
        }

        Collections.sort(sortedItemsByDecimal); // sort in desecending order of decimal values

        //System.out.println(Arrays.toString(items.toArray()));
        //System.out.println(Arrays.toString(sortedItemsByDecimal.toArray()));

        long roundedTotal = Math.round(sum);
        long remainingDiff = roundedTotal - sumOfFloors;
        for(var item : sortedItemsByDecimal)
        {
            if(remainingDiff == 0)
                break;
            item.priceFloored++;
            remainingDiff--;

        }

        long[] solution = new long[items.size()];
        for(int i = 0; i < solution.length; i++)
            solution[i] = items.get(i).priceFloored;

        return solution;
    }

    static class Item implements Comparable<Item>
    {
        double price;
        int priceFloored;
        double priceDecimal;

        int index;

        public Item(double price, int index)
        {
            this.price = price;
            this.index = index;

            priceFloored = (int)Math.floor(price);
            priceDecimal = price - Math.floor(price);
        }

        @Override
        public int compareTo(Item i) {
            if (priceDecimal == i.priceDecimal)
            {
                if (index == i.index)
                    return 0;
                else if (index < i.index)
                    return -1;
                else
                    return 1;
            }
            else if (priceDecimal < i.priceDecimal)
                return 1;
            else
                return -1;
        }

        public String toString()
        {
            return "Price: " + price + "; Index: " + index;
        }
    }
}
