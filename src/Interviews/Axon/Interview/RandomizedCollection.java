package Interviews.Axon.Interview;

/*
public class RandomizedCollection {

    int size = 0;
    HashMap<Integer, Integer> map = new HashMap<Integer, Integer>();
    HashMap<Integer, Integer> reverseMap = new HashMap<Integer, Integer>();
    Random random = new Random();

    // Initialize your data structure here.
    public RandomizedCollection() {

    }

    //Inserts a value to the collection. Returns true if the collection did not already contain the specified element.
    public boolean insert(int val) {
        map.put(size, val);
        reverseMap.put(val, size);
        size++;
    }

    // Removes a value from the collection. Returns true if the collection contained the specified element.
    public boolean remove(int val) {
        Map.Entry<Integer, Integer> entry = reverseMap.remove(val);
        if (entry == null)
            return false;
        Map.Entry entry2 = map.remove(entry.key);
        entry2.getKey() = entry.getValue();
        reverseMap.put(val, entry2.getKey());
        map.put(entry2.getKey(), entry2.getValue());
        size--;
        return true;
    }

    // Get a random element from the collection.
    public int getRandom() throws EmptyDataStructureException {
        if(size <= 0)
            throw new EmptyDataStructureException;
        int r = random.nextInt(size);
        return map.get(r);
    }

    public static void main(String[] args) throws EmptyDataStructureException
    {
        TestForEmptyStructure();
    }

    // UNIT TESTS
    public static void TestForEmptyStructure()
    {
        try
        {
            RandomizedCollection rc = new RandomizedCollection();
            int x = rc.getRandom();
        } catch (EmptyDataStructureException e)
        {
            Assert.isTrue(true);
        }
        Assert.isTrue(false);
    }

    public static void TestForSize1()
    {
        int expected = 5;
        RandomizedCollection rc = new RandomizedCollection();
        insert(expected);
        int actual = getRandom();
        Assert.IsEquals(expected, actual);
    }

    public static void TestForMany()
    {
        int[] input = {5, 9, 14, 27, 4};
        HashSet<Integer> S = new HashSet<Integer>(input);
        RandomizedCollection rc = new RandomizedCollection();
        for(int i : input)
            insert(i);

        int gets = 200;
        for(int i = 0 ; i < gets; i++)
        {
            boolean found = S.contains(rc.getRandom());
            Assert.IsTrue(found);
        }
    }
}
*/