package Interviews.Axon.Interview;

import java.util.*;

public class MineSweeper {
    static int[][] filterMap;
    static int[][] map;

    // underlying real map that cannot be seen by user
    static final int EMPTY = 0;
    static final int BOMB = -1;

    // filter map
    static final int HIDDEN = 0;
    static final int REVEALED = 1;

    static final int VALID_MOVE = 0;
    static final int GAME_OVER = 1;
    static final int WON = 2;

    static int validMovesCounter = 0;
    static int bombCount = 0;

    public static int selectPosition(int row, int col)
    {
        if(validMovesCounter - bombCount == map.length * map[row].length)
            return WON;

        if(map[row][col] == BOMB)
            return GAME_OVER;

        revealOnMap(row, col);
        return VALID_MOVE;
    }

    public static void revealOnMap(int row, int col)
    {
        for(int i = row-1; i < row+1; i++)
        {
            for(int j = col-1; j < col+1; j++)
            {
                if(isValidLocation(i, j) && (map[row][col] != BOMB))
                    filterMap[i][j] = REVEALED;
            }
        }
    }

    public static void initializeMap(int N, int M, int B)
    {
        filterMap = new int[N][M];
        map = new int[N][M];

        // sets the bombs on the map
        Random r = new Random();
        for (int i = 0; i < B; i++)
        {
            int row = r.nextInt(N);
            int col = r.nextInt(M);
            while(map[row][col] == BOMB)
            {
                row = r.nextInt(N);
                col = r.nextInt(M);
            }
            map[row][col] = BOMB;
            setBombCountOnCell(row, col);
        }
    }

    public static void setBombCountOnCell(int row, int col)
    {
        for(int i = row-1; i < row+1; i++)
        {
            for(int j = col-1; j < col+1; j++)
            {
                if(isValidLocation(i, j) && (map[row][col] != BOMB))
                    map[i][j]++;
            }
        }
    }

    public static boolean isValidLocation(int row, int col)
    {
        return row >= 0 && row < map.length && col >= 0 && col < map[row].length;
    }

    public static void main(String[] args) {
        System.out.println("Hello, Py!");
        System.out.println("You're running Java!");
    }
}


