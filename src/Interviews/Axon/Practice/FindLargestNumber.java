package Interviews.Axon.Practice;

public class FindLargestNumber {

    public static void main(String[] args)
    {
        System.out.println(solution(new long[] {7, 2, 6, 3}));
    }

    public static long solution(long[] numbers) {
        if (numbers == null || numbers.length == 0)
            return 0;

        long max = numbers[0];
        for(int i = 1; i < numbers.length; i++)
            if (numbers[i] > max)
                max = numbers[i];

        return max;
    }
}
