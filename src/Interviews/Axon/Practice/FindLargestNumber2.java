package Interviews.Axon.Practice;

public class FindLargestNumber2 {
    public static void main(String[] args)
    {
        long[] numbers = { 7, 2, 6, 3 };
        System.out.println(solution(numbers));
    }

    public static long solution(long[] numbers) {
        if(numbers == null || numbers.length == 0)
            return 0;

        long max = numbers[0];
        for(int i = 1; i < numbers.length; i++)
            if(max < numbers[i])
                max = numbers[i];

        return max;
    }
}
