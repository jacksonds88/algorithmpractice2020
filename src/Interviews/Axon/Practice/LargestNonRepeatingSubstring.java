package Interviews.Axon.Practice;

import java.util.HashSet;

public class LargestNonRepeatingSubstring {

    public static void main(String[] args)
    {
        String s = "nndfddf";
        s = "DOKYoDUxVblpTQuMEEORFoqeKJKtZhAamsjveuJiIgnyjVtopOMhUtxUWCxcoOOTQNvpxBJsDTfztkVHFNmTehNXgdJFEdfPrvuqrvDpqCDjeVCYmbNITJYfDbBeCfRHxNlQmfLOjJTGkUITCqHCDDkAStgqKKWXoHzPVlhevecMrRXkMvNnZCAwDIqCVvNipaAscTDthBxKbzhptlXbVhgaovTFNxETAcfiFZxEbasvpMBnPuRPUZdiI...";
        s = "rjwTLbYDCpfQVmAlbFMbTipDUUATaIEHhyHccillUiTFyQxFybOyrKQcAWdBCFRNfoqWxuBlkpCtTpjURpGSdlSQZykijCePGVrxYZpyDUFQsAxDAfWcJMvsJINEnwkYIOpOOrPQUNkWGLNHPDuxmbxUcmCEbOLwTjqAgLBeBVOYCgaIeaxtRfNEbKybTiBBtQuDzGJzqTJuPEAZGfStQbbGKaiPLFnpkdPmfHFhsyLgbacRHAxjjmmsO";
        System.out.println(solution(s));
    }

    public static long solution(String s) {
        if(s == null || s.length() == 0)
            return 0;

        var sb = new StringBuilder();
        var set = new HashSet<Character>();
        long count = 0;
        long largestCount = 0;
        for(var c : s.toCharArray())
        {
            if (!Character.isLetter(c))
            {
                count += 1;
                continue;
            }

            // c = Character.toLowerCase(c); // solution gets worse with this
            if (set.contains(c))
            {
                if (count > largestCount)
                {
                    largestCount = count;
                    System.out.println(sb.toString());
                }
                sb = new StringBuilder();
                set = new HashSet<>();
                count = 0;
            }

            sb.append(c);
            set.add(c);
            count += 1;
        }

        return largestCount;
    }
}
