package Interviews.Axon.Practice;

import java.util.HashSet;

public class FindASumOfPairs {
    public static void main(String[] args)
    {
        long[] numbers = { 7, 2, 6, 2 };
        System.out.println(solution(numbers, 5));
    }

    public static boolean solution(long[] numbers, long target) {
        var V = new HashSet<Long>();

        for(long x : numbers)
        {
            if(V.contains(target - x))
                return true;

            V.add(x);
        }

        return false;
    }
}
