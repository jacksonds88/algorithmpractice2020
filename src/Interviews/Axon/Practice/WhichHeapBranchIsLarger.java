package Interviews.Axon.Practice;

public class WhichHeapBranchIsLarger {
    public static void main(String[] args)
    {
        long[] H = {3, 6, 2, 9, -1, 10, 0, 1, 3, 0, 0, 2};
        System.out.println(solution(H));
    }

    public static String solution(long[] arr) {
        if (arr == null || arr.length == 0)
            return "";

        final int treeHeight = (int)Math.sqrt(arr.length + 1);
        int layer = 0;
        long leftSum = arr[0];
        long rightSum = arr[0];
        while(layer <= treeHeight)
        {
            int leafCount = (int)Math.pow(2, layer);
            int firstLeafOnLayer = leafCount - 1;

            int leftIndex, rightIndex;
            for(int i = 0; i < leafCount/2; i++)
            {
                leftIndex = firstLeafOnLayer + i;
                rightIndex = firstLeafOnLayer + i + (leafCount/2);
                //System.out.println(layer + " - " + leftIndex + " / " + rightIndex);

                if (leftIndex < arr.length && arr[leftIndex] > 0)
                    leftSum += arr[leftIndex];

                if (rightIndex < arr.length && arr[rightIndex] > 0)
                    rightSum += arr[rightIndex];
            }

            layer++;
        }

        //System.out.println(leftSum);
        //System.out.println(rightSum);

        if (leftSum > rightSum)
            return "Left";
        else if (leftSum < rightSum)
            return "Right";
        else
            return "";
    }
}
