package Interviews.Axon.Practice;

public class WhichHeapBranchIsLarger2 {
    public static void main(String[] args)
    {
        long[] A = {3, 6, 2, 9, -1, 14};
        System.out.println(solution(A));
    }

    public static String solution(long[] arr) {
        if (arr == null || arr.length < 2)
            return "";
        else if (arr.length == 2)
            return "Left";

        long[] sums = new long[arr.length];

        for(int i = arr.length - 1; i > 0; i--)
        {
            if(arr[i] == -1)
                continue;

            sums[i] = arr[i];

            //int parent = (i - 1) / 2;
            int leftIndex = i * 2 + 1;
            int rightIndex = leftIndex + 1;

            if(leftIndex < arr.length && arr[leftIndex] != -1)
                sums[i] += arr[leftIndex];
            if(rightIndex < arr.length && arr[rightIndex] != -1)
                sums[i] += arr[rightIndex];
        }

        for(long x : sums)
            System.out.println(x);

        if(sums[1] == sums[2])
            return "";
        else if (sums[1] > sums[2])
            return "Left";
        else
            return "Right";
    }
}
