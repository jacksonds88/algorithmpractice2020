package Interviews.Axon;

import java.util.*;

public class GettingToIncidents {
    /*
     * Complete the 'calculateDistance' function below.
     *
     * The function is expected to return an INTEGER.
     * The function accepts STRING cityMap as parameter.
     */
    static final char OFFICER_CHAR = 'O';
    static final char TARGET_CHAR = 'T';
    static final char PATH_CHAR = '_';
    static final char OBSTACLE_CHAR = 'X';

    static final int OFFICER = 2;
    static final int TARGET = 3;
    static final int PATH = 1;
    static final int OBSTACLE = 0;
    static final int VISITED = -1;

    static final int NO_SOLUTION = -1;
    static final int OFFICER_NOT_FOUND = -2;

    static int[][] directions = new int[][]{{1,0},{0,-1},{-1,0},{0,1}};

    static class Node
    {
        int r, c;
        int step;
        public Node(int r, int c, int step)
        {
            this.r = r;
            this.c = c;
            this.step = step;
        }

        public String toString()
        {
            return r + "," + c;
        }
    }

    static int[][] generateMap(String cityMap)
    {
        String[] cityMapSplit = cityMap.split(";");
        int[][] G = new int[cityMapSplit.length][];
        for(int i = 0; i < G.length; i++)
        {
            G[i] = new int[cityMapSplit[i].length()];
            for(int j = 0; j < G[i].length; j++)
            {
                char c = cityMapSplit[i].charAt(j);
                switch(c)
                {
                    case OFFICER_CHAR:
                        G[i][j] = OFFICER;
                        break;
                    case TARGET_CHAR:
                        G[i][j] = TARGET;
                        break;
                    case PATH_CHAR:
                        G[i][j] = PATH;
                        break;
                    case OBSTACLE_CHAR:
                        G[i][j] = OBSTACLE;
                        break;
                }
            }
        }

        return G;
    }

    static Node findOfficer(int[][] cityMap)
    {
        for(int i = 0; i < cityMap.length; i++)
        {
            for(int j = 0; j < cityMap[i].length; j++)
            {
                if (cityMap[i][j] == OFFICER)
                    return new Node(i, j, 0);
            }
        }

        return null; // can't find officer
    }

    static boolean outOfBounds(int[][] G, int r, int c)
    {
        return r < 0 || r >= G.length || c < 0 || c >= G[r].length;
    }

    public static int calculateDistance(String cityMap) {
        Queue<Node> Q = new LinkedList<>();
        int[][] G = generateMap(cityMap);
        Node officerNode = findOfficer(G);
        if (officerNode == null)
            return OFFICER_NOT_FOUND;

        Q.add(officerNode);

        while(!Q.isEmpty())
        {
            Node current = Q.poll();
            G[current.r][current.c] = VISITED;

            for(int[] d : directions)
            {
                int r = current.r + d[0];
                int c = current.c + d[1];

                if(!outOfBounds(G, r, c) && G[r][c] != OBSTACLE && G[r][c] != VISITED)
                {
                    if(G[r][c] == TARGET)
                    {
                        return current.step + 1;
                    }

                    Q.add(new Node(r, c, current.step + 1));
                }
            }
        }

        return NO_SOLUTION; // no solution
    }

    public static void main(String[] args)
    {
        System.out.println(calculateDistance("O__;_XT;___"));
        System.out.println(calculateDistance("___"));
        System.out.println(calculateDistance("_X_;OXT;_X_"));
    }
}
