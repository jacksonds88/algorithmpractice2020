package Interviews.Axon;

import java.util.Stack;

public class AngledBrackets {

    public static void main(String[] args)
    {
        System.out.println(solution("><<><"));
        System.out.println(solution("<"));
        System.out.println(solution("<<>>>>><<<>>"));
        System.out.println(solution("<<<<>>>>><<<>>"));
        System.out.println(solution("<<<<<>>>>><<<>>>"));
    }

    public static String solution(String angles) {
        if (angles == null || angles.length() == 0)
            return angles;

        Stack<Character> unpairedLeft = new Stack<Character>();
        int unpairedRight = 0;
        for(char c : angles.toCharArray())
        {
            if (c == '<')
            {
                unpairedLeft.push(c);
            }
            else if (c == '>' && !unpairedLeft.empty())
                unpairedLeft.pop();
            else if (c == '>' && unpairedLeft.empty())
                unpairedRight++;
        }

        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < unpairedRight; i++)
            sb.append("<");
        sb.append(angles);
        for (int i = 0; i < unpairedLeft.size(); i++)
            sb.append(">");

        return sb.toString();
    }
}
