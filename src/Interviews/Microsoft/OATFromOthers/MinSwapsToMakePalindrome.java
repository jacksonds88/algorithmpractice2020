package Interviews.Microsoft.OATFromOthers;

public class MinSwapsToMakePalindrome {

    static int getNumberOfSwaps(String s)
    {
        if (!isShuffledPalindrome(s))
            return -1;

        char[] C = s.toCharArray();
        int p1 = 0, p2 = C.length - 1;
        int numberOfSwaps = 0;
        while(p1 < p2)
        {
            if(C[p1] == C[p2])
            {
                p1++;
                p2--;
                continue;
            }

            int k = p2;
            while(k > p1)
            {
                if(C[p1] == C[k])
                    break;
                k--;
            }

            swap(C, k, p2);
            numberOfSwaps++;
            p1++;
            p2--;
        }

        System.out.println("Palindome: " + (new String(C)));
        return numberOfSwaps;
    }

    static int getNumberOfAdjacentSwaps(String s)
    {
        if (!isShuffledPalindrome(s))
            return -1;

        char[] C = s.toCharArray();
        int p1 = 0, p2 = C.length - 1;
        int numberOfSwaps = 0;
        while(p1 < p2)
        {
            if(C[p1] == C[p2])
            {
                p1++;
                p2--;
                continue;
            }

            int k = p2;
            while(k > p1)
            {
                if(C[p1] == C[k])
                    break;
                k--;
            }

            if (k == p1) // this is a weird condition, but it works on input "ntiin"
            {
                swap(C, p1, p1+1);
                numberOfSwaps++;
            }
            else
            {
                while (k < p2)
                {
                    swap(C, k, k+1);
                    numberOfSwaps++;
                    k++;
                }
            }

            p1++;
            p2--;
        }

        System.out.println("Palindome: " + (new String(C)));
        return numberOfSwaps;
    }

    static void swap(char[] C, int i, int j)
    {
        char temp = C[i];
        C[i] = C[j];
        C[j] = temp;
    }

    static boolean isShuffledPalindrome(String p)
    {
        int[] occurrences = new int[26]; // assuming all lowercase

        for(int i = 0; i < p.length(); i++)
        {
            occurrences[p.charAt(i) - 'a']++;
        }

        int oddOccurrences = 0;
        for(int o : occurrences)
            if(o % 2 == 1)
                oddOccurrences++;

        return oddOccurrences <= 1;
    }

    public static void main(String[] args)
    {
        System.out.println(getNumberOfAdjacentSwaps("mamad"));
        System.out.println(getNumberOfAdjacentSwaps("asflkj"));
        System.out.println(getNumberOfAdjacentSwaps("aabb"));
        System.out.println(getNumberOfAdjacentSwaps("ntiin"));
    }
}
