package Interviews.Microsoft.OATFromOthers;

import java.util.Arrays;

// https://www.geeksforgeeks.org/microsoft-interview-experience-set-141-off-campus-online-coding-test-idc/
public class DoubleAndShiftArray {

    static int[] solution(int[] A)
    {
        int j = 0;
        for(int i = 0; i < A.length; i++) {
            if (A[i] == 0)
                continue;

            if (i < A.length - 1 && A[i] == A[i + 1]) {
                A[i] += A[i + 1];
                A[i + 1] = 0;
            }

            if (A[i] > 0)
            {
                if (i != j)
                {
                    A[j] = A[i];
                    A[i] = 0;
                }

                j++;
            }
        }

        return A;
    }

    public static void main(String[] args)
    {
        int[] A = {2, 2, 0, 4, 0, 8};
        int[] B = solution(A);

        System.out.println(Arrays.toString(B));

        A = new int[]{2, 2, 0, 4, 0, 2};
        B = solution(A);

        System.out.println(Arrays.toString(B));
    }
}
