package Interviews.Microsoft;

import java.io.File;
import java.util.HashMap;
import java.util.LinkedList;

/*{ paths:[
        "Compute\TM\foo.md",
                "Compute\bar.md",
                "Network/foo2.md"
    ]
    }

    Compute
       |  \
    TM  bar.md
       |
    foo.md


            Network
        |
    foo2.md
     */

public class TreeDirectory {
    public class SystemNode
    {
        String path;
        String name;
        boolean isDirectory;
        private LinkedList<SystemNode> children;

        public SystemNode(String[] path, boolean isDirectory)
        {
            this.path = String.join(File.separator, path);
            this.name = path[path.length - 1];
            this.isDirectory = isDirectory;
            if (isDirectory)
                children = new LinkedList<>();
        }

        public boolean contains(String path)
        {
            if (!isDirectory)
                return false;

            for (SystemNode node : children)
                if (path.equalsIgnoreCase(node.path))
                    return true;

            return false;
        }

        public SystemNode get(String path)
        {
            if (!isDirectory)
                return null;

            for (SystemNode node : children)
                if (path.equalsIgnoreCase(node.path))
                    return node;

            return null;
        }

        public void addChild(SystemNode newChild)
        {
            children.add(newChild);
            files.put(newChild.path, newChild);
        }

        @Override
        public boolean equals(Object o)
        {
            if (!(o instanceof SystemNode))
            return false;

            SystemNode other = (SystemNode)o;
            if (this == other)
                return true;

            return this.path.equalsIgnoreCase(other.path);
        }
    }

    SystemNode root = new SystemNode(new String[]{ File.separator }, true);
    HashMap<String, SystemNode> files = new HashMap<>();

    public void build(String[] paths)
    {
        for (String path : paths)
        {
            String[] splitPath = path.split(File.separator); // Compute\TM\foo.md
            insert(root, path, splitPath, 1);
        }
    }

    public void insert(SystemNode current, String path, String[] splitPath, int index)
    {
        if (index == splitPath.length)
            return;

        //if (!current.contains(splitPath[index]))
        if (!files.containsKey(path))
        {
            boolean isDirectory = index == splitPath.length - 1;
            SystemNode newNode = new SystemNode(splitPath, isDirectory);
            current.addChild(newNode);
        }

        SystemNode childNode = current.get(splitPath[index]);
        insert(childNode, path, splitPath, index + 1);
    }
}
