package Interviews.Microsoft.OAT;

import java.util.*;

public class Task3_MinSwaps {

    static int solution(String balls)
    {
        LinkedList<Integer> redBalls = getRedBalls(balls);
        int mid = redBalls.size() / 2;
        int minSwaps = 0;
        for(int i = 0; i < redBalls.size(); i++)
        {
            minSwaps += Math.abs(redBalls.get(mid) - redBalls.get(i)) - Math.abs(mid - i);
        }

        return minSwaps;
    }

    static LinkedList<Integer> getRedBalls(String balls)
    {
        LinkedList<Integer> redBalls = new LinkedList<>();
        for(int i = 0; i < balls.length(); i++)
        {
            if(balls.charAt(i) == 'R')
            {
                redBalls.add(i);
            }
        }

        return redBalls;
    }

    public int alternatvieSolution(String S){
        List<Integer> reds = new ArrayList<>();
        for (int i = 0; i < S.length(); i++){
            if (S.charAt(i) == 'R'){
                reds.add(i);
            }
        }

        // RRRWRR
        // 01245
        int res = 0;
        int l = 0;
        int r = reds.size()-1;
        while(l < r){
            int occupiedInMiddle = r-l-1; // 3 occupied in middle when left is 0 and right is 5
            res += reds.get(r) - reds.get(l) - 1 - occupiedInMiddle;
            r--;
            l++;
        }
        return res;
    }

    public static void main(String[] args)
    {
        System.out.println("solution: " + solution("WRRWRW"));
        System.out.println("solution: " + solution("WWWRWWRWR"));
    }
}
