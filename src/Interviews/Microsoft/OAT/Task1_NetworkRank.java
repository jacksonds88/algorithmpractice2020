package Interviews.Microsoft.OAT;

public class Task1_NetworkRank {

    // time complexity: O(n)
    // space complexity: O(n) because of the degrees array
    public int solution(int[] A, int[] B, int N) {
        if (N <= 1 || A.length == 0 || A.length != B.length)
            return 0;

        int[] degrees = new int[N];
        for(int i = 0; i < A.length; i++)
        {
            degrees[A[i]-1]++;
            degrees[B[i]-1]++;
        }

        int networkRank = 0;
        for(int i = 0; i < A.length; i++)
        {
            int pairRank = degrees[A[i]-1] + degrees[B[i]-1] - 1;
            if (pairRank > networkRank)
                networkRank = pairRank;
        }

        return networkRank;
    }

    public static void main(String[] args)
    {
        int[] A = {1, 2, 3, 3};
        int[] B = {2, 3, 1, 4};
        int N = 4;
        Task1_NetworkRank s = new Task1_NetworkRank();
        System.out.println(s.solution(A, B, N));

        A = new int[]{1, 2, 4, 5};
        B = new int[]{2, 3, 5, 6};
        N = 6;
        System.out.println(s.solution(A, B, N));
    }
}
