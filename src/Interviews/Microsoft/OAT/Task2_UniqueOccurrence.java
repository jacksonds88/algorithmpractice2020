package Interviews.Microsoft.OAT;


import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class Task2_UniqueOccurrence {
    public static int solution(String s)
    {
        if (s == null || s.length() == 0)
            return 0;

        HashMap<Character, Integer> O = new HashMap<>();
        for(char c : s.toCharArray())
        {
            if(O.containsKey(c))
                O.put(c, O.get(c) + 1);
            else
                O.put(c, 1);
        }

        HashSet<Integer> U = new HashSet<>();
        int removeCount = 0;
        for (Integer occurrence : O.values())
        {
            if(!U.contains(occurrence))
            {
                U.add(occurrence);
                continue;
            }

            while(occurrence > 0 && U.contains(occurrence))
            {
                occurrence--;
                removeCount++;
            }

            if (occurrence > 0)
                U.add(occurrence);
        }

        return removeCount;
    }

    public static int solutionBrian(String s) {
        if (s == null || s.length() == 0) {
            return 0;
        }

        Map<Character, Integer> m = new HashMap<>(26);

        for (char c : s.toCharArray()) {
            m.put(c, m.getOrDefault(c, 0) + 1);
        }

        int d = 0;

        Set<Integer> h = new HashSet<>(26);

        for (int v : m.values()) {
            if (!h.add(v)) {
                while (h.contains(v) && v > 0) {
                    v--;
                    d++;
                }
                if (v > 0) {
                    h.add(v);
                }
            }
        }

        return d;
    }

    public static int solution2(String s) {
        HashMap<Character, Integer> map = new HashMap<Character, Integer>();
        for (char c : s.toCharArray()) {
            if (map.containsKey(c)) {
                map.put(c, map.get(c) + 1);
            }
            else {
                map.put(c, 1);
            }
        }
        int res = 0;
        HashSet<Integer> set = new HashSet<>();
        for (int count : map.values()) {

            if (!set.contains(count)) {
                set.add(count);
            }
            else {
                int n = count;
                while (set.contains(n) && n > 0) {
                    n = n - 1;
                    res = res + 1;
                }
                if (n > 0) {
                    set.add(n);
                }
            }
        }

        return res;
    }

    public static void main(String[] args)
    {
        System.out.println(solution("example"));
        System.out.println(solution("eeee"));
        System.out.println(solution("aaaabbbb"));
    }
}
