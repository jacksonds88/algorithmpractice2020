package Interviews.Microsoft.OAT2;

import java.util.Arrays;

/*
This program is used to find Binary Period of a number.

Definition of a binary period : The period of this string is the smallest positive integer P such that: P ≤ Q / 2 and S[K] = S[K+P] for 0 ≤ K < Q − P. For example, 7 is the period of “abracadabracadabra”. A positive integer M is the binary period of a positive integer N if M is the period of the binary representation of N.

For example, 1651 has the binary representation of "110011100111". Hence, its binary period is 5. On the other hand, 102 does not have a binary period, because its binary representation is “1100110” and it does not have a period.

For example, 7 is the period of “pepsicopepsicopep”. A positive integer M is the binary period of a positive integer N if M is the period of the binary representation of N.
 */

public class BinaryPeriod {
    int solution(int n) {
        int m = n;
        int[] d = new int[30];
        int l = 0;
        int p;
        while (n > 0) {
            d[l] = n % 2;
            n /= 2;
            l++;
        }

        d = Arrays.copyOf(d, l);
        System.out.println("n=" + m + "=" + Arrays.toString(d));

        for (p = 1; p < 1 + l / 2; ++p) {
            int i;
            boolean ok = true;
            for (i = 0; i < l - p; ++i) {
                System.out.println("p=" + p + ": d[" + (i) + "]=" + d[i] + " ?equals d[" + + (i+p) + "]=" + d[i + p] + " is " + (d[i] == d[i + p]));
                if (d[i] != d[i + p]) {
                    ok = false;
                    break;
                }
            }
            System.out.println("Are we done? " + ok);
            if (ok) {
                return p;
            }
        }
        return -1;
    }

    public static void main(String[] args)
    {
        // 1101 1101 11
        BinaryPeriod o = new BinaryPeriod();
        System.out.println(o.solution(5));
        System.out.println();

        //System.out.println(o.solution(26));
        System.out.println(o.solution(42));
        System.out.println();

        System.out.println(o.solution(43));
        System.out.println();

        System.out.println(o.solution(955));
        System.out.println();

        //System.out.println(o.solution(955));
        //System.out.println(o.solution(2641));
        //System.out.println(o.solution(10863));  //
        //System.out.println(o.solution(21672));  // 7
    }
}
