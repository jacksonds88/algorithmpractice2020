import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;

public class TopologicalSort2 {
    public static void main(String[] args)
    {
        int[][] G = new int[][] {
                {0,0,1,0,0},
                {0,0,1,0,0},
                {0,0,0,1,1},
                {0,0,0,0,1},
                {0,0,0,0,0},
        };
        System.out.println();
        for(int x : sortDFS(G))
            System.out.println(x);

        System.out.println();
        for(int x : khan(G))
            System.out.println(x);
    }

    static LinkedList<Integer> khan(int[][] G)
    {
        Queue<Integer> Q = new LinkedList<>(getNodesWithNoIncomingEdges(G));
        LinkedList<Integer> ret = new LinkedList<>();

        while(!Q.isEmpty())
        {
            Integer current = Q.poll();
            ret.add(current);
            //System.out.println(current);

            for(int i = 0; i < G.length; i++)
            {
                if(G[current][i] == 1)
                {
                    G[current][i] = 0;

                    // remove incoming edges to current
                    boolean noIncomingEdges = true;
                    for(int j = 0; j < G[i].length; j++)
                    {
                        if (G[j][i] == 1)
                        {
                            noIncomingEdges = false;
                            break;
                        }
                    }

                    if (noIncomingEdges)
                        Q.add(i);
                }
            }
        }

        for(int i = 0; i < G.length; i++)
        {
            for(int j = 0; j < G.length; j++)
            {

            }
        }

        return ret;
    }

    static LinkedList<Integer> sortDFS(int[][] G)
    {
        var orderedRoute = new LinkedList<Integer>();
        var V = new HashSet<Integer>();
        var startingIndexes = getNodesWithNoIncomingEdges(G);

        for(int i : startingIndexes)
            orderedRoute = sortDFS(G, i, orderedRoute, V);

        var reverseOrderedRoute = new LinkedList<Integer>();
        for(int x : orderedRoute)
            reverseOrderedRoute.addFirst(x);
        return reverseOrderedRoute;
    }

    static Set<Integer> getNodesWithNoIncomingEdges(int[][] G)
    {
        HashSet<Integer> N = new HashSet<>();

        for(int col = 0; col < G.length; col++)
        {
            boolean noIncoming = true;
            for(int row = 0; row < G[col].length; row++)
            {
                if(G[row][col] == 1) {
                    noIncoming = false;
                    break;
                }
            }

            if(noIncoming)
            {
                N.add(col);
                System.out.println("col: " + col);
            }
        }

        return N;
    }

    static LinkedList<Integer> sortDFS(int[][] G, int i, LinkedList<Integer> route, HashSet<Integer> V)
    {
        //System.out.println(i);

        for(int next = 0; next < G[i].length; next++)
        {
            if(G[i][next] == 1 && !V.contains(next))
            {
                sortDFS(G, next, route, V);
            }
        }

        route.add(i);
        V.add(i);
        return route;
    }
}
