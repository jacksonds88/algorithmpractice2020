package CrackingTheCodingInterview;

// Implement an algorithm to determine if a String has all unique characters
// What if you cannot use additional data structures?

import java.util.Arrays;

public class Chapter1_1_IsUnique {

    // O(n^2)
    public static boolean solution2(String s)
    {
        for(int i = 0; i < s.length() - 1; i++)
        {
            for(int j = i + 1; j < s.length(); j++)
            {
                if (s.charAt(i) == s.charAt(j))
                    return false;
            }
        }

        return true;
    }

    // O(n logn)
    public static boolean solution(String s)
    {
        char[] C = s.toCharArray();
        Arrays.sort(C);
        //System.out.println(new String(C));

        for(int i = 0; i < s.length() - 1; i++)
        {
            if (C[i] == C[i+1])
                return false;
        }

        return true;
    }

    public static void main(String[] args)
    {
        System.out.println(solution("hello"));
        System.out.println(solution("helo"));
    }
}
