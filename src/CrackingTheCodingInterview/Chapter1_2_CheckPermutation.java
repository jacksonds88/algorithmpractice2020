package CrackingTheCodingInterview;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class Chapter1_2_CheckPermutation {

    // n
    public static boolean solution(String P1, String P2)
    {
        if (!isSameLength(P1, P2))
            return false;

        HashMap<Character, Integer> H1 = new HashMap<>();
        HashMap<Character, Integer> H2 = new HashMap<>();

        for (int i = 0; i < P1.length(); i++) {
            char c1 = P1.charAt(i);
            if (H1.containsKey(c1)) {
                H1.put(c1, H1.get(c1) + 1);
            }
            else {
                H1.put(c1, 1);
            }

            char c2 = P2.charAt(i);
            if (H2.containsKey(c2)) {
                H2.put(c2, H2.get(c2) + 1);
            } else {
                H2.put(c2, 1);
            }
        }

        // we only need to iterate through 1 hash map since we are guaranteed same length from validation step
        for (Map.Entry<Character, Integer> m : H1.entrySet()) {
            if (!H2.containsKey(m.getKey()) || !H2.get(m.getKey()).equals(m.getValue())) {
                return false;
            }
        }

        return true;
    }

    public static boolean solution2(String P1, String P2)
    {
        if (!isSameLength(P1, P2))
            return false;

        char[] C1 = P1.toCharArray();
        char[] C2 = P2.toCharArray();

        // n log n
        Arrays.sort(C1);
        Arrays.sort(C2);

        for (int i = 0; i < C1.length; i++) {
            if(C1[i] != C2[i])
                return false;
        }

        return true;
    }

    static boolean isSameLength(String P1, String P2)
    {
        if (P1 == null && P2 == null)
            return true;

        if (P1 == null || P2 == null)
            return false;

        if (P1.length() == P2.length())
            return true;

        return false;
    }

    public static void main(String[] args)
    {
        System.out.println(solution("CCC", "CCB"));
        System.out.println(solution2("CCC", "CCB"));
        System.out.println(solution("ABCD", "DACB"));
        System.out.println(solution2("ABCD", "DACB"));
    }
}
