import java.util.Arrays;

/*
https://www.youtube.com/watch?v=qli-JCrSwuk&t=269s

How many ways to decode this message?

 */
public class DecodeMessage {
    public static void main(String[] args)
    {
        String data = "123";
        System.out.println(num_ways(data, data.length()));
        data = "1111";
        System.out.println(num_ways(data, data.length()));

        data = "011";
        System.out.println(num_ways(data, data.length()));

        data = "999";
        System.out.println(num_ways(data, data.length()));

        data = "1111";
        int[] dp = new int[data.length()];
        Arrays.fill(dp, -1);
        System.out.println(num_ways(data, data.length(), dp));
    }

    static int num_ways(String data, int k)
    {
        if(k == 0)
            return 1; // made it to the end

        int s = data.length() - k;
        if(data.charAt(s) == '0')
            return 0; // dead end

        int result = num_ways(data, k - 1);
        if(k - 2 >= 0 && Integer.parseInt(data.substring(k-2, k)) <= 26)
        {
            // System.out.println("F: " + Integer.parseInt(data.substring(k-2, k)));
            result += num_ways(data, k - 2);
        }

        return result;
    }

    static int num_ways(String data, int k, int[] dp)
    {
        if(k == 0)
            return 1; // made it to the end

        int s = data.length() - k;
        if(data.charAt(s) == '0')
            return 0; // dead end

        if(dp[k - 1] >= 0)
            return dp[k - 1];

        int result = num_ways(data, k - 1, dp);
        if(k - 2 >= 0 && Integer.parseInt(data.substring(k-2, k)) <= 26)
        {
            // System.out.println("F: " + Integer.parseInt(data.substring(k-2, k)));
            result += num_ways(data, k - 2, dp);
        }

        dp[k - 1] = result;
        return result;
    }
}
