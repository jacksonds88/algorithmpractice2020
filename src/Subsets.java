import java.util.*;

public class Subsets {

    // [1, 2, 3] => [1] + [2, 3]
    // [1, 2, 3] => [2] + [1, 3]
    public static Set<Set<Integer>> getSubSets(Set<Integer> set)
    {
        if (set == null || set.isEmpty())
        {
            return new HashSet<>();
        }

        Set<Set<Integer>> ret = new HashSet<>();
        ret.add(set);
        if (set.size() == 1){
            return ret;
        }

        for (Integer x : set)
        {
            HashSet<Integer> subset = new HashSet<>(set);
            subset.remove(x);
            Set<Set<Integer>> subsets = getSubSets(subset);
            ret.add(subset);
            ret.addAll(subsets);
        }

        return ret;
    }

    public static void main(String[] args)
    {
        Set<Integer> set = new HashSet<>();
        set.add(1);
        set.add(2);
        set.add(3);
        set.add(4);

        Set<Set<Integer>> subsets = getSubSets(set);

        for (Set<Integer> subset : subsets)
        {
            int[] subsetArray = new int[subset.size()];
            int i = 0;
            for (Integer x : subset)
            {
                subsetArray[i++] = x;
            }

            System.out.println(Arrays.toString(subsetArray));
        }
    }
}
