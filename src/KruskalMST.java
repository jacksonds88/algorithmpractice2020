import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;

// https://www.tutorialspoint.com/data_structures_algorithms/prims_spanning_tree_algorithm.htm
//https://en.wikipedia.org/wiki/Kruskal%27s_algorithm

public class KruskalMST {

    class Edge implements Comparable<Edge>
    {
        int v1, v2;
        int distance;

        public Edge(int v1, int v2, int distance)
        {
            this.v1 = v1;
            this.v2 = v2;
            this.distance = distance;
        }

        @Override
        public int compareTo(Edge e) {
            return Double.compare(distance, e.distance);
        }
    }

    boolean isCycle(HashMap<Integer, HashSet<Integer>> S, int v1, int v2)
    {
        var S1 = S.get(v1);
        var S2 = S.get(v2);

        if (S1.contains(v2))
            return true;
        else
            return S2.contains(v1); // no need for else
    }

    void union(HashMap<Integer, HashSet<Integer>> S, int v1, int v2)
    {
        var S1 = S.get(v1);
        var S2 = S.get(v2);

        S1.addAll(S2);
        S.put(v1, S1);
        S.put(v2, S1);
    }

    LinkedList<Edge> solution(int[][] G)
    {
        // initialize
        var edges = new LinkedList<Edge>();
        var S = new HashMap<Integer, HashSet<Integer>>();
        for(int i = 0; i < G.length; i++)
        {
            var s2 = new HashSet<Integer>();
            s2.add(i);
            S.put(i, s2);

            for(int j = 0; j < G[i].length; j++)
            {
                if(G[i][j] > 0)
                    edges.add(new Edge(i, j, G[i][j]));
            }
        }

        // algorithm solution
        Collections.sort(edges);

        var result = new LinkedList<Edge>();
        for(var edge : edges)
        {
            if(!isCycle(S, edge.v1, edge.v2))
            {
                union(S, edge.v1, edge.v2);
                result.add(edge);
            }
        }

        return result;
    }

    public static void main(String[] args)
    {
        // https://www.tutorialspoint.com/data_structures_algorithms/prims_spanning_tree_algorithm.htm
        String[] names = new String[]{"S", "A", "C", "B", "D", "T"};
        int[][] G = new int[6][6];
        G[0][1] = 7;
        G[0][2] = 8;

        G[1][0] = 7;
        G[1][2] = 3;
        G[1][3] = 6;

        G[2][0] = 8;
        G[2][1] = 3;
        G[2][3] = 4;
        G[2][4] = 3;

        G[3][1] = 6;
        G[3][2] = 4;
        G[3][4] = 2;
        G[3][5] = 5;

        G[4][2] = 3;
        G[4][3] = 2;
        G[4][5] = 2;

        G[5][3] = 5;
        G[5][4] = 2;

        var kruskal = new KruskalMST();
        var result = kruskal.solution(G);
        for(var e : result)
        {
            System.out.println(names[e.v1] + ", " + names[e.v2]);
        }
        System.out.println();

        names = new String[]{"A", "B", "C", "D", "E", "F", "G"};
        G = new int[7][7];
        G[0][1] = 7;
        G[0][3] = 5;

        G[1][0] = 7;
        G[1][2] = 8;
        G[1][3] = 9;
        G[1][4] = 7;

        G[2][1] = 8;
        G[2][4] = 5;

        G[3][0] = 5;
        G[3][1] = 9;
        G[3][4] = 15;
        G[3][5] = 6;

        G[4][1] = 7;
        G[4][2] = 5;
        G[4][3] = 15;
        G[4][5] = 8;
        G[4][6] = 9;

        G[5][3] = 6;
        G[5][4] = 8;
        G[5][6] = 11;

        G[6][4] = 9;
        G[6][5] = 11;

        result = kruskal.solution(G);
        for(var e : result)
        {
            System.out.println(names[e.v1] + ", " + names[e.v2]);
        }
    }
}
