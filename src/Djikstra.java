import java.util.*;

// https://en.wikipedia.org/wiki/Dijkstra%27s_algorithm
/* Djikstra's algorith is a:
        Search Algorithm
        Greedy Algorithm
        Dynamic Programming

        O(|E| + |V|log|V|)
*/


public class Djikstra {
    public static void main(String[] args)
    {
        int[][] G = new int[][]{
                {0, 7, 9, 0, 0, 14},
                {7, 0, 10, 15, 0, 0},
                {9, 10, 0, 11, 0, 2},
                {0, 15, 11, 0, 6, 0},
                {0, 0, 0, 6, 0, 9},
                {14, 0, 2, 0, 9, 0},
        };

        int[][] G2 = new int[][]{
                {0, 1, 5, 9},
                {0, 0, 1, 0},
                {0, 0, 0, 1},
                {0, 0, 0, 0}
        };

        System.out.println("djikstra: " + djikstra(G, 0, 4));
        System.out.println("djikstra: " + djikstra(G2, 0, 3));
    }

    public static int djikstra(int[][] G, int start, int finish)
    {
        Queue<Integer> Q = new LinkedList<>();
        Q.add(start);

        Set<Integer> V = new HashSet<>();
        V.add(start);

        int[] costs = new int[G.length];
        Arrays.fill(costs, Integer.MAX_VALUE);
        costs[start] = 0;

        while(!Q.isEmpty())
        {
            int current = Q.poll();
            V.add(current);
            if (current == finish)
                continue; // no need to traverse from the finish node

            for (int i = 0; i < G.length; i++)
            {
                if (G[current][i] > 0)
                {
                    int cost = costs[current] + G[current][i];
                    if (cost < costs[i]) {
                        costs[i] = cost;
                    }

                    if (!V.contains(i))
                    {
                        Q.add(i);
                    }
                }
            }
        }

        return costs[finish];
    }
}
