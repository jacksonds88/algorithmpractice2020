public class BinarySearch
{
    public static void main(String[] args)
    {
        int x = 7;
        int[] A = {2, 5, 8, 12, 16, 23, 38, 56, 72, 91};

        System.out.println(binarySearchRecursive(x, A, 0, A.length - 1));
        System.out.println(binarySearchIterative(x, A));
    }

    public static boolean binarySearchRecursive(int searchKey, int[] searchSpace, int left, int right)
    {
        if (left > right)
            return false;

        int mid = ((right - left) / 2) + left;
        if (searchSpace[mid] == searchKey)
            return true;
        else if (searchSpace[mid] > searchKey)
            return binarySearchRecursive(searchKey, searchSpace, left, mid - 1);
        else
            return binarySearchRecursive(searchKey, searchSpace, mid + 1, right);
    }

    public static boolean binarySearchIterative(int searchKey, int[] searchSpace)
    {
        int left = 0;
        int right = searchSpace.length - 1;

        while(left <= right)
        {
            int mid = ((right - left) / 2) + left;
            if (searchSpace[mid] == searchKey)
                return true;
            else if (searchSpace[mid] > searchKey)
                right = mid - 1;
            else
                left = mid + 1;
        }

        return false;
    }
}
