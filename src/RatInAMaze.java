
// https://www.geeksforgeeks.org/rat-in-a-maze-backtracking-2/

import java.util.Arrays;

public class RatInAMaze {

    static final int[][] DIR = {
            {1, 0},
            {0, 1},
            {-1, 0},
            {0, -1}
    };

    static final int ROW = 0;
    static final int COL = 1;

    static final int UNVISITED = 1;
    static final int VISITED = 2;

    public static boolean hasSolution(int[][] M, int[] start, int[] end)
    {
        return hasSolution(M, start[ROW], start[COL], end);
    }

    static boolean isValidMove(int[][] M, int row, int col)
    {
        return row >= 0 && row < M.length && col >= 0 && col < M[row].length;
    }

    static boolean hasSolution(int[][] M, int row, int col, int[] end)
    {
        M[row][col] = VISITED;
        if (row == end[ROW] && col == end[COL])
        {
            return true;
        }

        for (int i = 0; i < M.length; i++)
        {
            int nextRow = row + DIR[i][ROW];
            int nextCol = col + DIR[i][COL];
            if (isValidMove(M, nextRow, nextCol) && M[nextRow][nextCol] == UNVISITED)
            {
                return hasSolution(M, nextRow, nextCol, end);
            }
        }
        M[row][col] = UNVISITED;
        return false;
    }

    static void print(int[][] M)
    {
        for(int[] m : M)
        {
            System.out.println(Arrays.toString(m));
        }
    }

    public static void main(String[] args)
    {
        int[][] M = {
                {1, 0, 0, 0},
                {1, 1, 0, 0},
                {0, 1, 0, 0},
                {1, 1, 1, 1},
        };

        int[] start = {0, 0};
        int[] end = {M.length - 1, M[0].length - 1};

        System.out.println(hasSolution(M, start, end));
        print(M);
    }
}
