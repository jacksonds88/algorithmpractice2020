import Core.ListNode;

public class ReverseLinkedList {
    static ListNode reverse(ListNode head)
    {
        ListNode prev = null;
        ListNode current = head;
        ListNode next = null;

        while(current != null)
        {
            next = current.next;
            current.next = prev;
            prev = current;
            current = next;
        }

        head = prev;
        return head;
    }

    public static void main(String[] args)
    {
        ListNode e = new ListNode(5);
        ListNode d = new ListNode(4, e);
        ListNode c = new ListNode(3, d);
        ListNode b = new ListNode(2, c);
        ListNode a = new ListNode(1, b);

        System.out.println(ListNode.toString(a));
        System.out.println(ListNode.toString(reverse(a)));
    }
}
