import Core.BinarySearchTree;
import Core.TreeNode;
import LeetCode.Trees.BalancedTree;

public class FindBinaryTreeHeight {
    public static int findHeight(TreeNode head)
    {
        if (head == null)
            return 0;

        return 1 + Math.max(findHeight(head.left), findHeight(head.right));
    }

    public static int findHeight(TreeNode head, int height)
    {
        if (head == null)
            return height;

        height++;
        int leftHeight = findHeight(head.left, height);
        int rightHeight = findHeight(head.right, height);

        return Math.max(leftHeight, rightHeight);
    }

    public static void main(String[] args)
    {
        TreeNode e = new TreeNode(7);
        TreeNode d = new TreeNode(15);
        TreeNode c = new TreeNode(20, d, e);
        TreeNode b = new TreeNode(9);
        TreeNode a = new TreeNode(1, b, c);

        TreeNode g2 = new TreeNode(4);
        TreeNode f2 = new TreeNode(4);
        TreeNode e2 = new TreeNode(3);
        TreeNode d2 = new TreeNode(3, f2, g2);
        TreeNode c2 = new TreeNode(2);
        TreeNode b2 = new TreeNode(2, d2, e2);
        TreeNode a2 = new TreeNode(1, b2, c2);

        TreeNode c3 = new TreeNode(3);
        TreeNode b3 = new TreeNode(2, null, c3);
        TreeNode a3 = new TreeNode(1, null, b3);

        BinarySearchTree bst = new BinarySearchTree(a2);
        System.out.println(a2.toString());
        System.out.println(bst.toString());
        System.out.println(findHeight(a2));
        System.out.println(findHeight(a2, 0));

        bst = new BinarySearchTree(a3);
        System.out.println(a3.toString());
        System.out.println(bst.toString());
        System.out.println(findHeight(a3));
        System.out.println(findHeight(a3, 0));
    }
}
