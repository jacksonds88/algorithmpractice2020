import java.util.HashSet;
import java.util.LinkedList;
import java.util.Stack;

public class DepthFirstSearch {
    public static void main(String[] args)
    {
        int[][] G = new int[][]{
                {0, 7, 9, 0, 0, 14},
                {7, 0, 10, 15, 0, 0},
                {9, 10, 0, 11, 0, 2},
                {0, 15, 11, 0, 6, 0},
                {0, 0, 0, 6, 0, 9},
                {14, 0, 2, 0, 9, 0},
        };

        //for(var x : DFS(G, 4))
        //    System.out.println(x + 1);

        for(var x : DFS(G, 0, new HashSet<>(), new LinkedList<>()))
            System.out.println(x + 1);
    }

    static LinkedList<Integer> DFS(int[][] G, int S)
    {
        var route = new LinkedList<Integer>();
        var stack = new Stack<Integer>();
        var V = new HashSet<Integer>();
        stack.push(S);

        while(!stack.empty())
        {
            var current = stack.pop();
            V.add(current);
            route.add(current);

            for(int i = 0; i < G[current].length; i++)
            {
                if (G[current][i] != 0 && !V.contains(i))
                {
                    stack.push(i);
                    break;
                }
            }
        }

        return route;
    }

    static LinkedList<Integer> DFS(int[][] G, int i, HashSet<Integer> V, LinkedList<Integer> route)
    {
        route.add(i);
        V.add(i);

        for(int j = 0; j < G[i].length; j++)
            if(G[i][j] != 0 && !V.contains(j))
                return DFS(G, j, V, route);

        return route;
    }
}
