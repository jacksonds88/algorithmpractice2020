
// Backtracking
// Search Algorithm (DFS)

/*
    Interesting Note:
        Each solution of a hamiltonian cycle can be used in the Traveling Salesman Problem

 */

import java.util.ArrayList;
import java.util.Arrays;

public class HamiltonianCycle {

    public static final int UNVISITED = -1;
    static ArrayList<int[]> solutions = new ArrayList<>();
    static int[] path;
    static int pathSize = 0;
    static boolean found = false;

    public static void main(String[] args)
    {
        int[][] G = new int[][]{
                {0, 1, 0, 1, 1, 0, 0, 0},
                {1, 0, 1, 0, 0, 1, 0, 0},
                {0, 1, 0, 1, 0, 0, 1, 0},
                {1, 0, 1, 0, 0, 0, 0, 1},
                {1, 0, 0, 0, 0, 1, 0, 1},
                {0, 1, 0, 0, 1, 0, 1, 0},
                {0, 0, 1, 0, 0, 1, 0, 1},
                {0, 0, 0, 1, 1, 0, 1, 0},
        };

        path = new int[G.length + 1];
        findHamiltonianCycle(G);

        for(var solution : solutions)
        {
            for(var i : solution)
                System.out.print(i + ", ");
            System.out.println();
        }

        if (solutions.size() > 0)
            System.out.println("Hamiltonian Cycle exists");
        else
            System.out.println("No Hamiltonian Cycle found");
    }

    static void findHamiltonianCycle(int[][] G)
    {
        int[] path =  new int[G.length + 1];
        Arrays.fill(path, UNVISITED);

        //solve(G, 0);
        solve(G, path, 0, 0);
    }

    static void solve(int[][] G, int[] path, int c, int pathSize)
    {
        path[pathSize] = c;

        if(pathSize == G.length - 1)
        {
            if(G[c][0] == 1)
            {
                int[] newSolution = new int[path.length];
                for(int i = 0; i < G.length; i++)
                    newSolution[i] = path[i];
                newSolution[G.length] = newSolution[0];
                solutions.add(newSolution);
            }
        }
        else
        {
            for(int i = 0; i < G[c].length; i++)
            {
                if(G[c][i] == 1 && !inPath(path, i))
                    solve(G, path, i, pathSize + 1);
            }
        }

        path[pathSize] = UNVISITED;
    }

    static void solve(int[][] G, int c)
    {
        path[pathSize] = c;
        pathSize++;

        if(pathSize == G.length)
        {
            if(G[c][0] == 1)
            {
                int[] newSolution = new int[path.length];
                for(int i = 0; i < G.length; i++)
                    newSolution[i] = path[i];
                newSolution[G.length] = newSolution[0];
                solutions.add(newSolution);
            }
        }
        else
        {
            for(int i = 0; i < G[c].length; i++)
            {
                if(G[c][i] == 1 && !inPath(path, i))
                    solve(G, i);
            }
        }

        pathSize--;
        path[pathSize] = UNVISITED;
    }

    static boolean inPath(int[] path, int next)
    {
        for(int i = 0; i < path.length; i++)
            if(next == path[i])
                return true;

        return false;
    }
}
