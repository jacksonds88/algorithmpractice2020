// https://www.youtube.com/watch?v=We3YDTzNXEk

public class MinEditDistanceDP {

    public static int minEditDistance(String s1, String s2)
    {
        int[][] D = new int[s1.length() + 1][s2.length() + 1];

        for (int i = 0; i < D.length; i++) D[i][0] = i;
        for (int i = 0; i < D[0].length; i++) D[0][i] = i;

        for (int i = 1; i < D.length; i++)
        {
            for (int j = 1; j < D[i].length; j++)
            {
                int min = Math.min(D[i-1][j-1], D[i-1][j]);
                min = Math.min(min, D[i][j-1]);

                D[i][j] = s1.charAt(i - 1) != s2.charAt(j - 1) ? min + 1 : min;
            }
        }

        return D[D.length - 1][D[0].length - 1];
    }

    public static void main(String[] args)
    {
        System.out.println(minEditDistance("azced", "abcdef"));
    }
}
