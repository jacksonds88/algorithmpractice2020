import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;

public class TopologicalSort3 {
    public static void main(String[] args)
    {
        int[][] G = new int[][] {
                {0,0,1,0,0},
                {0,0,1,0,0},
                {0,0,0,1,1},
                {0,0,0,0,1},
                {0,0,0,0,0},
        };

        System.out.println();
        for(int x : khan(G))
            System.out.println(x);
    }

    static LinkedList<Integer> khan(int[][] G)
    {
        Queue<Integer> Q = getStartingNodes(G);
        Set<Integer> Queued = new HashSet<>();
        LinkedList<Integer> ret = new LinkedList<>();

        while (!Q.isEmpty())
        {
            int current = Q.poll();
            ret.add(current);

            // add the next nodes from current
            for (int j = 0; j < G[current].length; j++)
            {
                if (G[current][j] == 1 && !Queued.contains(j))
                {
                    Q.add(j);
                    Queued.add(j);
                }
            }
        }

        return ret;
    }

    static LinkedList<Integer> getStartingNodes(int[][] G)
    {
        LinkedList<Integer> startingNodes = new LinkedList<>();

        for(int i = 0; i < G[0].length; i++)
        {
            boolean noIncomingEdges = true;
            for(int j = 0; j < G.length; j++)
            {
                if (G[j][i] == 1)
                {
                    noIncomingEdges = false;
                    break;
                }
            }

            if (noIncomingEdges)
                startingNodes.add(i);
        }

        return startingNodes;
    }
}
