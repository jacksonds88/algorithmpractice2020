public class DoublyLinkedList {
    public static void main(String[] args)
    {
        var list = new DoublyLinkedList();
        list.add(1, 0);

        System.out.println(list);

        list.add(2, 0);

        System.out.println(list);
    }

    Node head, tail;

    public DoublyLinkedList()
    {
        head = new Node(-9, 0);
        tail = new Node(-1, 0);

        head.next = tail;
        tail.previous = head;
    }

    public void add(int key, int value)
    {
        var newNode = new Node(key, value);

        newNode.next = tail;
        newNode.previous = tail.previous;

        tail.previous.next = newNode;
        tail.previous = newNode;
    }

    public String toString()
    {
        StringBuilder sb = new StringBuilder();

        var current = head;
        while(current != null)
        {
            sb.append("key: ").append(current.key).append("\n");
            current = current.next;
        }

        return sb.toString();
    }

    public class Node
    {
        Node next;
        Node previous;
        int key;
        int value;

        public Node(int key, int value)
        {
            this.key = key;
            this.value = value;
        }

        public String toString()
        {
            return "key: " + key + ", value: " + value;
        }
    }
}
