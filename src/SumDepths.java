import java.util.ArrayList;
import java.util.LinkedList;

/*
Problem can be found here:
https://www.youtube.com/watch?v=-tNMxwWSN_M

1) Return the sum of depths for each node
2) Return the sum of sum of depths for each node
 */

public class SumDepths {

    static class Node
    {
        int val;
        Node left, right;
        int depth;

        public Node(int val)
        {
            this(val, null, null);
        }

        public Node(int val, Node left, Node right)
        {
            this.val = val;
            this.left = left;
            this.right = right;
        }

        public String toString()
        {
            var L = left != null ? left.val : "_";
            var R = right != null ? right.val : "_";
            return "v=" + val + "[D=" + depth + "], (L=" + L + ", R=" + R + ")";
        }
    }

    static int sumpDepthCount = 0;

    // the 1st challenge
    public static void sumDepths(Node current, int depth)
    {
        current.depth = depth; // just for debugging
        sumpDepthCount += depth;

        if (current.left != null)
            sumDepths(current.left, depth + 1);

        if (current.right != null)
            sumDepths(current.right, depth + 1);
    }

    public static int sumDepthsReturn(Node current, int depth)
    {
        current.depth = depth; // just for debugging

        var sumDepth = depth; // make sure you separate the depth variable with the parameter
        if (current.left != null)
            sumDepth += sumDepthsReturn(current.left, depth + 1);

        if (current.right != null)
            sumDepth += sumDepthsReturn(current.right, depth + 1);

        return sumDepth;
    }

    // the 2nd challenge
    public static int sumOfSumDepths(Node current)
    {
        int sum = sumDepthsReturn(current, 0);

        if (current.left != null)
            sum += sumOfSumDepths(current.left);

        if (current.right != null)
            sum += sumOfSumDepths(current.right);

        return sum;
    }

    static String toString(Node root)
    {
        if (root == null)
            return "";

        var layers = new ArrayList<LinkedList<Node>>();
        toString(root, layers, 0);

        StringBuilder sb = new StringBuilder();
        for(var layer : layers)
        {
            for(var node : layer)
            {
                sb.append(node).append(" | ");
            }

            sb.append("\n");
        }

        return sb.toString();
    }

    static void toString(Node node, ArrayList<LinkedList<Node>> layers, int depth)
    {
        var layer = layers.size() <= depth ? new LinkedList<Node>() : layers.get(depth);

        if (layers.size() <= depth)
        {
            layers.add(depth, layer);
        }

        layer.add(node);

        if (node.left != null)
            toString(node.left, layers, depth + 1);

        if (node.right != null)
            toString(node.right, layers, depth + 1);
    }

    public static void main(String[] args)
    {
        Node i = new Node(9);
        Node h = new Node(8);

        Node g = new Node(7);
        Node f = new Node(6);

        Node e = new Node(5);
        Node d = new Node(4, h, i);

        Node c = new Node(3, f, g);
        Node b = new Node(2, d, e);

        Node root = new Node(1, b, c);

        sumDepths(root, 0);
        System.out.println(sumpDepthCount);

        System.out.println(sumDepthsReturn(root, 0)); // 16

        System.out.println(toString(root));

        System.out.println(sumOfSumDepths(root)); // 26
    }
}
