public class SelectionSort {
    public static void main(String[] args)
    {
        int[] A = {11, 25, 12, 22, 64};
        printArray(A);
        selectionSort(A);
        printArray(A);
    }

    public static void selectionSort(int[] A)
    {
        for (int i = 0; i < A.length; i++) {
            int min = i;
            for (int j = i + 1; j < A.length - 1; j++) {
                if (A[min] > A[j])
                    min = j;
            }
            if(min != i)
                swap(A, i, min);
        }
    }

    public static void swap(int[] A, int i, int j)
    {
        int temp = A[i];
        A[i] = A[j];
        A[j] = temp;
    }

    public static void printArray(int[] A)
    {
        StringBuffer sb = new StringBuffer(A.length * 2);
        for(int x : A)
            sb.append(x).append(",");
        if (A.length > 0)
            System.out.println(sb.substring(0, sb.length() - 1));
    }
}
