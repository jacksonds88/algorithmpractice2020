package Core;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Queue;

public class BinarySearchTree {

    private TreeNode root;
    int size = 0;

    public BinarySearchTree() {}

    public BinarySearchTree(TreeNode root) {
        this.root = root;
    }

    public boolean insert(int value)
    {
        int lastSize = size;
        if (root == null)
        {
            root = new TreeNode(value);
            size++;
            return true;
        }

        insert(root, value);
        return lastSize < size;
    }

    public TreeNode getRoot()
    {
        return root;
    }

    public void setRoot(TreeNode newRoot)
    {
        this.root = newRoot;
    }

    public boolean isEmpty()
    {
        return root == null;
    }

    public static TreeNode cloneTree(TreeNode current) {
        return cloneTree(current, null);
    }

    public String toString()
    {
        if (root == null)
            return "[]";

        StringBuilder sb = new StringBuilder();
        inOrderTraversalToString(root, sb);
        return "[" + String.join(", ", sb.toString().split(",")) + "]";
    }

    public static String toStringBFS(TreeNode root)
    {
        ArrayList<LinkedList<String>> layers = new ArrayList<>();
        Queue<TreeNode> Q = new LinkedList<>();
        Q.add(root);
        HashMap<TreeNode, Integer> depthMap = new HashMap<>();
        depthMap.put(root, 0);

        while(!Q.isEmpty())
        {
            TreeNode current = Q.poll();
            if (depthMap.get(current) >= layers.size())
            {
                layers.add(depthMap.get(current), new LinkedList<>());
            }
            LinkedList<String> layer = layers.get(depthMap.get(current));
            layer.add(current.toString());

            System.out.println(depthMap.get(current) + ": " + current.toString());

            if (current.left != null)
            {
                Q.add(current.left);
                depthMap.putIfAbsent(current.left, depthMap.get(current) + 1);
            }

            if (current.right != null)
            {
                Q.add(current.right);
                depthMap.putIfAbsent(current.right, depthMap.get(current) + 1);
            }
        }

        StringBuilder sb = new StringBuilder();
        for(int depth = 0; depth < layers.size(); depth++)
        {
            sb.append("depth=").append(depth).append(": ").append(String.join(",", layers.get(depth))).append("\n");
        }

        return sb.toString();
    }

    private void inOrderTraversalToString(TreeNode current, StringBuilder sb)
    {
        if (current.getLeft() != null)
            inOrderTraversalToString(current.getLeft(), sb);

        sb.append(current.val).append(",");

        if (current.getRight() != null)
            inOrderTraversalToString(current.getRight(), sb);
    }

    private static TreeNode cloneTree(TreeNode current, TreeNode newCurrent) {
        if (newCurrent == null)
            newCurrent = new TreeNode(current.val);

        if (current.left != null) {
            newCurrent.left = new TreeNode(current.left.val);
            cloneTree(current.left, newCurrent.left);
        }

        if (current.right != null) {
            newCurrent.right = new TreeNode(current.right.val);
            cloneTree(current.right, newCurrent.right);
        }

        return newCurrent;
    }

    private void insert(TreeNode current, int value)
    {
        if (value == current.val)
            return;

        if (value < current.val)
        {
            if (current.getLeft() != null)
                insert(current.getLeft(), value);
            else
            {
                current.left = new TreeNode(value);
                size++;
            }
        }
        else
        {
            if (current.getRight() != null)
                insert(current.getRight(), value);
            else
            {
                current.right = new TreeNode(value);
                size++;
            }
        }
    }
}
