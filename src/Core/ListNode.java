package Core;

public class ListNode {
    public int val;
    public ListNode next;
    public ListNode() {}
    public ListNode(int val) { this.val = val; }
    public ListNode(int val, ListNode next) { this.val = val; this.next = next; }

    public String toString()
    {
        return "(" + val + " -> " + (next != null ? next.val : null) + ")";
    }

    public static String toString(ListNode l)
    {
        if (l == null)
            return "";

        StringBuilder sb = new StringBuilder();
        while (l != null)
        {
            sb.append(l.val).append(",");
            l = l.next;
        }

        return String.join(",", sb.toString().split(","));
    }
}