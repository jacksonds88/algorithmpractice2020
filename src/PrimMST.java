import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

public class PrimMST {
    class Edge implements Comparable<Edge>
    {
        int v1, v2;
        int distance;

        public Edge(int v1, int v2, int distance)
        {
            this.v1 = v1;
            this.v2 = v2;
            this.distance = distance;
        }

        @Override
        public int compareTo(Edge e) {
            return Double.compare(distance, e.distance);
        }
    }

    public List<Edge> solution(int[][] G)
    {
        if(G == null || G.length == 0)
            return new LinkedList<>();

        for (var g : G)
            if (g == null || g.length == 0)
                return new LinkedList<>();

        var ret = new LinkedList<Edge>();
        var V = new HashSet<Integer>();
        V.add(0);

        while(V.size() < G.length)
        {
            int minDistance = Integer.MAX_VALUE;
            int next = -1;
            int current = -1;
            for (int curr : V)
            {
                for(int i = 0; i < G[curr].length; i++)
                {
                    if (G[curr][i] > 0 && !V.contains(i))
                    {
                        if (G[curr][i] < minDistance)
                        {
                            minDistance = G[curr][i];
                            next = i;
                            current = curr;
                        }
                    }
                }
            }

            if (next < 0 || current < 0)
                break;

            ret.add(new Edge(current, next, minDistance));
            V.add(next);
        }

        return ret;
    }

    public static void main(String[] args)
    {
        //
        String[] names = new String[]{"S", "A", "C", "B", "D", "T"};
        int[][] G = new int[6][6];
        G[0][1] = 7;
        G[0][2] = 8;

        G[1][0] = 7;
        G[1][2] = 3;
        G[1][3] = 6;

        G[2][0] = 8;
        G[2][1] = 3;
        G[2][3] = 4;
        G[2][4] = 3;

        G[3][1] = 6;
        G[3][2] = 4;
        G[3][4] = 2;
        G[3][5] = 5;

        G[4][2] = 3;
        G[4][3] = 2;
        G[4][5] = 2;

        G[5][3] = 5;
        G[5][4] = 2;

        var prim = new PrimMST();
        var result = prim.solution(G);
        for(var e : result)
        {
            System.out.println(names[e.v1] + ", " + names[e.v2]);
        }
        System.out.println();

        names = new String[]{"A", "B", "C", "D", "E", "F", "G"};
        G = new int[7][7];
        G[0][1] = 7;
        G[0][3] = 5;

        G[1][0] = 7;
        G[1][2] = 8;
        G[1][3] = 9;
        G[1][4] = 7;

        G[2][1] = 8;
        G[2][4] = 5;

        G[3][0] = 5;
        G[3][1] = 9;
        G[3][4] = 15;
        G[3][5] = 6;

        G[4][1] = 7;
        G[4][2] = 5;
        G[4][3] = 15;
        G[4][5] = 8;
        G[4][6] = 9;

        G[5][3] = 6;
        G[5][4] = 8;
        G[5][6] = 11;

        G[6][4] = 9;
        G[6][5] = 11;

        result = prim.solution(G);
        for(var e : result)
        {
            System.out.println(names[e.v1] + ", " + names[e.v2]);
        }
    }
}
