import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Queue;

public class BinaryTreeExperiments {
    public static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode() {
        }

        TreeNode(int val) {
            this.val = val;
        }

        TreeNode(int val, TreeNode left, TreeNode right) {
            this.val = val;
            this.left = left;
            this.right = right;
        }

        public String toString()
        {
            return "[ " + val + " ]";
        }
    }

    static int getHeight(TreeNode node)
    {
        if (node == null)
            return 0;

        return 1 + Math.max(getHeight(node.left), getHeight(node.right));
    }

    static int getDepth(TreeNode node)
    {
        if (node == null)
            return -1;

        return 1 + Math.max(getDepth(node.left), getDepth(node.right));
    }

    static boolean isBalanced(TreeNode node)
    {
        if(node == null)
            return true;

        return isBalanced(node.left)
                && isBalanced(node.right)
                && Math.abs(getHeight(node.left) - getHeight(node.right)) < 2;
    }

    static int sumOfHeights(TreeNode root)
    {
        if(root == null)
            return 0;

        int height = getHeight(root);
        //System.out.println(height);
        return height + sumOfHeights(root.left) + sumOfHeights(root.right);
    }

    static int sumOfDepths(TreeNode root)
    {
        if(root == null)
            return 0;

        int height = getDepth(root);
        //System.out.println(height);
        return height + sumOfDepths(root.left) + sumOfDepths(root.right);
    }

    static String toStringBFS(TreeNode root)
    {
        ArrayList<LinkedList<String>> layers = new ArrayList<>();
        Queue<TreeNode> Q = new LinkedList<>();
        Q.add(root);
        HashMap<TreeNode, Integer> depthMap = new HashMap<>();
        depthMap.put(root, 0);

        while(!Q.isEmpty())
        {
            TreeNode current = Q.poll();
            if (depthMap.get(current) >= layers.size())
            {
                layers.add(depthMap.get(current), new LinkedList<>());
            }
            LinkedList<String> layer = layers.get(depthMap.get(current));
            layer.add(current.toString());

            System.out.println(depthMap.get(current) + ": " + current.toString());

            if (current.left != null)
            {
                Q.add(current.left);
                depthMap.putIfAbsent(current.left, depthMap.get(current) + 1);
            }

            if (current.right != null)
            {
                Q.add(current.right);
                depthMap.putIfAbsent(current.right, depthMap.get(current) + 1);
            }
        }

        StringBuilder sb = new StringBuilder();
        for(int depth = 0; depth < layers.size(); depth++)
        {
            sb.append("depth=").append(depth).append(": ").append(String.join(",", layers.get(depth))).append("\n");
        }

        return sb.toString();
    }

    public static void main(String[] args)
    {
        TreeNode g2 = new TreeNode(4);
        TreeNode f2 = new TreeNode(4);
        TreeNode e2 = new TreeNode(3);
        TreeNode d2 = new TreeNode(3, f2, g2);
        TreeNode c2 = new TreeNode(2);
        TreeNode b2 = new TreeNode(2, d2, e2);
        TreeNode a2 = new TreeNode(1, b2, c2);

        System.out.println(toStringBFS(a2));
        System.out.println(getHeight(a2));
        System.out.println(isBalanced(a2));
        System.out.println(sumOfHeights(a2));
        System.out.println(sumOfDepths(a2));

        TreeNode e = new TreeNode(7);
        TreeNode d = new TreeNode(15);
        TreeNode c = new TreeNode(20, d, e);
        TreeNode b = new TreeNode(9);
        TreeNode a = new TreeNode(1, b, c);

        System.out.println(toStringBFS(a));
        System.out.println(getHeight(a));
        System.out.println(isBalanced(a));
        System.out.println(sumOfHeights(a));
        System.out.println(sumOfDepths(a));
    }
}
