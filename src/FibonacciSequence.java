public class FibonacciSequence {
    public static void main(String[] args)
    {
        for(int i = 0; i < 7; i++)
            System.out.println(fibIteration(i));

        System.out.println();

        for(int i = 0; i < 7; i++)
            System.out.println(fibRecursion(i));

        System.out.println();

        int n = 7;
        for(int i = 0; i < n; i++)
            System.out.println(fibDynamicProgramming(i, new int[n]));
    }

    static int fibIteration(int n)
    {
        int a = 0;
        if(n == 0)
            return a;

        int b = 1;
        if(n == 1)
            return b;

        int c = a + b;
        for(int i = 1; i < n; i++)
        {
            c = a + b;
            a = b;
            b = c;
        }

        return c;
    }

    static int fibRecursion(int n)
    {
        if (n == 0)
            return 0;
        if (n == 1)
            return 1;

        return fibRecursion(n - 1) + fibRecursion(n - 2);
    }

    static int fibDynamicProgramming(int n, int[] dp)
    {
        if (n == 0)
            return 0;
        if (n == 1)
            return 1;

        int a = dp[n - 1] != 0 ? dp[n - 1] : fibDynamicProgramming(n - 1, dp);
        int b = dp[n - 2] != 0 ? dp[n - 2] : fibDynamicProgramming(n - 2, dp);

        dp[n - 1] = a;
        dp[n - 2] = b;

        return a + b;
    }
}
