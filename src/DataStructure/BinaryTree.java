package DataStructure;

public class BinaryTree<K extends Comparable<K>, V>
{
    public static void main(String[] args)
    {
        BinaryTree<Integer, String> BT = new BinaryTree<>();

        System.out.println(BT.put(5, "Five"));
        System.out.println(BT.put(6, "Six"));
        System.out.println(BT.put(3, "Three"));
        System.out.println(BT.put(2, "Two"));
        System.out.println(BT.put(4, "Four"));
        System.out.println(BT.put(1, "One"));
        System.out.println(BT.put(1, "One"));

        System.out.println(BT.toString());
        System.out.println("Size: " + BT.root.size);
    }

    private Node<K, V> root;

    boolean put(K key, V value)
    {
        if (key == null)
            return false;

        if (root == null)
        {
            root = new Node<>(key, value);
            return true;
        }

        return put(key, value, root) != null;
    }

    public String toString()
    {
        StringBuilder sb = new StringBuilder();
        preOrderToString(root, sb);
        return sb.toString();
    }

    private void preOrderToString(Node<K, V> current, StringBuilder sb)
    {
        if (current == null)
            return;
        else
            sb.append(current.toString()).append("\n");

        preOrderToString(current.left, sb);
        preOrderToString(current.right, sb);
    }

    private Node<K, V> put(K key, V value, Node<K, V> current)
    {
        if (current == null)
            return new Node<>(key, value);

        int compareTo = key.compareTo(current.key);
        if (compareTo < 0)
        {
            current.left = put(key,value, current.left);
            current.size = addSize(current.left) + addSize(current.right) + 1;
        }
        else if (compareTo > 0)
        {
            current.right = put(key, value, current.right);
            current.size = addSize(current.left) + addSize(current.right) + 1;
        }
        else {
            current.key = key;
            current.value = value;
        }

        return current;
    }

    private int addSize(Node current)
    {
        if (current == null)
            return 0;

        return current.size;
    }

    class Node<K extends Comparable<K>, V>
           {
        K key;
        V value;
        Node<K, V> left;
        Node<K, V> right;
        int size = 1;

        Node(K key, V value)
        {
            this.key = key;
            this.value = value;
        }

        public String toString()
        {
            return "K=" + key + "; Left=" + (left != null ? left.key : null)
                    + "; Right=" + (right != null ? right.key : null) + "; V=" + value + "; Size=" + size;
        }
    }
}
