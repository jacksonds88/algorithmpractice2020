package DataStructure;

import java.util.Random;

// http://www.mathcs.emory.edu/~cheung/Courses/323/Syllabus/Map/skip-list-impl.html
// https://en.wikipedia.org/wiki/Skip_list

public class SkipList
{
    public static void main(String[] args)
    {
        SkipList sk = new SkipList();
        sk.add(9);
        sk.add(8);
        sk.add(1);
        sk.add(3);
        sk.add(5);
        sk.add(6);
        sk.add(2);
        sk.add(7);

        System.out.println(sk);

        System.out.println();
        System.out.println(sk.search(5));
        System.out.println(sk.search(11));
    }

    enum Coin { HEAD, TAIL }
    private static Random randomGen = new Random();
    private static final double THRESHOLD = 0.2;

    public Node head;
    public Node tail;
    int height;

    public SkipList()
    {
        head = new Node(Integer.MIN_VALUE);
        tail = new Node(Integer.MAX_VALUE);
        head.next = tail;
        tail.previous = head;
        height = 1;
    }

    public boolean add(int x)
    {
        var current = head;
        Node newNode = null;

        while(current.next != null)
        {
            if (current.x == x)
            {
                System.out.println("Already exists");
                return false;
            }
            else if (x < current.next.x && current.below != null)
            {
                current = current.below;
                continue;
            }
            else if (x > current.next.x)
            {
                current = current.next;
                continue;
            }

            newNode = new Node(x);
            newNode.next = current.next;
            newNode.previous = current;

            current.next.previous = newNode;
            current.next = newNode;
            break;
        }

        if (newNode != null)
        {
            int newHeightOfEntry = getHeightOfNewEntry();
            //int newHeightOfEntry = height + 1;
            if (height < newHeightOfEntry)
            {
                Node newHead = new Node(Integer.MIN_VALUE);
                Node newTail = new Node(Integer.MAX_VALUE);

                newHead.next = newTail;
                newTail.previous = newHead;

                newHead.below = head;
                newTail.below = tail;

                head.above = newHead;
                tail.above = newTail;

                head = newHead;
                tail = newTail;

                if (newHeightOfEntry > height)
                    height = newHeightOfEntry;
            }
            addSkipLayer(newHeightOfEntry, newNode);
        }

        return newNode != null;
    }

    public boolean search(int x)
    {
        var current = head;
        while(current.next != null)
        {
            if (current.x == x)
                return true;
            else if (x < current.next.x && current.below != null)
            {
                current = current.below;
                continue;
            }
            else if (x > current.next.x && current.below == null) // passed entry in the ordered list
                return false;

            current = current.next;
        }

        return false;
    }

    public String toString()
    {
        StringBuilder sb = new StringBuilder();

        var current = head;
        while(current.below != null)
            current = current.below;

        current = current.next; // to move away from head
        while(current.next != null)
        {
            var currentAbove = current;
            while(currentAbove != null)
            {
                sb.append(currentAbove.x).append(",");
                currentAbove = currentAbove.above;
            }
            sb.append("\n");

            current = current.next; // to move away from head
        }

        return sb.toString();
    }

    private void addSkipLayer(int targetHeight, Node newNode)
    {
        var layerNode = head;

        while(layerNode.below != null)
            layerNode = layerNode.below;

        layerNode = layerNode.above; // to get to layer 2
        var currentHeight = 2;
        while(layerNode != null && currentHeight <= targetHeight)
        {
            var current = layerNode;
            while(current != null)
            {
                if (newNode.x > current.x)
                {
                    current = current.next;
                    continue;
                }

                Node newAbove = new Node(newNode.x);
                newAbove.next = current;
                newAbove.previous = current.previous;
                newAbove.below = newNode;
                newNode.above = newAbove;
                newNode = newNode.above; // move up

                current.previous.next = newAbove;
                current.previous = newAbove;
                break;
            }

            layerNode = layerNode.above;
            currentHeight++;
        }
    }

    private int getHeightOfNewEntry()
    {
        int currentHeight = 1;
        while (FlipCoin() == Coin.HEAD && currentHeight <= height)
            currentHeight++;

        return currentHeight;
    }

    private static Coin FlipCoin()
    {
        return randomGen.nextDouble() > THRESHOLD ? Coin.HEAD : Coin.TAIL;
    }

    public class Node
    {
        int x;
        Node above;
        Node below;
        Node next;
        Node previous;

        public Node(int x)
        {
            this.x = x;
        }

        public Node(int x, Node next)
        {
            this.x = x;
            this.next = next;
        }

        public Node(int x, Node next, Node previous)
        {
            this.x = x;
            this.next = next;
        }

        public String toString()
        {
            return "prev: " + (previous != null ? previous.x : "null") + ", x: " + x + ", next: " + (next != null ? next.x : "null") + ", above: " + (above != null ? "yes" : "no");
        }
    }
}
