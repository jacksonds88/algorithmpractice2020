package DataStructure;

public class SimpleLinkedList<E>
{
    Node<E> head;
    Node<E> tail;
    Node<E> current;
    int size = 0;

    public SimpleLinkedList()
    {
        current = head;
    }

    public void addAll(E... elements)
    {
        for (E e : elements)
            add(e);
    }

    public void add(E e)
    {
        size++;
        Node<E> newNode = new Node<>(e, null);

        if (head == null)
        {
            head = newNode;
            tail = head;
            current = head;
        }

        tail.next = newNode;
        tail = tail.next;
    }

    public Node<E> current()
    {
        return current;
    }

    public Node<E> next()
    {
        if(current == null)
            return null;

        if (current.hasNext())
        {
            current = current.next;
            return current;
        }

        return null;
    }

    public boolean hasNext() {
        if (current == null)
            return false;

        return current.hasNext();
    }

    public Node<E> getTailByIteration()
    {
        var current = head;
        while (current.hasNext()){
            current = current.next;
        }

        return current;
    }

    public int getSizeByIteration()
    {
        if(head == null)
            return 0;

        var current = head;
        int size = 1;
        while (current.hasNext()){
            current = current.next;
            size++;
        }

        return size;
    }

    public Node<E> getFirstNodeWithElement(E e)
    {
        Node<E> current = head;
        while(current != null)
        {
            if(current.e.equals(e))
                return current;

            current = current.next;
        }

        return null;
    }

    public int getSize()
    {
        return size;
    }

    public void resetCurrentToHead()
    {
        current = head;
    }

    public String toString()
    {
        StringBuilder sb = new StringBuilder(100);

        Node<E> current = head;
        while(current != null)
        {
            sb.append(current.e).append(",");
            current = current.next;
        }

        if (sb.length() > 0)
            return sb.toString().substring(0, sb.length() - 1);
        else
            return "";
    }

    public class Node<E>
    {
        public E e;
        public Node<E> next;

        public Node(E e, Node<E> next)
        {
            this.e = e;
            this.next = next;
        }

        public boolean hasNext()
        {
            return next != null;
        }

        public String toString()
        {
            return e + ": " + (next != null ? next.e : null);
        }
    }
}
