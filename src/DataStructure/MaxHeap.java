package DataStructure;

public class MaxHeap {

    static final int EXPAND_LENGTH = 10;

    int[] heap;
    int size = 0;

    public MaxHeap(int capacity)
    {
        this.heap = new int[capacity];
    }

    // bottom-up, more efficient, must balance after calling this
    public MaxHeap(int[] heap)
    {
        this.heap = heap;
        size = this.heap.length;
        bottomUp();
    }

    public void add(int x)
    {
        if(size >= heap.length)
        {
            int[] newHeap = new int[heap.length + EXPAND_LENGTH];
            for(int i = 0; i < newHeap.length; i++)
                if(i < heap.length)
                    newHeap[i] = heap[i];
                else
                    newHeap[i] = Integer.MIN_VALUE;
            heap = newHeap;
        }

        for(int a : heap)
            System.out.println(a);
        System.out.println("----------------------");
        heap[size] = x;
        heapifyUp(size);
        size++;
    }

    public int[] getHeap()
    {
        return heap;
    }

    void heapifyUp(int i)
    {
        while(i > 0)
        {
            i = getParentIndex(i);
            heapifyHelper(i);
        }
    }

    private int heapifyHelper(int p)
    {
        int parentOfP = getParentIndex(p);

        int c = getLeftChildIndex(p);
        if (c >= heap.length || heap[c] == Integer.MIN_VALUE)
            return parentOfP;

        int r = getRightChildIndex(p);
        if (r < heap.length && heap[r] > Integer.MIN_VALUE)
        {
            c = heap[r] > heap[c] ? r : c;
        }

        if (heap[c] > heap[p])
            swap(p, c);

        return parentOfP;
    }

    void swap(int i, int j)
    {
        int temp = heap[i];
        heap[i] = heap[j];
        heap[j] = temp;
    }

    void bottomUp()
    {
        for(int parent = getParentIndex(size); parent >= 0; parent--)
        {
            int p = parent;
            int pval = heap[p];
            boolean isHeap = false;
            int j = getLeftChildIndex(parent);
            while(!isHeap && j < size)
            {
                if (j + 1 < size && heap[j] < heap[j+1])
                    j = j + 1;

                if(pval > heap[j])
                    isHeap = true;
                else
                {
                    heap[p] = heap[j];
                    p = j;
                }
                j = getLeftChildIndex(p);
            }

            heap[p] = pval;
        }
    }

    int getParentIndex(int i)
    {
        return (i - 1) / 2;
    }

    int getLeftChildIndex(int i)
    {
        return i * 2 + 1;
    }

    int getRightChildIndex(int i)
    {
        return i * 2 + 2;
    }

    public static void main(String[] args)
    {
        int[] heap = new int[]{ 2, 9, 7, 6, 5, 8 };
        MaxHeap bh = new MaxHeap(heap);

        //for(int x : bh.getHeap()) System.out.println(x);

        int[] heap2 = new int[]{ 25, 19, 36, 17, 3, 100, 1, 2, 7 };
        MaxHeap bh2 = new MaxHeap(heap2);

        //for(int x : bh2.getHeap()) System.out.println(x);

        int[] heap3 = new int[]{ 2, 9, 7, 6, 5, 8 };
        MaxHeap bh3 = new MaxHeap(heap3.length);
        for(int x : heap3)
            bh3.add(x);

        for(int x : bh3.getHeap())
            System.out.println(x);
    }
}
