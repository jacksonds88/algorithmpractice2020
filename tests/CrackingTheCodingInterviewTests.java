import CrackingTheCodingInterview.*;

import org.junit.Assert;
import org.junit.Test;

public class CrackingTheCodingInterviewTests {
    @Test
    public void Chapter1_IsUnique()
    {
        Assert.assertTrue(Chapter1_1_IsUnique.solution("helo"));
        Assert.assertFalse(Chapter1_1_IsUnique.solution("hello"));
    }

    @Test
    public void Chapter1_CheckPermutation()
    {
        Assert.assertFalse(Chapter1_2_CheckPermutation.solution("CCC", "CCB"));
        Assert.assertTrue(Chapter1_2_CheckPermutation.solution("ABCD", "DACB"));
    }
}
