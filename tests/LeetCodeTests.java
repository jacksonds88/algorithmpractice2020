import Core.ListNode;
import org.junit.Assert;
import org.junit.Test;

import LeetCode.*;

public class LeetCodeTests {
    @Test
    public void LRUCacheSimple()
    {
        var cache = new LRUCacheSimple(2);
        cache.put(1, 1);
        cache.put(2, 2);
        Assert.assertEquals(cache.get(1), 1); ;       // returns 1
        cache.put(3, 3);    // evicts key 2
        Assert.assertEquals(cache.get(2), -1); ;       // returns 1
        cache.put(4, 4);    // evicts key 1
        Assert.assertEquals(cache.get(1), -1); ;       // returns 1
        Assert.assertEquals(cache.get(3), 3); ;       // returns 1
        Assert.assertEquals(cache.get(4), 4); ;       // returns 1

        Assert.assertEquals(cache.getSize(), cache.getCapacity());
    }

    @Test
    public void PlusOne()
    {
        var po = new PlusOne();
        Assert.assertArrayEquals(new int[]{1, 2, 4}, po.plusOne(new int[]{1, 2, 3}));
        Assert.assertArrayEquals(new int[]{4, 3, 2, 2}, po.plusOne(new int[]{4, 3, 2, 1}));
        Assert.assertArrayEquals(new int[]{1, 0, 0}, po.plusOne(new int[]{9, 9}));
    }

    @Test
    public void AddTwoNumbers()
    {
        var atn = new AddTwoNumbers();
        ListNode l1 = new ListNode(0);
        ListNode l1b = new ListNode(5);
        l1.next = l1b;

        ListNode l2 = new ListNode(0);
        ListNode l2b = new ListNode(5);
        l2.next = l2b;

        ListNode l3 = new ListNode(0);
        ListNode l3b = new ListNode(0);
        ListNode l3c = new ListNode(1);
        l3.next = l3b;
        l3b.next = l3c;

        Assert.assertTrue(atn.equals(l3, atn.addTwoNumbers(l1, l2)));
        Assert.assertFalse(atn.equals(l1, atn.addTwoNumbers(l1, l2)));
    }
}





